<?php
/**
 * Template Name: page du questionnaire old
 * Template post type: page, post
 */

$age_saisie = false;
$age_apres_3_ans = false;

if (isset($_POST) && !empty($_POST)) {

    if(isset($_POST["age_enfant"]) && !empty($_POST["age_enfant"])) {
        $_SESSION['age_enfant'] = $_POST["age_enfant"];
        $_SESSION['tranche_age'] = trouver_tranche_age($_POST["age_enfant"]);
        $age_saisie = true;
        if($_POST["age_enfant"] > 3) {
            $age_apres_3_ans = true;
        }
    }
}

function trouver_tranche_age($age) {
    $age_tranches = [2, 4, 6, 12];
    $num_tranche = 0;
    foreach($age_tranches as $borne) {
        if($age <= $borne) {
            return $num_tranche;
        }
        $num_tranche ++;
    }
    return $num_tranche;
}

?>

<?php  get_header(); ?>


<?php if(!$age_saisie) : ?>
Veuilez saisir l'age de l'enfant
<?php endif; ?>


<?php  if(have_posts()) : while(have_posts()): the_post(); ?>
    <!--h1><?php the_title(); ?></h1-->
    <br/>
    <br/>
    <!--div class="cadre" style="margin: 25px auto!important;">
        

    </div-->

<?php endwhile; endif; ?>



<form method="post" id="questionnaire_ecran_form" action="<?php bloginfo('url'); ?>/index.php/questionnaire-resultat">

<div id="accordion">
  <div class="card">
    <div class="card-header" id="heading-0">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-0" aria-expanded="true" aria-controls="collapse-0">
            1. Quels types d’écrans y a-t-il à la maison, et combien ?
        </a>
      </h5>
    </div>
    <div id="collapse-0" class="collapse show" data-parent="#accordion" aria-labelledby="heading-0">
      <div class="card-body">
    
        <div id="propo1question1">
            <label class="labeltext">Quels types d’écrans y a-t-il à la maison, et combien ?</label><br>
            <div class="form-check-inline">
                <label>Télévision: </label><span style="width: 25px"></span>
                <label class="customradio">
                    <input type="radio" checked="checked" name="teleRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Oui</span>
                </label>        
                <label class="customradio">
                    <input type="radio" name="teleRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Non</span>
                </label>
                <span style="width: 35px"></span>
                <span class="radiotextsty"> Combiens</span>
                <span style="width: 15px"></span>
                <input type="text" name="combiensTele" value=""/>
            </div>
        </div>

        <div id="propo2question1">  
            <div class="form-check-inline">
            <label>Tablette: </label><span style="width: 25px"></span>
                <label class="customradio">
                    <input type="radio" checked="checked" name="tabletteRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Oui</span>
                </label>        
                <label class="customradio">
                    <input type="radio" name="tabletteRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Non</span>
                </label>
                <span style="width: 35px"></span>
                <span class="radiotextsty"> Combiens</span>
                <span style="width: 15px"></span>
                <input type="text" name="combiensTele" value=""/>
            </div>
        </div>


        <div id="propo3question1">  
            <div class="form-check-inline">
            <label>Ordinateur: </label><span style="width: 25px"></span>
                <label class="customradio">
                    <input type="radio" checked="checked" name="ordiRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Oui</span>
                </label>        
                <label class="customradio">
                    <input type="radio" name="ordiRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Non</span>
                </label>
                <span style="width: 35px"></span>
                <span class="radiotextsty"> Combiens</span>
                <span style="width: 15px"></span>
                <input type="text" name="combiensTele" value=""/>
            </div>
        </div>

        <div id="propo4question1">  
            <div class="form-check-inline">
            <label>Téléphone: </label><span style="width: 25px"></span>
                <label class="customradio">
                    <input type="radio" checked="checked" name="telephoneRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Oui</span>
                </label>        
                <label class="customradio">
                    <input type="radio" name="telephoneRadio">
                    <span class="checkmark"></span>
                    <span class="radiotextsty">Non</span>
                </label>
                <span style="width: 35px"></span>
                <span class="radiotextsty"> Combiens</span>
                <span style="width: 15px"></span>
                <input type="text" name="combiensTele" value=""/>
            </div>
        </div>

        <br/>
        <a role="button" data-toggle="collapse" href="#collapse-1" aria-expanded="false" aria-controls="collapse-1">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>

      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="heading-1">
      <h5 class="mb-0">
        <a role="button" data-toggle="collapse" href="#collapse-1" aria-expanded="false" aria-controls="collapse-1">
            2.	Confiez-vous votre téléphone à votre enfant 
        </a>
      </h5>
    </div>
    <div id="collapse-1" class="collapse " data-parent="#accordion" aria-labelledby="heading-1">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>Pour le calmer quand il pleure ?</lable></td>
                <td><label for="oui_telephone_pleur">Oui</label><input id="oui_telephone_pleur" value="oui" name="telephone_pleur" type="radio" /></td>
                <td><label for="non_telephone_pleur">Non</label><input id="non_telephone_pleur" value="non" name="telephone_pleur" type="radio" /></td>
            </tr>
        </table>
        
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>Pour l’occuper ?</lable></td>
                <td><label for="oui_telephone_occuper">Oui</label><input id="oui_telephone_occuper" value="oui" name="telephone_occuper" type="radio" /></td>
                <td><label for="non_telephone_occuper">Non</label><input id="non_telephone_occuper" value="non" name="telephone_occuper" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>Pour l’aider à manger ?</lable></td>
                <td><label for="oui_telephone_manger">Oui</label><input id="oui_telephone_manger" value="oui" name="telephone_manger" type="radio" /></td>
                <td><label for="non_telephone_manger">Non</label><input id="non_telephone_manger" value="non" name="telephone_manger" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>Pour s’endormir ?</lable></td>
                <td><label for="oui_telephone_endormir">Oui</label><input id="oui_telephone_endormir" value="oui" name="telephone_endormir" type="radio" /></td>
                <td><label for="non_telephone_endormir">Non</label><input id="non_telephone_endormir" value="non" name="telephone_endormir" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>Pour son apprentissage ?</lable></td>
                <td><label for="oui_telephone_apprentissage">Oui</label><input id="oui_telephone_apprentissage" value="oui" name="telephone_apprentissage" type="radio" /></td>
                <td><label for="non_telephone_apprentissage">Non</label><input id="non_telephone_apprentissage" value="non" name="telephone_apprentissage" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>Pour parler avec la famille ?</lable></td>
                <td><label for="oui_telephone_parler">Oui</label><input id="oui_telephone_parler" name="telephone_parler" type="radio" /></td>
                <td><label for="non_telephone_parler">Non</label><input id="non_telephone_parler" name="telephone_parler" type="radio" /></td>
            </tr>
        </table>

        
        <div class="alert alert-danger alert-danger-questionnaire" id="telephone_pleur_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:group -->
            <div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:list -->
            <ul><li><strong><span class="has-inline-color has-vivid-red-color">Comment  calmer les pleurs de son enfant sans avoir recours aux écrans !</span></strong><ul><li>Parlez-lui pour le calmer<ul><li>en gardant votre calme</li><li>en gardant votre calme,</li><li>se  mettre vous a sa hauteur,</li><li>parlez-lui normalement</li><li>rassurez-le en le caressant</li><li>L’enfant réagit par mimétisme</li></ul></li><li>Détournez son attention<ul><li> avec humour ou en parlant d’un autre sujet</li><li>Prenez un petit sac avec des jouer ou des livres de quoi l’occuper</li></ul></li></ul><ul><li>Si la situation est tendue sortez à l’extérieur et foire plus au moins une promenade pour faire oublier les écrans   </li></ul></li></ul>
            <!-- /wp:list --></div></div>
            <!-- /wp:group -->
        </div>
      
        <div class="alert alert-danger alert-danger-questionnaire" id="telephone_occuper_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Comment l’occuper son enfant sans avoir recours aux écrans !</strong><ul><li>Faites-le participer à votre activité du moment (cuisine, bricolage, ménage …)</li></ul><ul><li>Faire des promenades</li></ul><ul><li>Prenez un petit sac avec des jouer ou des livres de quoi l’occuper lors d’une sortie</li></ul><ul><li>Faire des activités<ul><li>S’il a une fibre artistique = dessin, collage, décollage, peinture …)</li></ul><ul><li>Bricolage = jeux d’encastrement et de construction (lego, kalpa, Play mobile…)</li></ul><ul><li>Pour le rêveur = les jeux pour faire semblant (dinette, parcourt de voiture, matériel de bricolage, matériel de ménage …)</li></ul><ul><li>Les jeux de sociétés</li></ul><ul><li>Activité sportif</li></ul><ul><li>La cuisine</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>

  
        <div class="alert alert-danger alert-danger-questionnaire" id="telephone_manger_alert" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Comment aider son enfant à manger sans avoir recours aux écrans&nbsp;!</strong><ul><li>Attention le moment du repas est un moment de partage et d’apprentissage</li></ul><ul><li>Mettre votre enfant devant un écran ce l’exposé à ses risques<ul><li>Surpoids, obésité =&gt; l’enfant mange sans faire attention à ce qu’il ingère = il se rempli simplement</li></ul><ul><li>Trouble de langage et de la communication =&gt; temps volé aux échanges verbaux&nbsp;</li></ul><ul><li>Trouble de la motricité fine =&gt; temps volé à l’acquisition de ma motricité fine (tartiné, tenir une tasse …)</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        
        <div class="alert alert-danger alert-danger-questionnaire" id="telephone_endormir_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Comment aider son enfants &nbsp;à s’endormir sans avoir recours aux écrans &nbsp;?&nbsp;&nbsp;&nbsp;</strong><ul><li>Mettre votre enfant devant un écran avec de dormir c’est l’exposer au écran bleu qui va induire une modification de la sécrétion de la mélatonine =&gt; trouble du sommeil</li></ul><ul><li>L’enfant peut être exposée à des imagés ou des scènes non adapté à son âge et induire des cauchemardes, et une exposition à une violence audiovisuelle.</li></ul><ul><li>Lisez lui une petite histoire les enfants adorent cela.&nbsp;</li></ul></li></ul>
            <!-- /wp:list -->
        </div>

        <div class="alert alert-danger alert-danger-questionnaire" id="telephone_apprentissage_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Comment aider son enfants dans son apprentissage sans avoir recours aux écrans  ?</strong><ul><li>Attention mettre un enfant &lt;3 ans devant un écran ne lui apprendra rien, il sera victime du déficit de transfert => a cet âge l’enfant n’a pas la capacité d’interposé les objets virtuelles à la réalité</li></ul><ul><li>Des études ont prouvé que l’exposition de votre enfant à l’écran induire un retard des acquisitions<ul><li>Trouble de l’attention,</li></ul><ul><li>Trouble de langage</li></ul><ul><li>Trouble de la motricité fine</li></ul><ul><li>Trouble de l’écriture, dessin, lecture</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
            <!-- wp:image {"id":99,"width":155,"height":239,"sizeSlug":"large","className":"is-style-default"} -->
            <figure class="wp-block-image size-large is-resized is-style-default"><img src="http://localhost/wp-content/uploads/2020/10/image.png" alt="" class="wp-image-99" width="155" height="239"/></figure>
            <!-- /wp:image -->
        </div>

        <div class="alert alert-danger alert-danger-questionnaire" id="telephone_parler_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Qu'apporte les échanges en famille à votre enfant ?</strong><ul><li>Les échanges avec votre enfant à table ou en famille, ils sont très formateurs et enrichissantes pour votre enfant  il va apprendre des nouveaux vocabulaires et améliorer son langage et sa communication.</li></ul><ul><li>Il ne faut pas le mettre à l’écart et encore moins devant un écran</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:image {"align":"center","id":100,"sizeSlug":"full"} -->
            <div class="wp-block-image"><figure class="aligncenter size-full"><img src="http://localhost/wp-content/uploads/2020/10/image-1.png" alt="" class="wp-image-100"/></figure></div>
            <!-- /wp:image -->
        </div>

        <a role="button" data-toggle="collapse" href="#collapse-2" aria-expanded="false" aria-controls="collapse-2">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>
        
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="heading-2">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-2" aria-expanded="false" aria-controls="collapse-2">
        3.	Laissez-vous en général votre télé allumée en permanence (ou presque) ?
        </a>
      </h5>
    </div>
    <div id="collapse-2" class="collapse" data-parent="#accordion" aria-labelledby="heading-2">
      <div class="card-body">

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"></td>
                <td><label for="oui_television_allume">Oui</label><input id="oui_television_allume" value="oui" name="television_allume" type="radio" /></td>
                <td><label for="non_television_allume">Non</label><input id="non_television_allume" value="non" name="television_allume" type="radio" /></td>
            </tr>
        </table>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "0" ): ?>
        <div class="alert alert-danger television_allume_alert alert-danger-questionnaire" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>La télévision laissée allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parents-enfant</li><li>Sans ce rendre compte on expose notre ou nos enfants à des  programmes non destinés aux enfants (actualistes, sport, programme de jeu, film…) qui peuvent être source de violence visuelle </li><li>La télévision en font d’écran  interrompe  en permanence l’attention de notre enfants => son attention est en permanence morcelée par les courts interruptions ou il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité</li><li>Donc<ul><li>Pour chaque augmentation du temps d’écran  de 30 minutes par jour => augmente de 50% le risque que l’enfant souffre d’un retard de langage, car lorsque la télévision est allumée en arrière-plan toute la journée, un enfant entendra 13400 mots en  moins par  semaine que si la télévision est éteinte => Diminution de son temps  d’échange  verbal  => Trouble de langage</li></ul><ul><li>Diminution de son temps d’attention aux jeux => Trouble de l’apprentissage</li></ul><ul><li>Exposition à une violence visuelle</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "1" ): ?>
        <div class="alert alert-danger television_allume_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>La télévision laissée allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parents-enfant</li><li>Sans ce rendre compte on expose notre ou nos enfants a des  programmes non destinés aux enfants (actualités, sport, programme de jeu, film…) qui peuvent être source de violence visuelle </li><li>La télévision en font d’écran  interrompe  en permanence l’attention de notre enfants => son attention est en permanence morcelée par les courts interruptions ou il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité</li><li>Donc<ul><li>Pour chaque augmentation du temps d’écran  de 30 minutes par jour => augmente de 50% le risque que l’enfant souffre d’un retard de langage, car lorsque la télévision est allumée en arrière-plan toute la journée, un enfant entendra 13400 mots en  moins par  semaine que si la télévision est éteinte => Diminution de son temps  d’échange  verbal  => Trouble de langage</li></ul><ul><li>Diminution de son temps d’attention aux jeux => Trouble de l’apprentissage</li><li>Exposition à une violence visuelle</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "2" ): ?>
        <div class="alert alert-danger television_allume_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>Lorsque la télévision est allumée en arrière-plan toute la journée, un enfant entendra 13400 mots en&nbsp; moins par&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; semaine que si la télévision est éteinte</li><li>La télévision laissée allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parents-enfant</li><li>Sans ce rendre compte on expose notre ou nos enfants a des&nbsp; programmes non destinés aux enfants (actualistes, sport, programme de jeu, film…) qui peuvent être source de violence visuelle&nbsp;</li><li>La télévision en font d’écran&nbsp; interrompe&nbsp; en permanence l’attention de notre enfants =&gt; son attention est en permanence morcelée par les courts interruptions ou il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité</li><li>Donc<ul><li>Diminution de son temps&nbsp; d’échange&nbsp; verbal&nbsp; =&gt; Trouble de langage</li></ul><ul><li>Diminution de son temps d’attention aux jeux =&gt; Trouble de l’apprentissage</li></ul><ul><li>Exposition à une violence visuelle</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "3" ): ?>
        <div class="alert alert-danger television_allume_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>Lorsque la télévision est allumée en arrière-plan toute la journée, un enfant entendra 13400 mots en  moins par            semaine que si la télévision est éteinte</li><li>La télévision laissée allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parents-enfant</li><li>Sans ce rendre compte on expose notre ou nos enfants a des  programmes non destinés aux enfants (actualités, sport, programme de jeu, film…) qui peuvent être source de violence visuelle </li><li>La télévision en font d’écran  interrompe  en permanence l’attention de notre enfants => son attention est en permanence morcelée par les courts interruptions ou il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité</li><li>Donc<ul><li>Diminution de son temps  d’échange  verbal  => Trouble de langage</li></ul><ul><li>Diminution de son temps d’attention aux jeux => Trouble de l’apprentissage</li><li>Exposition à une violence visuelle</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "4" ): ?>
        <div class="alert alert-danger television_allume_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>La télévision laissée allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parents-enfant</li><li>Sans ce rendre compte on expose notre ou nos enfants a des&nbsp; programmes non destinés aux enfants (actualistes, sport, programme de jeu, film…) qui peuvent être source de violence visuelle&nbsp;</li><li>La télévision en font d’écran&nbsp; interrompe&nbsp; en permanence l’attention de notre enfants =&gt; son attention est en permanence morcelée par les courts interruptions ou il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité</li><li>Donc<ul><li>Diminution de son temps&nbsp; d’échange&nbsp; verbal&nbsp; =&gt; Trouble de langage</li></ul><ul><li>Diminution de son temps d’attention aux jeux =&gt; Trouble de l’apprentissage</li></ul><ul><li>Exposition à une violence visuelle</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>
        
        <div class="alert alert-info television_allume_astuce alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p><strong>Astuces pour limiter l’accès aux écrans</strong></p>
            <!-- /wp:paragraph -->
            <!-- wp:paragraph -->
            <p>Pour éviter que la télévision ne soit allumée par votre enfant</p>
            <!-- /wp:paragraph -->
            <!-- wp:list -->
            <ul><li>Mettre un code</li><li>Ranger la télécommande</li><li>Fermer la télévision après utilisation </li></ul>
            <!-- /wp:list -->
            <!-- wp:paragraph -->
            <p>Et n’hésitez pas à vous interroger sur vos habitudes</p>
            <!-- /wp:paragraph -->
            <!-- wp:paragraph -->
            <p>Si vous avez le reflex d’allumer la télévision changer vos habitude</p>
            <!-- /wp:paragraph -->
            <!-- wp:list -->
            <ul><li>en allument la radio,</li><li>écouter la musique</li><li>jouer ou faire une activité avec votre enfant</li></ul>
            <!-- /wp:list -->
        </div>

        <a role="button" data-toggle="collapse" href="#collapse-3" aria-expanded="false" aria-controls="collapse-3">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>

      </div>

    </div>
  </div>

  <div class="card">
    <div class="card-header" id="heading-3">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-3" aria-expanded="false" aria-controls="collapse-3">
            4.	Votre enfant passe-t-il du temps devant un écran ?
        </a>
      </h5>
    </div>
    <div id="collapse-3" class="collapse" data-parent="#accordion" aria-labelledby="heading-3">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"></td>
                <td>
                    <label for="oui_temps_ecran">Oui</label><input id="oui_temps_ecran" value="oui" name="temps_ecran" type="radio" />
                </td>
                <td><label for="non_temps_ecran">Non</label><input id="non_temps_ecran" value="non" name="temps_ecran" type="radio" /></td>
            </tr>
        </table>

           
        <div class="temps_ecran_extra">
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="oui_temps_ecran_30_m">Moins de 30 mn</label></td>
                    <td><input id="oui_temps_ecran_30_m" value="Moins de 30 mn" name="temps_enfant_ecran" type="radio" /></td>
                </tr>
            </table>
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="oui_temps_ecran_1_h">30 mn à 1 h </label></td>
                    <td><input id="oui_temps_ecran_1_h" name="temps_enfant_ecran" type="radio" /></td>
                </tr>
            </table>
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="oui_temps_ecran_2_h">De 1 à 2 h</label></td>
                    <td><input id="oui_temps_ecran_2_h" name="temps_enfant_ecran" type="radio" /></td>
                </tr>
            </table>
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="oui_temps_ecran_plus_2_h">Plus de 2 h</label> </td>
                    <td><input id="oui_temps_ecran_plus_2_h" name="temps_enfant_ecran" type="radio" /></td>
                </tr>
            </table>
        </div>
         
      
        <?php if($age_saisie && $_SESSION['tranche_age'] == "0" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire temps_enfant_ecran_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>Zéro écrans avant 2 ans</li><li>Sauf <em>(à une condition pas plus de 1 fois par jour)</em></li><li>Pour regarder des photos sur une tablette</li><li>Faire un coucou à la famille par Skype ou autre</li><li>Regarder une comptine sur les genoux d’un parent pendant quelque minute</li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Pourquoi&nbsp;?<ul><li>L’enfant a besoin de construire ses repères dans l’espace et dans le temps.</li></ul><ul><li>Les enfants n’apprennent rien devant les écrans, car ils n’ont pas encore la faculté de comprendre ce qu’ils regardent et de faire le lien avec la réalité qui les entoure =&gt; on appel a le phénomène «&nbsp;du déficit du transfert&nbsp;»</li></ul><ul><li>Par exemple&nbsp;: les enfants qui apprennent à compter ou&nbsp; à parler une autre langue via les écrans, ils ne font que répéter ce qu’ils ont entendu en boucle sans savoir la signification exacte du mot.&nbsp;</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "1" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire temps_enfant_ecran_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>La durée d’exposition ne doit pas dépasser les 30 minutes par jours</li><li>Une règle d’or à cet âge est l’accompagnement : un adulte aux côtés de l’enfant peut commenter et faire des liens avec la réalité (ce qui permet de diminuer le déficit vidéo)</li><li>Il est préférable de prévoir des temps courts de 10-15 min avec des moments sans écrans en essayant de passer à une autre activité.&nbsp;</li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Pourquoi ? Car votre enfant a besoin<ul><li>de se dépenser, de bouger</li></ul><ul><li>de manipuler,</li></ul><ul><li>de comprendre la notion de causalité,</li></ul><ul><li>d’utiliser la 3D,</li></ul><ul><li>de dessiner avec un vrai crayon (ce qui le préparer à l’écriture)</li></ul><ul><li>de lire des vrais livres pour préparer l’apprentissage de la lecture</li><li>Le recours à l’écran semble au début magique car votre enfant est tellement fasciné qu’il se laisse effectivement faire (manger, le calmer…)  =>  Mais cela crée une telle dépendance qu’il devient impossible de le retirer sans pleurs => induire une dépendance aux écrans</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "2" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire temps_enfant_ecran_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>On essai de se limiter à 1h par jour</li><li>Il n’est pas conseillé d’introduire les jeux numériques</li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Pourquoi ? Car votre enfant a besoin<ul><li>de se dépenser, de bouger</li></ul><ul><li>de manipuler,</li></ul><ul><li>de comprendre la notion de causalité,</li></ul><ul><li>d’utiliser la 3D,</li></ul><ul><li>de dessiner avec un vrai crayon (ce qui le préparer à l’écriture)</li></ul><ul><li>de lire des vrais livres pour préparer l’apprentissage de la lecture</li><li>Le recours à l’écran semble au début magique car votre enfant est tellement fasciné qu’il se laisse effectivement faire (manger, le calmer…)  =>  Mais cela crée une telle dépendance qu’il devient impossible de le retirer sans pleurs => induire une dépendance aux écrans</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "3" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire temps_enfant_ecran_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>La durée d’exposition ne doit pas dépasser<ul><li>les 1h par jour pour les enfants&nbsp;&nbsp; entre 6-8 ans</li></ul><ul><li>entre 1h-1h30 pour les enfants entre 8-12 ans</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "4" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire temps_enfant_ecran_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>La durée d’exposition ne doit être contrôlée</li><li>Il faudra mettre en place un contrat de confiance  (contrôle parental, application pour limiter le temps d’écrans</li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>


        <a role="button" data-toggle="collapse" href="#collapse-4" aria-expanded="false" aria-controls="collapse-4">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="heading-4">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-4" aria-expanded="false" aria-controls="collapse-4">
            5. Un ou des écrans sont-ils allumés pendant les repas ?  
        </a>
      </h5>
    </div>
    <div id="collapse-4" class="collapse" data-parent="#accordion" aria-labelledby="heading-4">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"></td>
                <td>
                    <label for="oui_allume_repas">Oui</label><input id="oui_allume_repas" value="oui" name="allume_repas" type="radio" />
                </td>
                <td><label for="non_allume_repas">Non</label><input id="non_allume_repas" value="non" name="allume_repas" type="radio" /></td>
            </tr>
        </table>

           
        <div class="card-body allume_repas_extra">
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="oui_allume_refus_manger">Refus de manger</label></td>
                    <td><input id="oui_allume_refus_manger" value="Refus de manger" name="allume_ecran_cause" type="radio" /></td>
                </tr>
            </table>
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="oui_allume_donne_manger">Utilisation de son smartphone lors qu’on donne à manger à son enfant</label></td>
                    <td><input id="oui_allume_donne_manger" value="Utilisation de son smartphone lors qu’on donne à manger à son enfant" name="allume_ecran_cause" type="radio" /></td>
                </tr>
            </table>
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first">
                        <label for="oui_allume_information">La télévision allumée en fond d’écran ou pour regarder l’information</label>
                    </td>
                    <td><input id="oui_allume_information" value="La télévision allumée en fond d’écran ou pour regarder l’information" name="allume_ecran_cause" type="radio" /></td>
                </tr>
            </table>
        </div>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "0" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire allume_ecran_cause_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list {"ordered":true,"type":"1"} -->
            <ol type="1"><li>Si votre enfant refuse de manger et n’ouvre la bouche que quand vous lui ouvrer un écran => vous rentrez dans un processus de chantage et risque que votre enfant devient dépendant aux écrans<ul><li>Votre enfant a besoin de votre attention, de vos paroles et de votre réaction<ul><li>Calez-le bien assis, à la bonne hauteur avec un grand bavoir</li><li>Laissez-lui des choses à toucher</li><li>Donnez-lui une cuillère</li><li>Pendant qu’il explore poursuivre le repas</li><li>Parle-lui dans votre langue maternelle</li></ul></li></ul></li><li>Il ne faut pas utiliser son propre smartphone lorsqu’on donne à manger à son enfant<ul><li>votre enfant a besoin des interactions en 3 dimensions avec une personne.</li></ul><ul><li>Les interactions entre les parents et les tous petits sont tellement important qu’il faut que chaque moment d’éveil soit un moment d’échange à 100%</li></ul></li><li>La télévision allumé c’est<ul><li>Passage des images choquantes pour votre enfant</li></ul><ul><li>Vous êtes moins attentif à votre enfant et à l’échange</li></ul><ul><li>Conversation mal entendue du fait du son </li></ul><ul><li>manger de façon automatique, sans prendre garde aux goûts, aux odeurs et aux textures.</li></ul><ul><li>prendre l’habitude de se remplir au lieu de manger.</li></ul></li></ol>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p>Repas sans télévision bénéfice</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Un moment d’échange => les enfants apprennent à s’exprimer => amélioration de leur expression orale</li><li>Apprendre les règles de vie (attendre son pour  parler, bien se tenir, …)</li><li>Apprendre la motricité fine avec le toucher</li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><span class="has-inline-color has-vivid-red-color">Risque =></span></strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>En regardant la télévision l’enfant ne s’aperçoit pas de ce qu’il mange, et donc il se remplit<ul><li><span class="has-inline-color has-vivid-red-color">Risque d’obésité  => IMC augmente pour chaque heure passer devant un écran pour un enfant &lt; 2ans</span></li></ul></li><li>Image choquante pour l’enfant<ul><li><span class="has-inline-color has-vivid-red-color">Risque de replis sur soi</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’Isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque de Violence</span></li></ul></li><li>Echange pauvre avec les parents<ul><li><span class="has-inline-color has-vivid-red-color">Trouble du langage et de communication</span></li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "1" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire allume_ecran_cause_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list {"ordered":true,"type":"1"} -->
            <ol type="1"><li>Si votre enfant refuse de manger et n’ouvre la bouche que quand vous lui ouvrer un écran => vous rentrez dans un processus de chantage et risque que votre enfant devient dépendant aux écrans<ul><li>Votre enfant a besoin de votre attention, de vos paroles et de votre réaction<ul><li>Calez-le bien assis, à la bonne hauteur avec un grand bavoir</li><li>Laissez-lui des choses à toucher</li><li>Donnez-lui une cuillère</li><li>Pendant qu’il explore poursuivre le repas</li><li>Parle-lui dans votre langue maternelle</li></ul></li></ul></li><li>Il ne faut pas utiliser son propre smartphone lorsqu’on donne à manger à son enfant<ul><li>votre enfant a besoin des interactions en 3 dimensions avec une personne.</li><li>Les interactions entre les parents et les tous petits sont tellement important qu’il faut que chaque moment d’éveil soit un moment d’échange à 100%</li></ul></li><li>La télévision allumé c’est<ul><li>Passage des images choquantes pour votre enfant</li></ul><ul><li>Vous êtes moins attentif à votre enfant et à l’échange</li></ul><ul><li>Conversation mal entendue du fait du son </li></ul><ul><li>manger de façon automatique, sans prendre garde aux goûts, aux odeurs et aux textures.</li></ul><ul><li>prendre l’habitude de se remplir au lieu de manger.</li></ul></li></ol>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p>Repas sans télévision bénéfice</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Un moment d’échange => les enfants apprennent à s’exprimer => amélioration de leur expression orale</li><li>Apprendre les règles de vie (attendre son pour  parler, bien se tenir, être serviable, pratiquer les taches manager…)</li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><span class="has-inline-color has-vivid-red-color">Risque =></span></strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>En regardant la télévision l’enfant ne s’aperçoit pas de ce qu’il mange, et donc il se remplit<ul><li><span class="has-inline-color has-vivid-red-color">Risque d’obésité  => IMC augmente pour chaque heure passer devant un écran pour un enfant &lt; 2ans</span></li></ul></li><li>Image choquante pour l’enfant<ul><li><span class="has-inline-color has-vivid-red-color">Risque de replis sur soi</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’Isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque de Violence</span></li></ul></li><li>Echange pauvre avec les parents<ul><li><span class="has-inline-color has-vivid-red-color">Trouble du langage et de communication</span></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "2" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire allume_ecran_cause_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list {"ordered":true,"type":"1"} -->
            <ol type="1"><li>Si votre enfant refuse de manger et n’ouvre la bouche que quand vous lui ouvrer un écran => vous rentrez dans un processus de chantage et risque que votre enfant devient dépendant aux écrans</li><li>Il ne faut pas utiliser son propre smartphone lorsqu’on donne à manger à son enfant<ul><li>votre enfant a besoin des interactions en 3 dimensions avec une personne.</li><li>Les interactions entre les parents et les tous petits sont tellement important qu’il faut que chaque moment d’éveil soit un moment d’échange à 100%</li></ul></li><li>La télévision allumé c’est<ul><li>Passage des images choquantes pour votre enfant</li></ul><ul><li>Vous êtes moins attentif à votre enfant et à l’échange</li></ul><ul><li>Conversation mal entendue du fait du son </li></ul><ul><li>manger de façon automatique, sans prendre garde aux goûts, aux odeurs et aux textures.</li></ul><ul><li>prendre l’habitude de se remplir au lieu de manger.</li></ul></li></ol>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p>Repas sans télévision bénéfice</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Un moment d’échange => les enfants apprennent à s’exprimer => amélioration de leur expression orale</li><li>Apprendre les règles de vie (attendre son pour  parler, bien se tenir, être serviable, pratiquer les taches manager…)</li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><span class="has-inline-color has-vivid-red-color">Risque =></span></strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>En regardant la télévision l’enfant ne s’aperçoit pas de ce qu’il mange, et donc il se remplit<ul><li><span class="has-inline-color has-vivid-red-color">Risque d’obésité  => IMC augmente pour chaque heure passer devant un écran pour un enfant &lt; 2ans</span></li></ul></li><li>Image choquante pour l’enfant<ul><li><span class="has-inline-color has-vivid-red-color">Risque de replis sur soi</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’Isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque de Violence</span></li></ul></li><li>Echange pauvre avec les parents<ul><li><span class="has-inline-color has-vivid-red-color">Trouble du langage et de communication</span></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "3" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire allume_ecran_cause_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list {"ordered":true,"type":"1"} -->
            <ol type="1"><li>Si votre enfant refuse de manger et n’ouvre la bouche que quand vous lui ouvrer un écran => vous rentrez dans un processus de chantage et risque que votre enfant devient dépendant aux écrans</li><li>Il ne faut pas utiliser son propre smartphone lorsqu’on donne à manger à son enfant<ul><li>votre enfant a besoin des interactions en 3 dimensions avec une personne.</li><li>Les interactions entre les parents et les tous petits sont tellement important qu’il faut que chaque moment d’éveil soit un moment d’échange à 100%</li></ul></li><li>La télévision allumé c’est<ul><li>Passage des images choquantes pour votre enfant</li></ul><ul><li>Vous êtes moins attentif à votre enfant et à l’échange</li></ul><ul><li>Conversation mal entendue du fait du son </li></ul><ul><li>manger de façon automatique, sans prendre garde aux goûts, aux odeurs et aux textures.</li></ul><ul><li>prendre l’habitude de se remplir au lieu de manger.</li></ul></li></ol>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p>Repas sans télévision bénéfice</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Un moment d’échange => les enfants apprennent à s’exprimer => amélioration de leur expression orale</li><li>Apprendre les règles de vie (attendre son pour  parler, bien se tenir, être serviable, pratiquer les taches manager…)</li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><span class="has-inline-color has-vivid-red-color">Risque =></span></strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>En regardant la télévision l’enfant ne s’aperçoit pas de ce qu’il mange, et donc il se remplit<ul><li><span class="has-inline-color has-vivid-red-color">Risque d’obésité  => IMC augmente pour chaque heure passer devant un écran pour un enfant &lt; 2ans</span></li></ul></li><li>Image choquante pour l’enfant<ul><li><span class="has-inline-color has-vivid-red-color">Risque de replis sur soi</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’Isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque de Violence</span></li></ul></li><li>Echange pauvre avec les parents<ul><li><span class="has-inline-color has-vivid-red-color">Trouble du langage et de communication</span></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "4" ): ?>
        <div class="alert alert-danger alert-danger-questionnaire allume_ecran_cause_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list {"ordered":true,"type":"1"} -->
            <ol type="1"><li>Si votre enfant refuse de manger et n’ouvre la bouche que quand vous lui ouvrer un écran => vous rentrez dans un processus de chantage et risque que votre enfant devient dépendant aux écrans</li><li>Il ne faut pas utiliser son propre smartphone lorsqu’on donne à manger à son enfant<ul><li>votre enfant a besoin des interactions en 3 dimensions avec une personne.</li><li>Les interactions entre les parents et les tous petits sont tellement important qu’il faut que chaque moment d’éveil soit un moment d’échange à 100%</li></ul></li><li>La télévision allumé c’est<ul><li>Passage des images choquantes pour votre enfant</li></ul><ul><li>Vous êtes moins attentif à votre enfant et à l’échange</li></ul><ul><li>Conversation mal entendue du fait du son </li></ul><ul><li>manger de façon automatique, sans prendre garde aux goûts, aux odeurs et aux textures.</li></ul><ul><li>prendre l’habitude de se remplir au lieu de manger.</li></ul></li></ol>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p>Repas sans télévision bénéfice</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Un moment d’échange =&gt; pour l’adolescent ces moments de partages sont incontournable car il permette<ul><li>D’évaluer leur état d’âme</li></ul><ul><li>D’évaluer leur rapport à la nourriture</li></ul><ul><li>D’évaluer leur état émotionnel</li></ul></li><li>Apprendre les règles de vie (attendre son pour&nbsp; parler, bien se tenir, être serviable, pratiquer les taches manager…)</li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><span class="has-inline-color has-vivid-red-color">Risque =></span></strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>En regardant la télévision l’enfant ne s’aperçoit pas de ce qu’il mange, et donc il se remplit<ul><li><span class="has-inline-color has-vivid-red-color">Risque d’obésité  => IMC augmente pour chaque heure passer devant un écran pour un enfant &lt; 2ans</span></li></ul></li><li>Image choquante pour l’enfant<ul><li><span class="has-inline-color has-vivid-red-color">Risque de replis sur soi</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’Isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque de Violence</span></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <a role="button" data-toggle="collapse" href="#collapse-5" aria-expanded="false" aria-controls="collapse-5">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>

      </div>
    </div>
  </div>


  <div class="card">
    <div class="card-header" id="heading-5">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-5" aria-expanded="false" aria-controls="collapse-5">
            6.	Votre enfant passe-t-il du temps devant un écran le matin ? 
        </a>
      </h5>
    </div>
    <div id="collapse-5" class="collapse" data-parent="#accordion" aria-labelledby="heading-5">
      <div class="card-body">

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"></td>
                <td><label for="oui_ecran_matin">Oui</label><input id="oui_ecran_matin" value="oui" name="ecran_matin" type="radio" /></td>
                <td><label for="non_ecran_matin">Non</label><input id="non_ecran_matin" value="non" name="ecran_matin" type="radio" /></td>
            </tr>
        </table>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "0" ): ?>
        <div class="alert alert-danger ecran_matin_alert alert-danger-questionnaire" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>ce temps est riche pour<ul><li>les apprentissages</li></ul><ul><li>exercer sa motricité fine lors du repas (boire à la tasse, tartiner, couper…) et lors de l’habillage (enfiler, mettre à l’endroit, boutonner …).</li></ul><ul><li>plus concentré en arrivant à l’école s’il a joué et a participé à toutes les activités du matin (toilette, repas, habillage…) sans un œil rivé sur l’écran.</li></ul><ul><li>&nbsp;L’excitation sensorielle auditive et visuelle que subissent vos enfants en regardant la télévision ou la tablette est massive. Quand tout s’arrête, ils sont complètement perdus et la maitresse parait bien fade &nbsp;=&gt; source d’ennui ou au contraire d’agitation.</li></ul><ul><li>La visualisation des écrans le matin vide les batteries de vos enfants =&gt;&nbsp; car les flaches visuels, les séquences rapides et les sens aigues vont puiser dans sa réserve d’attention volontaire.</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Risque :<ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul><ul><li>L’imitation de l’imaginaire</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul><ul><li>Retard des acquisitions<ul><li>Pert de la motricité fine</li></ul><ul><li>Trouble de l’attention et de la concentration</li></ul><ul><li>Trouble du langage et de la communication</li><li>Trouble dans l’apprentissage de l’écriture et les dessins</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "1" ): ?>
        <div class="alert alert-danger ecran_matin_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le matin avant d’aller à la maternelle </strong>&nbsp;: ce temps est riche pour<ul><li>les apprentissages</li></ul><ul><li>exercer sa motricité fine lors du repas (boire à la tasse, tartiner, couper…) et lors de l’habillage (enfiler, mettre à l’endroit, boutonner …).</li></ul><ul><li>plus concentré en arrivant à l’école s’il a joué et a participé à toutes les activités du matin (toilette, repas, habillage…) sans un œil rivé sur l’écran.</li></ul><ul><li>&nbsp;L’excitation sensorielle auditive et visuelle que subissent vos enfants en regardant la télévision ou la tablette est massive. Quand tout s’arrête, ils sont complètement perdus et la maitresse parait bien fade &nbsp;=&gt; source d’ennui ou au contraire d’agitation.</li></ul><ul><li>La visualisation des écrans le matin vide les batteries de vos enfants =&gt;&nbsp; car les flaches visuels, les séquences rapides et les sens aigues vont puiser dans sa réserve d’attention volontaire.</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Risque&nbsp;:<ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul><ul><li>L’imitation de l’imaginaire</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul><ul><li>Retard des acquisitions<ul><li>Pert de la motricité fine</li></ul><ul><li>Trouble de l’attention et de la concentration</li></ul><ul><li>Trouble du langage et de la communication</li></ul><ul><li>Trouble dans l’apprentissage de l’écriture et les dessins</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "2" ): ?>
        <div class="alert alert-danger ecran_matin_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le matin avant l’école</strong>&nbsp;: ce temps est riche pour<ul><li>les apprentissages</li></ul><ul><li>exercer sa motricité fine lors du repas (boire à la tasse, tartiner, couper…) et lors de l’habillage (enfiler, mettre à l’endroit, boutonner …).</li></ul><ul><li>plus concentré en arrivant à l’école s’il a joué et a participé à toutes les activités du matin (toilette, repas, habillage…) sans un œil rivé sur l’écran.</li></ul><ul><li>&nbsp;L’excitation sensorielle auditive et visuelle que subissent vos enfants en regardant la télévision ou la tablette est massive. Quand tout s’arrête, ils sont complètement perdus et la maitresse parait bien fade &nbsp;=&gt; source d’ennui ou au contraire d’agitation.</li></ul><ul><li>La visualisation des écrans le matin vide les batteries de vos enfants =&gt;&nbsp; car les flaches visuels, les séquences rapides et les sens aigues vont puiser dans sa réserve d’attention volontaire.</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Risque&nbsp;:<ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul><ul><li>L’imitation de l’imaginaire</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul><ul><li>Retard des acquisitions<ul><li>Pert de la motricité fine</li></ul><ul><li>Trouble de l’attention et de la concentration</li></ul><ul><li>Trouble du langage et de la communication</li></ul><ul><li>Trouble dans l’apprentissage de l’écriture et les dessins</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "3" ): ?>
        <div class="alert alert-danger ecran_matin_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le matin avant l’école</strong> : ce temps est riche pour<ul><li>les apprentissages</li></ul><ul><li>exercer sa motricité fine lors du repas (boire à la tasse, tartiner, couper…) et lors de l’habillage (enfiler, mettre à l’endroit, boutonner …).</li></ul><ul><li>plus concentré en arrivant à l’école s’il a joué et a participé à toutes les activités du matin (toilette, repas, habillage…) sans un œil rivé sur l’écran.</li></ul><ul><li> L’excitation sensorielle auditive et visuelle que subissent vos enfants en regardant la télévision ou la tablette est massive. Quand tout s’arrête, ils sont complètement perdus et la maitresse parait bien fade  => source d’ennui ou au contraire d’agitation.</li></ul><ul><li>La visualisation des écrans le matin vide les batteries de vos enfants =>  car les flaches visuels, les séquences rapides et les sens aigues vont puiser dans sa réserve d’attention volontaire.</li></ul></li><li>Risque :<ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul><ul><li>L’imitation de l’imaginaire</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul><ul><li>Retard des acquisitions<ul><li>Pert de la motricité fine</li></ul><ul><li>Trouble de l’attention et de la concentration</li><li>Trouble du langage et de la communication</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "4" ): ?>
        <div class="alert alert-danger ecran_matin_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le matin avant le collège et lycée </strong>: ce temps est riche pour<ul><li>les apprentissages</li></ul><ul><li>plus concentré en arrivant à l’école s’il a joué et a participé à toutes les activités du matin (toilette, repas, habillage…) sans un œil rivé sur l’écran.</li></ul><ul><li>&nbsp;L’excitation sensorielle auditive et visuelle que subissent vos enfants en regardant la télévision ou la tablette est massive. Quand tout s’arrête, ils sont complètement perdus et les professeurs&nbsp; paraissent&nbsp; bien moins intéressant &nbsp;&nbsp;=&gt; source d’ennui ou au contraire d’agitation.</li></ul><ul><li>La visualisation des écrans le matin vide les batteries de vos enfants =&gt;&nbsp; car les flaches visuels, les séquences rapides et les sens aigues vont puiser dans sa réserve d’attention volontaire.</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Risque :<ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul><ul><li>Retard des acquisitions<ul><li>Trouble de l’attention et de la concentration</li><li>Trouble du langage et de la communication</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>
        

        <a role="button" data-toggle="collapse" href="#collapse-6" aria-expanded="false" aria-controls="collapse-6">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>

      </div>

    </div>
  </div>


  <div class="card">
    <div class="card-header" id="heading-6">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-6" aria-expanded="false" aria-controls="collapse-6">
        8.	Votre enfant passe-t-il du temps devant un écran avant de se coucher ?
        </a>
      </h5>
    </div>
    <div id="collapse-6" class="collapse" data-parent="#accordion" aria-labelledby="heading-6">
      <div class="card-body">

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"></td>
                <td><label for="oui_ecran_avant_coucher">Oui</label><input id="oui_ecran_avant_coucher" value="oui" name="ecran_avant_coucher" type="radio" /></td>
                <td><label for="non_ecran_avant_coucher">Non</label><input id="non_ecran_avant_coucher" value="non" name="ecran_avant_coucher" type="radio" /></td>
            </tr>
        </table>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "0" ): ?>
        <div class="alert alert-danger ecran_avant_coucher_alert alert-danger-questionnaire" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas d’écrans</strong></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "1" ): ?>
        <div class="alert alert-danger ecran_avant_coucher_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le soir au moins une heure avant le coucher</strong>&nbsp;:<ul><li>&nbsp;la lumière bleue des écrans inhibe la mélatonine (hormone qui nous aide à dormir)</li></ul><ul><li>les images excitantes retardent l’endormissement.</li></ul><ul><li>Certains jeux ou images visualisés peuvent perturber la qualité du sommeil, entrainer des réveils nocturnes, des cauchemars …</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><span class="has-inline-color has-vivid-red-color">Risque</span><ul><li><span class="has-inline-color has-vivid-red-color">Impact sur la santé</span><ul><li><span class="has-inline-color has-vivid-red-color">Myopie</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Surpoids –obésité</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’addiction</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Trouble du sommeil</span></li></ul></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Effet psycho-sociale</span><ul><li><span class="has-inline-color has-vivid-red-color">Passivité</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Repli sur soi, isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Violence</span></li></ul></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Influence sur notre mode de vie et nos consommations</span></li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Solution<ul><li>Il faut favoriser les jeux calmes<ul><li>Lecture</li><li>Jeux de société</li><li>Puzzle…</li></ul></li><li>Introduire un rituel de coucher qu’il faudra lui expliquer lors d’un moment calme par exemple lors  d’un bain</li><li>Expliquer à votre enfant comment fonctionne l’endormissement</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><em><span class="has-inline-color has-vivid-cyan-blue-color">Votre enfant a besoin de 12h de sommeil /jours</span></em></strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p><em><b><span class="has-inline-color has-vivid-cyan-blue-color">La sieste est nécessaire le plus souvent </span></b></em><strong><em><span class="has-inline-color has-vivid-cyan-blue-color">jusqu’à 4ans et demi</span></em></strong></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "2" ): ?>
        <div class="alert alert-danger ecran_avant_coucher_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le soir au moins une heure avant le coucher</strong>&nbsp;:<ul><li>&nbsp;la lumière bleue des écrans inhibe la mélatonine (hormone qui nous aide à dormir)</li></ul><ul><li>les images excitantes retardent l’endormissement.</li></ul><ul><li>Certains jeux ou images visualisés peuvent perturber la qualité du sommeil, entrainer des réveils nocturnes, des cauchemars …</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><span class="has-inline-color has-vivid-red-color">Risque</span><ul><li><span class="has-inline-color has-vivid-red-color">Impact sur la santé</span><ul><li><span class="has-inline-color has-vivid-red-color">Myopie</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Surpoids –obésité</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’addiction</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Trouble du sommeil</span></li></ul></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Effet psycho-sociale</span><ul><li><span class="has-inline-color has-vivid-red-color">Passivité</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Repli sur soi, isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Violence</span></li></ul></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Influence sur notre mode de vie et nos consommations</span></li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Solution<ul><li>Il faut favoriser les jeux calmes<ul><li>Lecture<ul><li>Jeux de societe</li></ul><ul><li>Puzzle…</li></ul></li></ul><ul><li>Introduire un rituel de coucher qu’il faudra lui expliquer lors d’un moment calme par example lors&nbsp; d’un bain</li></ul><ul><li>Expliquer à votre enfant comment fonctionne l’endormissement</li></ul></li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><em><strong><em><span class="has-inline-color has-vivid-cyan-blue-color">Votre enfant a besoin de 10-12h de sommeil /jours</span></em></strong></em></strong></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "3" ): ?>
        <div class="alert alert-danger ecran_avant_coucher_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le soir au moins une heure avant le coucher</strong>&nbsp;:<ul><li>&nbsp;la lumière bleue des écrans inhibe la mélatonine (hormone qui nous aide à dormir)</li></ul><ul><li>les images excitantes retardent l’endormissement.</li></ul><ul><li>Certains jeux ou images visualisés peuvent perturber la qualité du sommeil, entrainer des réveils nocturnes, des cauchemars …</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><strong>Risque</strong><ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul><ul><li>Trouble du sommeil</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><strong>Solution</strong><ul><li>Il faut favoriser les jeux calmes<ul><li>Lecture<ul><li>Jeux de société</li></ul><ul><li>Puzzle…</li></ul></li></ul><ul><li>Introduire un rituel de coucher qu’il faudra lui expliquer lors d’un moment calme par exemple lors  d’un bain</li></ul><ul><li>Expliquer à votre enfant comment fonctionne l’endormissement</li></ul></li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><em><span class="has-inline-color has-vivid-cyan-blue-color">Votre enfant a besoin de 12h de sommeil /jours</span></em></strong></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "3" ): ?>
        <div class="alert alert-danger ecran_avant_coucher_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le soir au moins une heure avant le coucher</strong>&nbsp;:<ul><li>&nbsp;la lumière bleue des écrans inhibe la mélatonine (hormone qui nous aide à dormir)</li></ul><ul><li>les images excitantes retardent l’endormissement.</li></ul><ul><li>Certains jeux ou images visualisés peuvent perturber la qualité du sommeil, entrainer des réveils nocturnes, des cauchemars …</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><strong>Risque</strong><ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul><ul><li>Trouble du sommeil</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><strong>Solution</strong><ul><li>Il faut favoriser les jeux calmes<ul><li>Lecture</li><li>Jeux de société</li><li>Puzzle…</li></ul></li><li>Introduire un rituel de coucher qu’il faudra lui expliquer lors d’un moment calme par exemple lors  d’un bain</li><li>Expliquer à votre enfant comment fonctionne l’endormissement</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><em><span class="has-inline-color has-vivid-cyan-blue-color">Votre enfant a besoin de 10-12h de sommeil /jours</span></em></strong></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $_SESSION['tranche_age'] == "4" ): ?>
        <div class="alert alert-danger ecran_avant_coucher_alert alert-danger-questionnaire">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas le soir au moins une heure avant le coucher</strong>&nbsp;:<ul><li>&nbsp;la lumière bleue des écrans inhibe la mélatonine (hormone qui nous aide à dormir)</li></ul><ul><li>les images excitantes retardent l’endormissement.</li></ul><ul><li>Certains jeux ou images visualisés peuvent perturber la qualité du sommeil, entrainer des réveils nocturnes, des cauchemars …</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><strong>Risque</strong><ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul><ul><li>Trouble du sommeil</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><strong>Solution</strong><ul><li>Les informés de la physiologie de l’endormissement et de leur besoin en sommeil</li><li>Il faudra imposer des règles<ul><li>Poser les écrans dans un lieu commun de la maison à une heure convenue</li></ul><ul><li>Mettre le téléphone en mode avion</li></ul><ul><li> …</li></ul></li><li>Lui acheter un réveil</li><li>Faire de la lecture sur un vrai livre</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p><strong><em><span class="has-inline-color has-vivid-cyan-blue-color">Votre enfant a besoin de 9-10h de sommeil /jours</span></em></strong></p>
            <!-- /wp:paragraph -->
        </div>
        <?php endif; ?>
        

        <a role="button" data-toggle="collapse" href="#collapse-7" aria-expanded="false" aria-controls="collapse-3">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>

      </div>

    </div>
  </div>

  <!-- Question 8 -->
  <div class="card">
    <div class="card-header" id="heading-7">
      <h5 class="mb-0">
        <a role="button" data-toggle="collapse" href="#collapse-7" aria-expanded="false" aria-controls="collapse-7">
            8.	Votre enfant a-il  dans sa chambre ? 
        </a>
      </h5>
    </div>
    <div id="collapse-7" class="collapse " data-parent="#accordion" aria-labelledby="heading-7">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Un ordinateur </lable></td>
                <td><label for="oui_chambre_ordinateur">Oui</label><input id="oui_chambre_ordinateur" value="oui" name="chambre_ordinateur" type="radio" /></td>
                <td><label for="non_chambre_ordinateur">Non</label><input id="non_chambre_ordinateur" value="non" name="chambre_ordinateur" type="radio" /></td>
            </tr>
        </table>
        
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Une TV: </lable></td>
                <td><label for="oui_chambre_television">Oui</label><input id="oui_chambre_television" value="oui" name="chambre_television" type="radio" /></td>
                <td><label for="non_chambre_television">Non</label><input id="non_chambre_television" value="non" name="chambre_television" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Y apporte-t-il un ou des écrans mobiles ?</lable></td>
                <td><label for="oui_chambre_ecran_mobile">Oui</label><input id="oui_chambre_ecran_mobile" value="oui" name="chambre_ecran_mobile" type="radio" /></td>
                <td><label for="non_chambre_ecran_mobile">Non</label><input id="non_chambre_ecran_mobile" value="non" name="chambre_ecran_mobile" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	S’endort-il devant ?</lable></td>
                <td><label for="oui_chambre_ecran_endormir">Oui</label><input id="oui_chambre_ecran_endormir" value="oui" name="chambre_ecran_endormir" type="radio" /></td>
                <td><label for="non_chambre_ecran_endormir">Non</label><input id="non_chambre_ecran_endormir" value="non" name="chambre_ecran_endormir" type="radio" /></td>
            </tr>
        </table>
        
      
        <?php if($age_saisie && !$age_apres_3_ans): ?>
        <div class="alert alert-danger alert-danger-questionnaire chambre_ecran_alert" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li><strong>Pas d’écrans</strong></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>
        <?php if($age_saisie && $age_apres_3_ans) : ?>
        <div class="alert alert-danger alert-danger-questionnaire chambre_ecran_alert" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Pas d’écrans dans la chambre quel que soit l’âge de votre enfant &nbsp;</p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>S’il y a des écrans dans la chambre, vous ne pourriez ni contrôlé le temps passé, ni le contenu</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li><strong>Risque :</strong><ul><li>Impact sur la santé<ul><li>Myopie</li></ul><ul><li>Surpoids –obésité</li></ul><ul><li>Risque d’addiction</li></ul><ul><li>Trouble du sommeil</li></ul></li></ul><ul><li>Effet psycho-sociale<ul><li>Passivité</li></ul><ul><li>Repli sur soi, isolement</li></ul><ul><li>Violence</li></ul><ul><li>L’imitation de l’imaginaire</li></ul></li></ul><ul><li>Influence sur notre mode de vie et nos consommations</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><strong>Solution</strong><ul><li>Aucuns écrans dans la chambre, les écrans nomades  doivent être rangé dans un lieu dédié commun à la famille</li></ul><ul><li>Si votre a besoin de faire un recherche sur internet pour l’école, il faudra toujours être à proximité</li></ul><ul><li>Utiliser les contrôles parentaux</li></ul><ul><li>Fermer la wifi</li></ul><ul><li>Discussion et information de votre  enfant</li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <a role="button" data-toggle="collapse" href="#collapse-8" aria-expanded="false" aria-controls="collapse-8">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>
        
      </div>
    </div>
  </div>

  <!-- Question 9 -->
  <div class="card">
    <div class="card-header" id="heading-8">
      <h5 class="mb-0">
        <a role="button" data-toggle="collapse" href="#collapse-8" aria-expanded="false" aria-controls="collapse-8">
        9.	Est-ce que votre enfant regarde et écoute habituellement :
        </a>
      </h5>
    </div>
    <div id="collapse-8" class="collapse " data-parent="#accordion" aria-labelledby="heading-8">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Les dessins animés : </lable></td>
                <td><label for="seul_dessin_anime">Seul</label><input id="seul_dessin_anime" value="seul" name="dessin_anime" type="radio" /></td>
                <td><label for="avec_vous_dessin_anime">Avec vous</label><input id="avec_vous_dessin_anime" value="avec_vous" name="dessin_anime" type="radio" /></td>
            </tr>
        </table>
        
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Les programmes éducatifs : </lable></td>
                <td><label for="seul_programme_educatif">Seul</label><input id="seul_programme_educatif" value="seul" name="programme_educatif" type="radio" /></td>
                <td><label for="avec_vous_programme_educatif">Avec vous</label><input id="avec_vous_programme_educatif" value="avec_vous" name="programme_educatif" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Les vidéos : </lable></td>
                <td><label for="seul_video">Seul</label><input id="seul_video" value="seul" name="video" type="radio" /></td>
                <td><label for="avec_vous_video">Avec vous</label><input id="avec_vous_video" value="avec_vous" name="video" type="radio" /></td>
            </tr>
        </table>


        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Des programmes non destinés aux enfants : </lable></td>
                <td><label for="seul_programme_non_enfant">Seul</label><input id="seul_programme_non_enfant" value="seul" name="programme_non_enfant" type="radio" /></td>
                <td><label for="avec_vous_programme_non_enfant">Avec vous</label><input id="avec_vous_programme_non_enfant" value="avec_vous" name="programme_non_enfant" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Jeux video : </lable></td>
                <td><label for="seul_jeu_video">Seul</label><input id="seul_jeu_video" value="seul" name="jeu_video" type="radio" /></td>
                <td><label for="avec_vous_jeu_video">Avec vous</label><input id="avec_vous_jeu_video" value="avec_vous" name="jeu_video" type="radio" /></td>
            </tr>
        </table>
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Internet : </lable></td>
                <td><label for="seul_internet">Seul</label><input id="seul_internet" value="seul" name="internet" type="radio" /></td>
                <td><label for="avec_vous_internet">Avec vous</label><input id="avec_vous_internet" value="avec_vous" name="internet" type="radio" /></td>
            </tr>
        </table>
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Autre :  <input type="text" name="autre_programmes" /></lable></td>
                <td><label for="seul_autre">Seul</label><input id="seul_autre" value="seul" name="autre" type="radio" /></td>
                <td><label for="avec_vous_autre">Avec vous</label><input id="avec_vous_autre" value="avec_vous" name="autre" type="radio" /></td>
            </tr>
        </table>
        

        <?php if($age_saisie && $age_apres_3_ans) : ?>
        <div class="alert alert-danger alert-danger-questionnaire enfant_regarde_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>Il faudra contrôler les contenus visionnés &nbsp;=&gt; il faut un contenu adapté à l’âge</li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Les programmes destinés aux adultes n’apporte rien à votre enfant => ce du temps volé à toutes les autres activités</li><li>Risque de violence visuel => les petits enfants n’ont pas les capacités émotionnelles de comprendre des images qui ne sont pourtant ni violentes ni choquantes</li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>Les jeux vidéo<ul><li>soyez vigilant à l’âge conseillé</li></ul><ul><li>pas de jeu vidéo avant 6 ans</li></ul><ul><li>regarder quelque minute avec votre enfant le but du jeu et le rythme</li></ul><ul><li>utiliser le site PEGI ( Pan European Game Information) pour avoir l’âge pour lequel le jeu est préconisé <a href="https://pegi.info/fr">https://pegi.info/fr</a></li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li>internet<ul><li>Attention en France les sites pornographiques sont accessibles à tous, sans aucune restriction</li></ul><ul><li>Pas d’internet avant 9 ans</li></ul><ul><li>Il faudra poser des limites</li></ul><ul><li>Contrôlé ce que votre enfant regarde</li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:list -->
            <ul><li><span class="has-inline-color has-vivid-red-color">Risque :</span><ul><li><span class="has-inline-color has-vivid-red-color">Impact sur la santé</span><ul><li><span class="has-inline-color has-vivid-red-color">Myopie</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Surpoids –obésité</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Risque d’addiction</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Trouble du sommeil</span></li></ul></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Effet psycho-sociale</span><ul><li><span class="has-inline-color has-vivid-red-color">Passivité</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Repli sur soi, isolement</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Violence</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">L’imitation de l’imaginaire</span></li></ul></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Influence sur notre mode de vie et nos consommations</span></li></ul><ul><li><span class="has-inline-color has-vivid-red-color">Violence  visuelle</span></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <a role="button" data-toggle="collapse" href="#collapse-9" aria-expanded="false" aria-controls="collapse-9">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>
        
      </div>
    </div>
  </div>


  <!-- Question 10 -->
  <div class="card">
    <div class="card-header" id="heading-9">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-9" aria-expanded="false" aria-controls="collapse-9">
            10.	Est-ce que vous regardez régulièrement en famille la télévision ?
        </a>
      </h5>
    </div>
    <div id="collapse-9" class="collapse" data-parent="#accordion" aria-labelledby="heading-9">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"></td>
                <td>
                    <label for="oui_television_famille">Oui</label><input id="oui_television_famille" value="oui" name="television_famille" type="radio" />
                </td>
                <td><label for="non_television_famille">Non</label><input id="non_television_famille" value="non" name="television_famille" type="radio" /></td>
            </tr>
        </table>

        <hr/>
        <div class="television_famille_extra">
            <label> Ces moments sont-ils l’occasion d’échanges avec votre enfant ? </label>
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="oui_echange_television_famille">Oui</label></td>
                    <td><input id="oui_echange_television_famille" value="oui" name="echange_television_famille" type="radio" /></td>
                </tr>
            </table>
            <table class="table_oui_non_reponse">
                <tr>
                    <td class="first"><label for="non_echange_television_famille">Non</label></td>
                    <td><input id="non_echange_television_famille" value="non" name="echange_television_famille" type="radio" /></td>
                </tr>
            </table>
        </div>
         
        <!-- réponse non -->
        <div class="alert alert-warning alert-danger-questionnaire non_television_famille_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p><strong>Bien mais :</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Il est possible de regarder la télévision avec son enfant à condition que le contenu soi adapté à son âge pour limiter la violence visuelles</li><li>Et il faudra échanger avec votre enfant sur le contenu, vous devez avoir un rôle actif</li></ul>
            <!-- /wp:list -->
        </div>

        <!-- réponse oui -->
        <div class="alert alert-success alert-danger-questionnaire oui_television_famille_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
            <!-- /wp:paragraph -->
        </div>

        <!-- réponse oui puis non pour la seconde -->
        <div class="alert alert-danger alert-danger-questionnaire oui_non_television_famille_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Il est important d’échanger avec votre enfant ce qu’il regarde à la télévision.</p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>C’est un moment de partage.</p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>Rapporte les effets virtuels à la réalité pour réduire le déficit de transfert ou vidéo</p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>Devenez acteur en commentant et en interrogeant</p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>Vous devez avoir un rôle actif Regarder la télévision avec son enfant sa reste un moment pédagogique</p>
            <!-- /wp:paragraph -->
        </div>


        <a role="button" data-toggle="collapse" href="#collapse-10" aria-expanded="false" aria-controls="collapse-10">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>
      </div>
    </div>
  </div>

  <!-- Question 11 -->
  <div class="card">
    <div class="card-header" id="heading-10">
      <h5 class="mb-0">
        <a class="collapsed" role="button" data-toggle="collapse" href="#collapse-10" aria-expanded="false" aria-controls="collapse-10">
        11. Votre famille s’est-elle dotée de règles ou de directives relatives à l’utilisation des écrans ? Sont-elles faciles à faire respecter ?
        </a>
      </h5>
    </div>
    <div id="collapse-10" class="collapse" data-parent="#accordion" aria-labelledby="heading-10">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"></td>
                <td>
                    <label for="oui_regle_ecran">Oui</label><input id="oui_regle_ecran" value="oui" name="regle_ecran" type="radio" />
                </td>
                <td><label for="non_regle_ecran">Non</label><input id="non_regle_ecran" value="non" name="regle_ecran" type="radio" /></td>
            </tr>
        </table>

        <!-- réponse non -->
        <div class="alert alert-danger alert-danger-questionnaire non_regle_ecran_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Il est important de mettre en place des règles pour la bonne pratique des écrans</p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>Il existe la «&nbsp;Family Media Use Plan&nbsp;», qui &nbsp;est un outil de synthèse développé par L’Académie Américaine de Pédiatrie pour aider les familles à se positionner sur les différents sujets des écrans dans la famille et d’impliquer toute la famille sur les bonne régles à suivre.</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Tout y est, ce qui permet de pouvoir évoquer tous les sujets en famille et de s’accorder sur les points essentiels.</li><li>c’est un document personnalisable en ligne selon les habitudes familiales et l’âge des enfants.</li><li> Une fois rempli par tous les membres de la famille, il est possible de l’imprimer pour l’afficher dans la cuisine sur le réfrigérateur..</li><li>Pour les anglophones et hispanophones il est possible de le remplir en ligne <a href="https://www.healthychildren.org/English/media/Pages/default.aspx">en cliquant ici</a></li><li>Pour les francophones<ul><li>En cliquant ici pour <a href="http://www.surexpositionecrans.org/wp-content/uploads/2018/05/Charte-familiale-moins-de-cinq-ans-cose.pdf">la charte familiale du moins de cinq ans</a></li><li>En cliquant ici pour<a href="http://www.surexpositionecrans.org/wp-content/uploads/2018/05/Charte-familiale-plus-de-cinq-ans-cose.pdf"> la charte familiale du plus de cinq ans</a></li></ul></li></ul>
            <!-- /wp:list -->

            <!-- wp:paragraph -->
            <p></p>
            <!-- /wp:paragraph -->
        </div>

        <!-- réponse oui -->
        <div class="alert alert-success alert-danger-questionnaire oui_regle_ecran_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
            <!-- /wp:paragraph -->
        </div>

  

        <a role="button" data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>
      </div>
    </div>
  </div>

  <!-- Question 14 -->
  <div class="card">
    <div class="card-header" id="heading-11">
      <h5 class="mb-0">
        <a role="button" data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">
            Pensez-vous que l’utilisation des écrans :
        </a>
      </h5>
    </div>
    <div id="collapse-11" class="collapse " data-parent="#accordion" aria-labelledby="heading-11">
      <div class="card-body">
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Développe l’éveil et les apprentissages ?</lable></td>
                <td><label for="oui_eveil_apprentissage">Oui</label><input id="oui_eveil_apprentissage" value="oui" name="eveil_apprentissage" type="radio" /></td>
                <td><label for="non_eveil_apprentissage">Non</label><input id="non_eveil_apprentissage" value="non" name="eveil_apprentissage" type="radio" /></td>
            </tr>
        </table>
        
        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Aide votre enfant à se calmer ?</lable></td>
                <td><label for="oui_calme_enfant">Oui</label><input id="oui_calme_enfant" value="oui" name="calme_enfant" type="radio" /></td>
                <td><label for="non_calme_enfant">Non</label><input id="non_calme_enfant" value="non" name="calme_enfant" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Améliore son attention et sa concentration  ?</lable></td>
                <td><label for="oui_ameliore_attention">Oui</label><input id="oui_ameliore_attention" value="oui" name="ameliore_attention" type="radio" /></td>
                <td><label for="non_ameliore_attention">Non</label><input id="non_ameliore_attention" value="non" name="ameliore_attention" type="radio" /></td>
            </tr>
        </table>

        <table class="table_oui_non_reponse">
            <tr>
                <td class="first"><lable>✓	Le prépare au monde de demain  ?</lable></td>
                <td><label for="oui_monde_demain">Oui</label><input id="oui_monde_demain" value="oui" name="monde_demain" type="radio" /></td>
                <td><label for="non_monde_demain">Non</label><input id="non_monde_demain" value="non" name="monde_demain" type="radio" /></td>
            </tr>
        </table>

        <?php if($age_saisie && !$age_apres_3_ans) : ?>
        <div class="alert alert-danger alert-danger-questionnaire oui_eveil_apprentissage_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
           <!-- wp:paragraph -->
            <p>Développe l’éveil et les apprentissages</p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>l’écran n’apporte rien du au phénomène du déficit du transfert</li><li>Mettre votre enfant devant un écran à cet âge ce lui voler du temps pour son développement psychomoteur </li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $age_apres_3_ans) : ?>
        <div class="alert alert-danger alert-danger-questionnaire oui_eveil_apprentissage_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
           <!-- wp:paragraph -->
            <p>Développe l’éveil et les apprentissages : </p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Attention il faudra respecter la durée conseiller pour la quel votre enfant peut utiliser les écrans car une surexposition aux écrans engendre des troubles importants ( cf fiche effet 2md) </li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>
        <!-- réponse non -->
        <div class="alert alert-success alert-danger-questionnaire non_eveil_apprentissage_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
            <!-- /wp:paragraph -->
        </div>



        <div class="alert alert-danger alert-danger-questionnaire oui_calme_enfant_alert" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Aide votre enfant à se calmer : </p>
            <!-- /wp:paragraph -->

            <!-- wp:list -->
            <ul><li>Attention calmer votre enfants par un écran ceci risque de le rendre addict  aux écrans (cf fiche)</li><li>Conduite à tenir pour calmer les pleurs<ul><li>Parlez-lui pour le calmer<ul><li>en gardant votre calme</li><li>se  mettre vous a sa hauteur</li><li>parlez-lui normalement</li><li>rassurez-le en le caressant</li><li>L’enfant réagit par mimétisme</li></ul></li><li>Détournez son attention<ul><li>Détournez son attention avec humour ou en parlant d’un autre sujet</li><li>Prenez un petit sac avec des jouer ou des livres de quoi l’occuper</li></ul></li><li>Si la situation est tendue sortez à l’extérieur et foire plus au moins une promenade pour faire oublier les écrans</li></ul></li></ul>
            <!-- /wp:list -->
        </div>

        <!-- réponse non -->
        <div class="alert alert-success alert-danger-questionnaire non_calme_enfant_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
            <!-- /wp:paragraph -->
        </div>



        <?php if($age_saisie && !$age_apres_3_ans) : ?>
        <div class="alert alert-danger alert-danger-questionnaire oui_ameliore_attention_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>Pour les enfants  &lt; 3 ans  pour lesquels il y a le phénomène du déficit du transfert<ul><li>Des études ont prouvé que exposer son enfant aux écrans a cet âge ce induire<ul><li>Un trouble du langage, de communication</li><li>Un trouble de concentration et d’attention</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <?php if($age_saisie && $age_apres_3_ans) : ?>
        <div class="alert alert-danger alert-danger-questionnaire oui_ameliore_attention_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>pour les enfants > 3 ans pour lesquels le déficit du transfert diminue avec l’âge<ul><li>Si on ne respecte pas la durée limite à l’exposition des écrans et contrôler  le contenu  ceci induire<ul><li>des trouble de l’attention et de concentration et bien plus encore (cf effet 2<sup>nd</sup>)</li><li>trouble visuel</li><li>violence visuelle</li></ul></li></ul></li></ul>
            <!-- /wp:list -->
        </div>
        <?php endif; ?>

        <!-- réponse non -->
        <div class="alert alert-success alert-danger-questionnaire non_ameliore_attention_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
            <!-- /wp:paragraph -->
        </div>



        <div class="alert alert-danger alert-danger-questionnaire oui_monde_demain_alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:list -->
            <ul><li>Aucune étude n’a prouvé que les enfants moins exposer aux écrans avaient des problèmes dans la vie adulte mais tous le contraire un enfant sur exposée aux écrans développera plusieurs trouble  ( cf effet 2<sup>nd</sup> )</li></ul>
            <!-- /wp:list -->
        </div>
        <div class="alert alert-success alert-danger-questionnaire non_monde_demain_alert" >
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <!-- wp:paragraph -->
            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
            <!-- /wp:paragraph -->
        </div>
       


        <a role="button" data-toggle="collapse" href="#collapse-12" aria-expanded="false" aria-controls="collapse-12">
            <button type="button" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Suivant</button>
        </a>
        
      </div>
    </div>
  </div>
</div>


<button type="button" class="btn btn-mdb btn-primary waves-effect waves-light comments-load-button">Valider</button>

</form>




<?php get_footer(); ?>