<?php
/**
 * Template Name: page de conseil
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>

<div class="row">
    <div class="col-md-10 offset-md-1">
    
    <div class="mt-25">

    <p class="paragraphe_description"><?= the_title(); ?></p>
    <div class="cadre"  style="margin-top: 25px!important;">   
    <ul>
    <?php
        $questions = new WP_Query([
            'post_type' => 'question'
        ]);
        
        while($questions->have_posts()) : $questions->the_post();
    ?>

            <li class="fiche-conseil titre-conseil rouge-bordeau"><?php the_title(); ?>
                <ul>
                    <li class="titre-conseil">
                        <?php the_content(); ?>
                    </li>
                </ul>
            </li>

        <?php endwhile;  wp_reset_postdata();?>
        </ul>
    </div>

    </div>
    </div>
</div>
<?php get_footer(); ?>