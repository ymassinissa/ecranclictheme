<?php
class GestionAffichePage {
    const GROUP = 'affiche_options';
    public static function register() {
        add_action('admin_menu', [self::class, 'addMenu']);
        add_action('admin_init',[self::class, 'registerSettings']);
    }

    public static function registerSettings() {
        register_setting(self::GROUP, 'affiche_nom');
        add_settings_section('affiche_option_section', 'Paramètres', function() {
            echo 'Vous pouvez ajouter une affiche';
        }, self::GROUP);
        add_settings_field('affiche_nom', "Nom de l'affiche", function() {
            ?>
            <input type="text" name="affiche_nom" value="">
            <?php
        }, self::GROUP, 'affiche_option_section');

    }


    public static function addMenu() {
        add_options_page('Ajouter une affiche', 'Affiches','manage_options',self::GROUP,  [self::class, 'render']);
    }

    public static function render() {
        ?>
        <h1>Affiche</h1>

        <form action="options.php" method="post">
            <?php 
            settings_fields(self::GROUP);
            do_settings_sections(self::GROUP); 
            submit_button();  
            ?>
        </form>

        <?php
    }
}

