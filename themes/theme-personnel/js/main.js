$(document).ready(function() {

    $(".pop").popover({
        trigger: "manual",
        html: true,
        animation: true
      })
      .on("mouseenter", function() {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function() {
          $(_this).popover('hide');
        });
      }).on("mouseleave", function() {
        var _this = this;
        setTimeout(function() {
          if (!$(".popover:hover").length) {
            $(_this).popover("hide");
          }
        }, 300);
      });


    /* $(document).on("mouseover", "#myLink", (event) => {

        $(event.target).tooltip(
            {
                trigger: 'manual',
                delay: { "hide": 500 }
            }).tooltip("show");
    });

    var isOver = false;

    $(document).on("mouseleave", "#myLink", (event) => {
        setTimeout(function () { }, 1000);

        if (!isOver)
            setTimeout(function () { $('#myLink').tooltip('hide'); }, 500);
    });

    $(document).on("mouseover", ".tooltip", (event) => {

        isOver = true;
    });

    $(document).on("mouseleave", ".tooltip", (event) => {

        setTimeout(function () { $('#myLink').tooltip('hide'); }, 500);
    });*/





    $('.btn-number').click(function(e){
        e.preventDefault();
        
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                
                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                } 
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    
    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });

    $('.input-number').change(function() {
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        
    });

    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });



     /****** question 5 radio event ************/
     $('input[name="allume_repas"]').change(function(){
        if($('#oui_allume_repas').prop('checked')){
            $('.allume_repas_extra').slideDown();
        }else{
            $('.allume_repas_extra').slideUp();
        }
    });


    /****** question 4 radio event ************/
    $('input[name="temps_ecran"]').change(function(){
        if($('#oui_temps_ecran').prop('checked')){
            $('.temps_ecran_extra').slideDown();
        }else{
            $('.temps_ecran_extra').slideUp();
        }
    });

    /****** question 10 radio event ************/
    $('input[name="television_famille"]').change(function(){
        if($('#oui_television_famille').prop('checked')){
            $('.television_famille_extra').slideDown();
        }else{
            $('.television_famille_extra').slideUp();
        }
    });

    /****** question 11 radio event ************/
    /* $('input[name="regle_ecran"]').change(function(){
        if($('#oui_regle_ecran').prop('checked')){
            $('.regle_ecran_extra').slideDown();
        }else{
            $('.regle_ecran_extra').slideUp();
        }
    });*/


    /*** stepper */
    if ($('.bs-stepper')[0]) {
        var stepper1 = new Stepper($('.bs-stepper')[0], {
            linear: false,
            animation: true
        });
        $('.prev').click(function () {
            stepper1.previous();
        });
        
        $('.next').click(function () {
            stepper1.next();
        });
    }

    /********* Téléchargement en PDF */
    $('.telecharger_pdf_questionnaire').click(function() {
        var opt = {
            margin:       0.4,
            image:        { type: 'jpeg', quality: 1 },
            html2canvas:  { scale: 2 },
            jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
          };
        html2pdf().from($("#resultat_du_questionnaire").html()).set(opt).save("résultat_questionnaire");
    });

    $('#telecharger_pdf_plus').click(function() {
        html2pdf().from($("#resultat_du_questionnaire_plus").html()).save("résultat_questionnaire_plus");
    });

    
    /******** évenement pour les chart de famille (passer le langue aussi) *******/
    $('.lien-charte').first().next().next().find('a').click(function(e) {
        e.preventDefault();
        $('#form_langue').submit();
    });


   
});


function ImagetoPrint(source){
    return "<html><head><scri"+"pt>function step1(){\n" +
            "setTimeout('step2()', 10);}\n" +
            "function step2(){window.print();window.close()}\n" +
            "</scri" + "pt></head><body onload='step1()'>\n" +
            "<img height='170%' width='100%' src='" + source + "' /></body></html>";
}
// width='827px'
// height='1070px'
function PrintImage(source){
    var Pagelink = "";
    var pwa = window.open(Pagelink, "_new");
    pwa.document.open();
    pwa.document.write(ImagetoPrint(source));
    pwa.document.close();
}


function ImagetoPrinttest(){
    return "<html><head><scri"+"pt>function step1(){\n" +
            "setTimeout('step2()', 10);}\n" +
            "function step2(){window.print();window.close()}\n" +
            "</scri" + "pt>\n"+
            "<style>body {background: red}</style>\n"+
            "</head><body onload='step1()'>\n" +
            $('#resultat_du_questionnaire').html() +
            "</body></html>";
}
// width='827px'
// height='1070px'
function Printtest(){
    var Pagelink = "";
    var pwa = window.open(Pagelink, "_new");
    pwa.document.open();
    pwa.document.write(ImagetoPrinttest());
    pwa.document.close();
}
