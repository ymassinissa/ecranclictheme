<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand logo" href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?>
  <i class="fa fa-hand-pointer-o" id="logo_pointer" aria-hidden="true" style="color: black!important;"></i>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <?php wp_nav_menu([
        'theme_location'=>'header',
        'container' => false,
        'menu_class' => 'navbar-nav mr-auto ml-auto w-100 justify-content-end',
        ]); ?>
    <?php  // get_search_form(); ?>
  </div>
</nav>

    <div class="container">
    
