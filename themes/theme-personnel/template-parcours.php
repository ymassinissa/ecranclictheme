<?php
/**
 * Template Name: page de parcours soins
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>

<?php

/*
	~How To Use~
	[tooltip placement="top" title="This is the title"]This is the content[/tooltip]
*/

function myjournal_tooltip( $atts, $content = null ){ // $atts can be $args etc
	
	// get the attributes
	$atts = shortcode_atts( // this $atts = $args
		array(
			'placement'	=> 'top',
			'title'		=> ''
		),
		$atts, // not $args
		'exptooltip'
	);
	$title = ($atts['title'] == '' ? $content : $atts['title']);
	
	// return HTML
    // return '<span id="myLink" class="myjournal-tooltip" data-html="true" data-toggle="tooltip" data-placement="' . $atts['placement'] . '" title="' . $title . '">' . $content . '</span>';
    // return '<span id="myLink" class="myjournal-tooltip" data-html="true" data-toggle="tooltip" data-placement="' . $atts['placement'] . '" title="' . $title . '">' . $content . '</span>';
	return '<span class="pop" data-container="body" data-toggle="popover" data-placement="' . $atts['placement'] . '" 
    data-content="' . $title . '"
    data-original-title="" title="">' . $content . 
    ' <i class="fa fa-info-circle" aria-hidden="true"></i>'
    .'</span>';
}

add_shortcode('exptooltip', 'myjournal_tooltip');

?>



<div class="row">
    <div class="col-md-10 offset-md-1">

    <p class="paragraphe_description"><?php the_title(); ?></p>
    <div class="cadre" style="margin-top: 25px!important;">

<!--ul>

<?php
    $liens = new WP_Query([
        'post_type' => 'liens'
    ]);
    while($liens->have_posts()) : $liens->the_post();
    $blocks = parse_blocks($post->post_content);
    ?>
    
            <li class="titre-conseil fiche-conseil"><?php the_title(); ?>
                <ul>
                    
                <?php foreach ( $blocks as $bloque ) {
                        $liens_contenue =apply_filters( 'the_content', render_block( $bloque ));
                    
                        if(str_replace(" ", "", $liens_contenue) !== "") {
                            // Cas le bloque a affiché est déjà une liste (ne pas ajouter le tiret)
                            if (strpos($liens_contenue, '<ul>') === 0) {

                                echo $liens_contenue;
                            } else {
                                echo '<li class="titre-conseil">'. $liens_contenue . '</li>';
                            }
                        }
                        
                    }
                ?>
                    
                </ul>
            </li>
    
        <?php endwhile; wp_reset_postdata(); ?>

    </ul-->


    <div class="titre-conseil">
        <?php the_content(); ?>
    </div>



</div>
<!--
<?php  if(have_posts()) : while(have_posts()): the_post(); ?>
    <h1><?php the_title(); ?></h1>
    <br/>
    <br/>
    <p style="text-align: center;">Structure et professionnel de santé qui apporte un soutien et un suivi psychologique  pour les patients souhaitant retrouver un usage raisonnable des écrans</p>
    <div class="cadre" style="width: 600px; margin: 25px auto!important;">
    <?php $bloques = parse_blocks($post->post_content); 
        foreach ( $bloques as $bloque ) {
            echo apply_filters( 'the_content', render_block( $bloque ) );
        }
    ?>
    </div>
<?php endwhile; wp_reset_postdata(); endif; ?>
</ul>
    -->
    </div>
</div>
<?php get_footer(); ?>