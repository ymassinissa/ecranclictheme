<?php get_header(); 




?>  
<div class="row">
<?php  if(have_posts()) : while(have_posts()): the_post(); ?>
 
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php the_title(); ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?php the_category(); ?></h6>
                    <p class="card-text">
                        <?php the_content(); ?>
                    </p>
                </div>
                <a href="<?php the_permalink(); ?>">voir plus</a>
            </div>

        </div>

<?php endwhile; endif;?>
</div>
<?php get_footer(); ?>