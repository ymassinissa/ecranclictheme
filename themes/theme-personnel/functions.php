<?php

function medoc_theme_support() {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');
    register_nav_menu('header', 'En entête du menu');
}


function medoc_register_assets() {
    
    wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');
    wp_enqueue_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('stepper', 'https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css');
    wp_enqueue_script( 'boot1','https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot2','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot3','https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script('jquery-form', "http://malsup.github.com/jquery.form.js", array( 'jquery' ),'',true);
    wp_enqueue_script('stepper', "https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js", array( 'jquery' ),'',true);
    wp_enqueue_script('htmltopdf', "https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js", array( 'jquery' ),'',true);
    // wp_enqueue_script('print', "https://raw.githubusercontent.com/Xportability/css-to-pdf/master/js/xepOnline.jqPlugin.js", array( 'jquery' ),'',true);
    // wp_enqueue_style('prinfff', 'https://printjs-4de6.kxcdn.com/print.min.css');
    
    
    wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style('style_default', get_template_directory_uri() . '/css/default.css');
    wp_enqueue_style('style_hover', get_template_directory_uri() . '/css/hover.css');
    wp_enqueue_style('style_questionnaire', get_template_directory_uri() . '/css/questionnaire.css');
    wp_enqueue_style('style_checkbox', get_template_directory_uri() . '/css/checkbox.css');
    wp_enqueue_style('style_timeline', get_template_directory_uri() . '/css/timeline.css');
    wp_enqueue_style('style_carte', get_template_directory_uri() . '/css/cartes.css');
    wp_enqueue_script('script', get_template_directory_uri() . '/js/main.js?version=1.0.0', array( 'jquery' ),'',true);
    wp_enqueue_script('script2', get_template_directory_uri() . '/js/print.js?version=2.0.0', array( 'jquery' ),'',true);
    // Envoyer une variable de PHP à JS proprement
    // wp_localize_script( 'script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );

}

function medoc_menu_class($classes) {
    $classes[] = 'nav-item';
    return $classes;
}

function medoc_menu_link_class($attr) {
    $attr['class'] = 'nav-link';
    return $attr;
}

function medoc_init() {

    // Affiches 
    register_taxonomy('affiche_details','affiche', [
        'labels' => [
            'name' => 'affiche détails'
        ],
        "show_in_rest" => true,
        "hierarchical" => true 
    ]);
    register_taxonomy('affiche_langues','affiche', [
        'labels' => [
            'name' => 'langue de l\'affiche'
        ],
        "show_in_rest" => true,
        "hierarchical" => true 
    ]);

    register_taxonomy('age_categorie','affiche', [
        'labels' => [
            'name' => 'Catégorie de l\'âge'
        ],
        "show_in_rest" => true,
        "hierarchical" => true 
    ]);
    register_post_type('affiche', [
        'label' => 'affiche',
        'public' => true,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-images-alt',
        'supports' => ['title', 'editor', 'thumbnail'],
        'show_in_rest' => true,
        'has_archive' => true,
    ]);
    
    
    // Les conseils
    register_post_type('question', [
        'label' => 'Conseils',
        'public' => true,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-lightbulb',
        'supports' => ['title', 'editor', 'thumbnail'],
        'show_in_rest' => true,
        'has_archive' => true,
    ]);
    
    //// questions 
    /* register_post_type('questions', [
        'label' => 'Questions',
        'public' => true,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-megaphone',
        'supports' => ['title', 'editor', 'thumbnail'],
        'show_in_rest' => true,
        'has_archive' => true,
    ]);

    register_taxonomy('question_options','questions', [
        'labels' => [
            'name' => 'question possibilitées'
        ],
        "show_in_rest" => true,
        "hierarchical" => true 

    ]); */

    
    //// les liens 
    register_post_type('liens', [
        'label' => 'liens',
        'public' => true,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-admin-links',
        'supports' => ['title', 'editor', 'thumbnail'],
        'show_in_rest' => true,
        'has_archive' => true,
    ]);

    register_taxonomy('liens_categorie','liens', [
        'labels' => [
            'name' => 'catégories des médecins'
        ],
        "show_in_rest" => true,
        "hierarchical" => true 

    ]);

    // Les livres 
    register_post_type('livre', [
        'label' => 'livre',
        'public' => true,
        'menu_position' => 3,
        'menu_icon' => 'dashicons-book-alt',
        'supports' => ['title', 'editor', 'thumbnail'],
        'show_in_rest' => true,
        'has_archive' => true,
    ]);
}

function medoc_register_widget() {
     register_sidebar([
        'id' => 'homepage',
        'name' => 'Sidebar Accueil',
        'before_widget' => '<div class="p-4 %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="font-italic">',
        'after_title' => '</h4>'
    ]);
}



//////////// Actions
add_action('init', 'medoc_init');
add_action('after_setup_theme', 'medoc_theme_support');
add_action('wp_enqueue_scripts', 'medoc_register_assets');
add_action('widgets_init', 'medoc_register_widget');


/////////////// filters
add_filter('nav_menu_css_class', 'medoc_menu_class');
add_filter('nav_menu_link_attributes', 'medoc_menu_link_class');


////////////// imports 
require_once("affiches/options.php");
require_once("metaboxes/pourreponseoui.php");
require_once("metaboxes/pourreponsenon.php");
require_once("metaboxes/questionmere.php");
GestionAffichePage::register();     
// ReponseOuiBox::register();
// ReponseNonBox::register();
// QuestionMereBox::register();