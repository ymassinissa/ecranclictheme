<?php  get_header(); 

function myjournal_tooltip( $atts, $content = null ){ // $atts can be $args etc
	
	// get the attributes
	$atts = shortcode_atts( // this $atts = $args
		array(
			'placement'	=> 'top',
			'title'		=> ''
		),
		$atts, // not $args
		'tooltip'
	);
	$title = ($atts['title'] == '' ? $content : $atts['title']);
	
	// return HTML
	return '<span id="myLink" class="myjournal-tooltip" data-html="true" data-toggle="tooltip" data-placement="' . $atts['placement'] . '" title="' . $title . '">' . $content . '</span>';
	
}

add_shortcode( 'tooltip', 'myjournal_tooltip' );
?>

<?php  if(have_posts()) : while(have_posts()): the_post(); ?>
    <!--h1><?php the_title(); ?></h1-->
    <div class="cadre">
        <?php the_content(); ?>
    </div>
<?php endwhile; endif; ?>

<div class="row">
    <div class="col-md-8">
        <div class="center accueil-questionnaire">
            <form method="POST" action="<?php bloginfo('url'); ?>/index.php/questions">
                <h5 class="rouge-bordeau">Évaluation de la consommation des écrans de votre enfant</h5>
                <hr/>
                <br>
                <label for="age_enfant_ans">Âge de l'enfant:</label>
                <div class="saisie-age-enfant">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <label class="input-label label-age">ans: </label>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number"  data-type="minus" data-field="age_enfant_ans">
                                        <span class="glyphicon glyphicon-minus"></span>
                                        -
                                    </button>
                                </span>
                                <input type="text" id="age_enfant_ans" name="age_enfant_ans" class="form-control input-number" value="1" min="0" max="18">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="age_enfant_ans">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        +
                                    </button>
                                </span> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <label for="age_enfant_mois" class="input-label label-age">mois: </label>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number"  data-type="minus" data-field="age_enfant_mois">
                                        <span class="glyphicon glyphicon-minus"></span>
                                        -
                                    </button>
                                </span>
                                <input type="text" id="age_enfant_mois" name="age_enfant_mois" class="form-control input-number" value="0" min="0" max="11">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="age_enfant_mois">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        +
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <button type="submit" class="btn btn-mdb btn-primary waves-effect waves-light">Commencer l'évaluation</button>
            </form>

      </div>
    <p></p>
     

   
    </div>
    <div class="col-md-4">
        <h5 class="rouge-bordeau">Pour aller plus loin</h5>
        <hr/>
        <aside class="col-md-4 blog-sidebar">
            <?= get_sidebar('homepage'); ?>
        </aside>

    </div>
</div>

<?php get_footer(); ?>