<?php

class QuestionMereBox {

    const META_KEY = 'question_mere';
    const NONCE = '_question_mere_nonce';

    
    public static function register() {
        add_action('add_meta_boxes', [self::class, 'add']);
        // add_action('add_meta_boxes', [self::class, 'add'], 10, 2);
        add_action('save_post', [self::class, 'save']);
    }

    public static function add() {
        add_meta_box(self::META_KEY, 'Question mère', [self::class, 'render'], 'questions');
    }

    public static function render($post) {
        $value = get_post_meta($post->ID, self::META_KEY, true);
        wp_nonce_field(self::NONCE, self::NONCE);
        $questions = new WP_Query([
            'post_type' => 'questions'
        ]);
        echo '<select>';
        echo '<option>Selectionner une option</option>';
        while($questions->have_posts()) : $questions->the_post();
        ?>
            <option value="<?= get_the_ID(); ?>"><?= get_the_ID(); ?></option>
         <?php endwhile; wp_reset_postdata();?>
        </select>
        <?php
    }

    public static function save ($post) {
        if (
            array_key_exists(self::META_KEY, $_POST) && 
            current_user_can('publish_posts', $post) &&
            wp_verify_nonce($_POST[self::NONCE], self::NONCE)
            ) {
                update_post_meta($post, self::META_KEY, $_POST[self::META_KEY]);
            
        }
    }
}