<?php

class ReponseNonBox {
    const META_KEY = 'reponse_non';
    const NONCE = '_reponse_non_nounce';

    public static function register() {
        add_action('add_meta_boxes', [self::class, 'add']);
        // add_action('add_meta_boxes', [self::class, 'add'], 10, 2);
        add_action('save_post', [self::class, 'save']);
    }

    public static function add() {
        add_meta_box(self::META_KEY, 'Réponse pour le non', [self::class, 'render'], 'questions');
    }

    public static function render($post) {
        $value = get_post_meta($post->ID, self::META_KEY, true);
        wp_nonce_field(self::NONCE, self::NONCE);

        ?>

        <label for="<?= self::META_KEY; ?>">La réponse pour le non</label>
        <textarea name='<?= self::META_KEY; ?>'>
        <?= $value; ?>
        </textarea>
       
        <?php
    }

    public static function save ($post) {
        if (
            array_key_exists(self::META_KEY, $_POST) && 
            current_user_can('publish_posts', $post) &&
            wp_verify_nonce($_POST[self::NONCE], self::NONCE)
            ) {
                update_post_meta($post, self::META_KEY, $_POST[self::META_KEY]);
            
        }
    }

}