<?php
/**
 * Template Name: page de mentions légales
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>


<div class="row">
    <div class="col-md-10 offset-md-1">

        <p class="paragraphe_description"><?php the_title(); ?></p>
        <div class="cadre" style="margin-top: 25px!important;">

            <div class="titre-conseil">
            <?php the_content(); ?>
            </div>
        </div>

    </div>
</div>
<?php get_footer(); ?>