<?php
/**
 * Template Name: page du questionnaire
 * Template post type: page, post
 */

$age_saisie = false;
$age_apres_3_ans = false;


if (isset($_POST) && !empty($_POST)) {

    if(
        (isset($_POST["age_enfant_ans"]) && !empty($_POST["age_enfant_ans"])) ||
        (isset($_POST["age_enfant_mois"]) && !empty($_POST["age_enfant_mois"]))
    ) {
        $age_saisie = true;

        $_SESSION['age_enfant'] = $_POST["age_enfant"];

        $_SESSION['tranche_age'] = trouver_tranche_age($_POST["age_enfant_ans"], $_POST["age_enfant_mois"]);
        
        if($_POST["age_enfant_ans"] > 3) {
            $age_apres_3_ans = true;
        }

        if($_POST["age_enfant_ans"] == 3 && $_POST["age_enfant_mois"] > 0) {
            $age_apres_3_ans = true;
        }
    }
}

function trouver_tranche_age($age_ans, $age_mois) {
    
    $age_tranches = [3, 4, 6, 12];
    $num_tranche = 0;
    foreach($age_tranches as $borne) {
        if($age_ans < $borne) {
            return $num_tranche;
        } else if ($age_ans == $borne) {
            // echo 'age '. $age_ans .' = borne' . $borne; 
            if($age_mois > 0) {
                return ++$num_tranche; 
            } else {
                return $num_tranche;
            }
        }

        $num_tranche ++;
    }
    return $num_tranche;
}

?>

<?php  get_header(); ?>


<?php if(!$age_saisie) : ?>
Veuilez saisir l'age de l'enfant
<?php endif; ?>




<form method="post" id="questionnaire_ecran_form" action="<?php bloginfo('url'); ?>/index.php/questionnaire-resultat">


<input type="hidden" value="<?= $_POST["age_enfant_ans"]; ?>" name="age_enfant_ans" />
<input type="hidden" value="<?= $_POST["age_enfant_mois"]; ?>" name="age_enfant_mois" />
<input type="hidden" value="<?= $_SESSION['tranche_age']; ?>" name="tranche_age" />
<input type="hidden" value="<?= $age_apres_3_ans ? 'true': 'false'; ?>" name="age_apres_3_ans" />


<div class="row">
    <div class="col-md-12 mt-5">
        <h2>Questionnaire sur les écrans</h2>
        <div id="stepper1" class="bs-stepper">
        <div class="bs-stepper-header row">
            <div class="step" data-target="#question-l-1">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">1</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-2">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">2</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-3">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">3</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-4">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">4</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-5">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">5</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-6">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">6</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-7">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">7</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-8">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">8</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-9">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">9</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-10">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">10</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-11">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">11</span>
            </button>
            </div>
        </div>
        <div class="bs-stepper-content">
        <div id="question-l-1" class="content">
            <p class="enonce_question">  1. Quels types d’écrans y a-t-il à la maison, et combien ?</p>
            <div class="row">
                <div id="propo1question1" class="amr-radiomenu col-md-3">
                    <label>Télévision: </label> <br>
                    <div class="amr-option-holder">
                        <input type="radio" id="oui_tele_radio" value="oui" name="tele_radio">
                        <label class="amr-label" for="oui_tele_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" id="non_tele_radio" value="non" name="tele_radio">
                        <label class="amr-label" for="non_tele_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>

                    <span>Combien :</span>
                    <span style="width: 15px"></span>
                    <input class="form-control" type="text" name="combiens_tele" />

                </div>

                <div id="propo2question1" class="amr-radiomenu col-md-3">  
                    
                    <label>Tablette: </label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_tablette_radio" name="tablette_radio">
                        <label class="amr-label" for="oui_tablette_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_tablette_radio" name="tablette_radio">
                        <label class="amr-label" for="non_tablette_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                    
                    <span>Combien : </span>
                    <span style="width: 15px"></span>
                    <input class="form-control" type="text" name="combiens_tablette" />
                    
                </div>

                <div id="propo3question1" class="amr-radiomenu col-md-3"> 
                    <label>Ordinateur: </label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_ordinateur_radio" name="ordinateur_radio">
                        <label class="amr-label" for="oui_ordinateur_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_ordinateur_radio" name="ordinateur_radio">
                        <label class="amr-label" for="non_ordinateur_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>

                    <span>Combien : </span>
                    <span style="width: 15px"></span>
                    <input class="form-control" type="text" name="combiens_ordinateurs" />

                </div>


                <div id="propo4question1" class="amr-radiomenu col-md-3"> 
                    <label>Téléphone: </label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_telephone_radio" name="telephone_radio">
                        <label class="amr-label" for="oui_telephone_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_telephone_radio" name="telephone_radio">
                        <label class="amr-label" for="non_telephone_radio" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>

                    <span>Combien : </span>
                    <span style="width: 15px"></span>
                    <input class="form-control" type="text" name="combiens_telephone" />

                </div>

            </div>
            <button type="button" class="btn btn-primary next">Suivant</button>
            </div>
        <div id="question-l-2" class="content">
            <p class="enonce_question" >2.	Confiez-vous votre téléphone à votre enfant </p>

            <div class="row">
                <div class="amr-radiomenu col-md-4"> 
                    <label>Pour le calmer quand il pleure ? </label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_telephone_pleur" name="telephone_pleur" />
                        <label class="amr-label" for="oui_telephone_pleur" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_telephone_pleur" name="telephone_pleur">
                        <label class="amr-label" for="non_telephone_pleur" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

                <div class="amr-radiomenu col-md-4"> 
                    <label>Pour l’occuper ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_telephone_occupe" name="telephone_occupe">
                        <label class="amr-label" for="oui_telephone_occupe" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_telephone_occupe" name="telephone_occupe">
                        <label class="amr-label" for="non_telephone_occupe" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>


                <div class="amr-radiomenu col-md-4"> 
                    <label>Pour l’aider à manger ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_telephone_manger" name="telephone_manger">
                        <label class="amr-label" for="oui_telephone_manger" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_telephone_manger" name="telephone_manger">
                        <label class="amr-label" for="non_telephone_manger" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

                <div class="amr-radiomenu col-md-4"> 
                    <label>Pour s’endormir ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_telephone_endormir" name="telephone_endormir">
                        <label class="amr-label" for="oui_telephone_endormir" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_telephone_endormir" name="telephone_endormir">
                        <label class="amr-label" for="non_telephone_endormir" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>


                <div class="amr-radiomenu col-md-4"> 
                    <label>Pour son apprentissage ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_telephone_apprentissage" name="telephone_apprentissage">
                        <label class="amr-label" for="oui_telephone_apprentissage" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_telephone_apprentissage" name="telephone_apprentissage">
                        <label class="amr-label" for="non_telephone_apprentissage" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

                <div class="amr-radiomenu col-md-4"> 
                    <label>Pour parler avec la famille ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_telephone_parler" name="telephone_parler">
                        <label class="amr-label" for="oui_telephone_parler" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_telephone_parler" name="telephone_parler">
                        <label class="amr-label" for="non_telephone_parler" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

            </div>
            

            <button type="button" class="btn btn-primary next">Suivant</button>
        </div>
        <div id="question-l-3" class="content">
            <p class="enonce_question">3.	Laissez-vous en général votre télé allumée en permanence (ou presque) ?</p>
            <div class="row">
                <div class="amr-radiomenu col-md-4"> 
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_television_allume" name="television_allume">
                        <label class="amr-label" for="oui_television_allume" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_television_allume" name="television_allume">
                        <label class="amr-label" for="non_television_allume" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>
            </div>

            <button type="button" class="btn btn-primary next" >Suivant</button>
        </div>
        <div id="question-l-4" class="content">
            <p class="enonce_question">4. Votre enfant passe-t-il du temps devant un écran ?</p>
            <div class="row">
                <div class="amr-radiomenu col-md-4"> 
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_temps_ecran" name="temps_ecran">
                        <label class="amr-label" for="oui_temps_ecran" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_temps_ecran" name="temps_ecran">
                        <label class="amr-label" for="non_temps_ecran" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

                <div class="amr-radiomenu col-md-4 temps_ecran_extra"> 
                    <div class="amr-option-holder">
                        <input type="radio" value="Moins de 30 mn" id="oui_temps_ecran_30_m" name="temps_enfant_ecran">
                        <label class="amr-label" for="oui_temps_ecran_30_m" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Moins de 30 mn
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="30 mn à 1 h" id="oui_temps_ecran_1_h" name="temps_enfant_ecran">
                        <label class="amr-label" for="oui_temps_ecran_1_h" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            30 mn à 1 h 
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="1 h à 1 h 30 mn" id="oui_temps_ecran_1_h_30" name="temps_enfant_ecran">
                        <label class="amr-label" for="oui_temps_ecran_1_h_30" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            1 h à 1 h 30 mn 
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="1 h 30 mn à 2 h" id="oui_temps_ecran_2_h" name="temps_enfant_ecran">
                        <label class="amr-label" for="oui_temps_ecran_2_h" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            1 h 30 mn à 2 h 
                        </label>
                    </div>


                    <div class="amr-option-holder">
                        <input type="radio" value="Plus de 2 h" id="oui_temps_ecran_plus_2_h" name="temps_enfant_ecran">
                        <label class="amr-label" for="oui_temps_ecran_plus_2_h" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Plus de 2 h
                        </label>
                    </div>
                </div>
            </div>

        
            <button type="button" class="btn btn-primary next" >Suivant</button>
        </div>
    <div id="question-l-5" class="content">
        <p class="enonce_question">5. Un ou des écrans sont-ils allumés pendant les repas ?</p>

        <div class="row">
            <div class="amr-radiomenu col-md-4"> 
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_allume_repas" name="allume_repas">
                    <label class="amr-label" for="oui_allume_repas" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_allume_repas" name="allume_repas">
                    <label class="amr-label" for="non_allume_repas" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-4 allume_repas_extra"> 
                <label>Refus de manger :</label>
                <div class="row">
                    <div class="col-md-6 amr-option-holder">
                        <input type="radio" value="oui" id="oui_allume_refus_manger" name="allume_ecran_cause_refus_manger">
                        <label class="amr-label" for="oui_allume_refus_manger" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="col-md-6 amr-option-holder">
                        <input type="radio" value="non" id="non_allume_refus_manger" name="allume_ecran_cause_refus_manger">
                        <label class="amr-label" for="non_allume_refus_manger" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

                <label>Utilisation de son smartphone alors qu’on donne à manger à son enfant: </label>
                <div class="row">
                    <div class="col-md-6 amr-option-holder">
                        <input type="radio" value="oui" id="oui_allume_donne_manger" name="allume_ecran_cause_donne_manger">
                        <label class="amr-label" for="oui_allume_donne_manger" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="col-md-6 amr-option-holder">
                        <input type="radio" value="non" id="non_allume_donne_manger" name="allume_ecran_cause_donne_manger">
                        <label class="amr-label" for="non_allume_donne_manger" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>
                
                <label>La télévision allumée en fond d’écran ou pour regarder les informations : </label>
                <div class="row">
                    <div class="col-md-6 amr-option-holder">
                        <input type="radio" value="oui" id="oui_allume_information" name="allume_ecran_cause_information">
                        <label class="amr-label" for="oui_allume_information" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>
                    <div class="col-md-6 amr-option-holder">
                        <input type="radio" value="non" id="non_allume_information" name="allume_ecran_cause_information">
                        <label class="amr-label" for="non_allume_information" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>
            </div>
        </div>

           
       
        <button type="button" class="btn btn-primary next" >Suivant</button>
    </div>
    <div id="question-l-6" class="content">
        <p class="enonce_question">6. Votre enfant passe-t-il du temps devant un écran le matin ? </p>
        <div class="row">
            <div class="amr-radiomenu col-md-4"> 
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_ecran_matin" name="ecran_matin">
                    <label class="amr-label" for="oui_ecran_matin" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_ecran_matin" name="ecran_matin">
                    <label class="amr-label" for="non_ecran_matin" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>
        </div>

        <button type="button" class="btn btn-primary next" >Suivant</button>
    </div>
    <div id="question-l-7" class="content">
        <p class="enonce_question"> 7.	Votre enfant passe-t-il du temps devant un écran avant de se coucher ? </p>

        <div class="row">
            <div class="amr-radiomenu col-md-4"> 
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_ecran_avant_coucher" name="ecran_avant_coucher">
                    <label class="amr-label" for="oui_ecran_avant_coucher" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_ecran_avant_coucher" name="ecran_avant_coucher">
                    <label class="amr-label" for="non_ecran_avant_coucher" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>
        </div>

        <button type="button" class="btn btn-primary next" >Suivant</button>
    </div>

    <div id="question-l-8" class="content">
        <p class="enonce_question">8. Votre enfant a-il  dans sa chambre ?</p>

        <div class="row">
            <div class="amr-radiomenu col-md-3"> 
                <div>Un ordinateur :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_chambre_ordinateur" name="chambre_ordinateur">
                    <label class="amr-label" for="oui_chambre_ordinateur" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_chambre_ordinateur" name="chambre_ordinateur">
                    <label class="amr-label" for="non_chambre_ordinateur" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-3"> 
                <div>Une TV :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_chambre_television" name="chambre_television">
                    <label class="amr-label" for="oui_chambre_television" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_chambre_television" name="chambre_television">
                    <label class="amr-label" for="non_chambre_television" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-5"> 
                <div>Y apporte-t-il un ou des écrans mobiles ?</div>
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_chambre_ecran_mobile" name="chambre_ecran_mobile">
                    <label class="amr-label" for="oui_chambre_ecran_mobile" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_chambre_ecran_mobile" name="chambre_ecran_mobile">
                    <label class="amr-label" for="non_chambre_ecran_mobile" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-3"> 
                <div>S’endort-il devant ?</div>
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_chambre_ecran_endormir" name="chambre_ecran_endormir">
                    <label class="amr-label" for="oui_chambre_ecran_endormir" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_chambre_ecran_endormir" name="chambre_ecran_endormir">
                    <label class="amr-label" for="non_chambre_ecran_endormir" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>


        </div>

        <button type="button" class="btn btn-primary next" >Suivant</button>
    </div>

    <div id="question-l-9" class="content">
        <p class="enonce_question">9. Est-ce que votre enfant regarde et écoute habituellement :</p>

        <div class="row">
            <div class="amr-radiomenu col-md-3"> 
                <div>Les dessins animés :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="seul" id="seul_dessin_anime" name="dessin_anime">
                    <label class="amr-label" for="seul_dessin_anime" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Seul
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="avec_vous_dessin_anime" name="dessin_anime">
                    <label class="amr-label" for="avec_vous_dessin_anime" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Avec vous
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="aucun" id="aucun_dessin_anime" name="dessin_anime">
                    <label class="amr-label" for="aucun_dessin_anime" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Aucun
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-3"> 
                <div>Les programmes éducatifs :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="seul" id="seul_programme_educatif" name="programme_educatif">
                    <label class="amr-label" for="seul_programme_educatif" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Seul
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="avec_vous_programme_educatif" name="programme_educatif">
                    <label class="amr-label" for="avec_vous_programme_educatif" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Avec vous
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="aucun" id="aucun_programme_educatif" name="programme_educatif">
                    <label class="amr-label" for="aucun_programme_educatif" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Aucun
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-3"> 
                <div>Les vidéos :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="seul" id="seul_video" name="video">
                    <label class="amr-label" for="seul_video" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Seul
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="avec_vous_video" name="video">
                    <label class="amr-label" for="avec_vous_video" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Avec vous
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="aucun_video" name="video">
                    <label class="amr-label" for="aucun_video" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Aucun
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-3"> 
                <div>Des programmes non destinés aux enfants :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="seul" id="seul_programme_non_enfant" name="programme_non_enfant">
                    <label class="amr-label" for="seul_programme_non_enfant" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Seul
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="avec_vous_programme_non_enfant" name="programme_non_enfant">
                    <label class="amr-label" for="avec_vous_programme_non_enfant" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Avec vous
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="aucun" id="aucun_programme_non_enfant" name="programme_non_enfant">
                    <label class="amr-label" for="aucun_programme_non_enfant" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Aucun
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-3"> 
                <div>Jeux video :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="seul" id="seul_jeux_video" name="jeux_video">
                    <label class="amr-label" for="seul_jeux_video" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Seul
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="avec_vous_jeux_video" name="jeux_video">
                    <label class="amr-label" for="avec_vous_jeux_video" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Avec vous
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="aucun" id="aucun_jeux_video" name="jeux_video">
                    <label class="amr-label" for="aucun_jeux_video" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Aucun
                    </label>
                </div>
            </div>

            
            <div class="amr-radiomenu col-md-3"> 
                <div>Internet :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="seul" id="seul_internet" name="internet">
                    <label class="amr-label" for="seul_internet" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Seul
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="avec_vous_internet" name="internet">
                    <label class="amr-label" for="avec_vous_internet" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Avec vous
                    </label>
                </div>
                
                <div class="amr-option-holder">
                    <input type="radio" value="aucun" id="aucun_internet" name="internet">
                    <label class="amr-label" for="aucun_internet" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Aucun
                    </label>
                </div>
            </div>

            <!--div class="amr-radiomenu col-md-3"> 
                <div>Autre :</div>
                <div class="amr-option-holder">
                    <input type="radio" value="seul" id="seul_autre" name="autre">
                    <label class="amr-label" for="seul_autre" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Seul
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="avec vous" id="avec_vous_autre" name="autre">
                    <label class="amr-label" for="avec_vous_autre" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Avec vous
                    </label>
                </div>
            </div-->

        </div>

        
        <button type="button" class="btn btn-primary next">Suivant</button>
    </div>

    <div id="question-l-10" class="content">
        <p class="enonce_question">10. Est-ce que vous regardez régulièrement en famille la télévision ?</p>
        
        <div class="row">
            <div class="amr-radiomenu col-md-3"> 
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_television_famille" name="television_famille">
                    <label class="amr-label" for="oui_television_famille" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_television_famille" name="television_famille">
                    <label class="amr-label" for="non_television_famille" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>

            <div class="amr-radiomenu col-md-5 television_famille_extra" > 
                <div>Ces moments sont-ils l’occasion d’échanges avec votre enfant ? </div>
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_echange_television_famille" name="echange_television_famille">
                    <label class="amr-label" for="oui_echange_television_famille" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_echange_television_famille" name="echange_television_famille">
                    <label class="amr-label" for="non_echange_television_famille" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>

        </div>

        <button type="button" class="btn btn-primary next">Suivant</button>
    </div>
    <div id="question-l-11" class="content">
        <p class="enonce_question">11. Votre famille s’est-elle dotée de règles ou de directives relatives à l’utilisation des écrans ?</p>

        <div class="row">
            <div class="amr-radiomenu col-md-3"> 
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_regle_ecran" name="regle_ecran">
                    <label class="amr-label" for="oui_regle_ecran" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_regle_ecran" name="regle_ecran">
                    <label class="amr-label" for="non_regle_ecran" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div>


            <!--div class="amr-radiomenu col-md-4 regle_ecran_extra"> 
                <div>Sont-elles faciles à faire respecter ?</div>
                <div class="amr-option-holder">
                    <input type="radio" value="oui" id="oui_regle_facile" name="regle_facile">
                    <label class="amr-label" for="oui_regle_facile" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Oui
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="non" id="non_regle_facile" name="regle_facile">
                    <label class="amr-label" for="non_regle_facile" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Non
                    </label>
                </div>
            </div-->
        </div>


        <button type="submit" class="btn btn-mdb btn-primary waves-effect waves-light">Valider</button>
    </div>
        </div>
        </div>
    </div>
</div>



</form>




<?php get_footer(); ?>