<?php
/**
 * Template Name: resultat questionnaire plus
 * Template post type: page, post
 */


$age_apres_3_ans = false;
$age_enfant_ans = $_POST['age_enfant_ans'];
$age_enfant_mois = $_POST['age_enfant_mois'];

if($_POST["age_enfant_ans"] > 3) {
    $age_apres_3_ans = true;
}

if($_POST["age_enfant_ans"] == 3 && $_POST["age_enfant_mois"] > 0) {
    $age_apres_3_ans = true;
}



if (isset($_POST) && !empty($_POST)) {

    if(
        (isset($_POST["age_enfant_ans"]) && !empty($_POST["age_enfant_ans"])) ||
        (isset($_POST["age_enfant_mois"]) && !empty($_POST["age_enfant_mois"]))
    ) {
    
        $tranche_age = trouver_tranche_age($_POST["age_enfant_ans"], $_POST["age_enfant_mois"]);
        
    }
}

function trouver_tranche_age($age_ans, $age_mois) {
    
    $age_tranches = [3, 4, 6, 12];
    $num_tranche = 0;
    foreach($age_tranches as $borne) {
        if($age_ans < $borne) {
            return $num_tranche;
        } else if ($age_ans == $borne) {
            // echo 'age '. $age_ans .' = borne' . $borne; 
            if($age_mois > 0) {
                return ++$num_tranche; 
            } else {
                return $num_tranche;
            }
        }

        $num_tranche ++;
    }
    return $num_tranche;
}


?>

<?php  get_header(); ?>
		
<div class="mt-5 mb-5" >
    <div class="row">
        <div class="offset-md-8">
            <button type="button" class="btn btn-mdb btn-primary waves-effect waves-light" id="telecharger_pdf_plus">Télécharger en PDF</button>
        </div>
    </div>
	
	<div class="row" id="resultat_du_questionnaire_plus">
        
		<div class="col-md-10 offset-md-1" >
            <h2>Résultat du questionnaire </h2>
            <ul class="timeline">
                <li>
                    <h3 class="question_label">1. Age de l'enfant : 
                        <?php echo $age_enfant_ans; echo  $age_enfant_ans == "1" ? " an " : " ans ";  ?>
                        et
                        <?php echo ' '.$age_enfant_mois . " mois";  ?>
                    </h3>
                </li>
				<li>
					<h3 class="question_label">2. Pensez-vous que l’utilisation des écrans : </h3>
                    <h4 class="sous_question_label">2.1. Développe l’éveil et les apprentissages ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["eveil_apprentissage"]; ?></span></p>
                    <?php if($_POST["eveil_apprentissage"] === "oui") : ?>
                        <?php if(!$age_apres_3_ans) : ?>
                            <div class="alert alert-danger">
                            <!-- wp:paragraph -->
                                <p>Développe l’éveil et les apprentissages</p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>l’écran n’apporte rien</li><li>Mettre votre enfant devant un écran à cet âge ce lui voler du temps pour son développement psychomoteur </li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php else: ?>
                            <div class="alert alert-danger">
                            <!-- wp:paragraph -->
                                <p>Développe l’éveil et les apprentissages : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Attention il faudra respecter la durée conseiller pour la quel votre enfant peut utiliser les écrans car une surexposition aux écrans engendre des troubles importants ( cf fiche effet 2md) </li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif ?>

                    <?php if($_POST["eveil_apprentissage"] === "non") : ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif ?>


                    <h4 class="sous_question_label">2.2. Aide votre enfant à se calmer ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["calme_enfant"]; ?></span></p>
                    <?php if($_POST["calme_enfant"] === "oui") : ?>
                        <div class="alert alert-danger" >
                            <!-- wp:paragraph -->
                            <p>Aide votre enfant à se calmer : </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>Attention calmer votre enfants par un écran ceci risque de le rendre addict  aux écrans</li><li>Conduite à tenir pour calmer les pleurs<ul><li>Parlez-lui pour le calmer<ul><li>en gardant votre calme</li><li>se mettre à sa hauteur</li><li>parlez-lui normalement</li><li>rassurez-le en le caressant</li><li>L’enfant réagit par mimétisme</li></ul></li><li>Détournez son attention<ul><li>Détournez son attention avec humour ou en parlant d’un autre sujet</li><li>Prenez un petit sac avec des jouer ou des livres de quoi l’occuper</li></ul></li><li>Si la situation est tendue sortez à l’extérieur et faire plus au moins une promenade pour faire oublier les écrans</li></ul></li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif ?>
                    <?php if($_POST["calme_enfant"] === "non") : ?>
                          <!-- réponse non -->
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif ?>  


                    <h4 class="sous_question_label">2.3. Améliore son attention et sa concentration ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["ameliore_attention"]; ?></span></p>
                    <?php if($_POST["ameliore_attention"] === "oui") : ?>
                        <?php if(!$age_apres_3_ans) : ?>
                            <div class="alert alert-danger">
                                <!-- wp:list -->
                                <ul><li>Pour les enfants  &lt; 3 ans  pour lesquels il y a le phénomène du déficit du transfert<ul><li>Des études ont prouvé que exposer son enfant aux écrans a cet âge ce induire<ul><li>Un trouble du langage, de communication</li><li>Un trouble de concentration et d’attention</li></ul></li></ul></li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php else : ?>
                            <div class="alert alert-danger">
                                <!-- wp:list -->
                                <ul><li>pour les enfants > 3 ans pour lesquels le déficit du transfert diminue avec l’âge<ul><li>Si on ne respecte pas la durée limite à l’exposition des écrans et contrôler  le contenu  ceci induire<ul><li>des trouble de l’attention et de concentration</li><li>trouble visuel</li><li>violence visuelle</li></ul></li></ul></li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif ?>

                    <?php if($_POST["ameliore_attention"] === "non") : ?>
                          <!-- réponse non -->
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif ?> 

                    <h4 class="sous_question_label">2.4. Le prépare au monde de demain ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["monde_demain"]; ?></span></p>
                    <?php if($_POST["monde_demain"] === "oui") : ?>
                        <div class="alert alert-danger">
                            <!-- wp:list -->
                            <ul><li>Aucune étude n’a prouvé que les enfants moins exposer aux écrans avaient des problèmes dans la vie adulte mais tous le contraire un enfant sur exposée aux écrans développera plusieurs trouble</li><li>Impact sur la santé<ul><li>Myopie</li><li>Surpoids –obésité</li><li>Risque d’addiction</li></ul></li><li>Effet psycho-sociale<ul><li>Passivité</li><li>Repli sur soi, isolement</li><li>Violence</li><li>L’imitation de l’imaginaire</li></ul></li><li>Influence sur notre mode de vie et nos consommations</li><li>Retard des acquisitions<ul><li>Perd de la motricité fine</li><li>Trouble de l’attention et de la concentration</li><li>Trouble du langage et de la communication</li><li>Trouble dans l’apprentissage de l’écriture et les dessins</li></ul></li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif ?>
                    <?php if($_POST["monde_demain"] === "non") : ?>
                          <!-- réponse non -->
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Bravo c'est très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif ?> 

                </li>
				<li>
                    <h3 class="question_label">3. D’après vous, le temps passé par votre enfant devant les écrans :</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["temps_devant_ecran"]; ?></span></p>
                    <?php if($_POST["temps_devant_ecran"]==="Est plutôt adapté"): ?>
                
                        <div class="alert alert-success" >
                            <!-- wp:list -->
                            <ul><li>Est plutôt adapté  si<ul><li>Enfant &lt; 3 ans = zéro écran</li><li>Enfant de 2-4 ans = max 30 min par jour</li><li>Enfant 4-6 ans  = max 1h  par jour</li><li>Enfant 6-12ans<ul><li>les 1h par jour pour les enfants   entre 6-8 ans</li><li>entre 1h-1h30 pour les enfants entre 8-12 ans</li></ul></li><li>Enfant > 12ans =  La durée d’exposition  doit être contrôlée</li></ul></li></ul>
                            <!-- /wp:list -->
                        </div>
        
                    <?php endif; ?>
               

                    <?php if($_POST["television_allume"]==="Est plutôt trop important"): ?>
                        <div class="alert alert-warning">
                            <!-- wp:list -->
                            <ul><li>Est plutôt trop important :<ul><li>Enfant &lt; 3ans = si exposition écran</li><li>Enfant de 2-4 ans = si > 30min par jour</li><li>Enfant 4-6 ans  = si > max 1h par jour</li><li>Enfant 6-12ans<ul><li>les 1h par jour pour les enfants   entre 6-8 ans</li><li>entre 1h-1h30 pour les enfants entre 8-12 ans</li></ul></li><li>Enfant > 12ans =  La durée d’exposition non contrôlée</li></ul></li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif; ?>
                </li>
				<li>
                    <h3 class="question_label">4. Age de début d’exposition aux écrans ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong>
                        <span class="valeur_reponse"><?= $_POST["debut_exposition_ecran"]; ?></span></p>
                    
          
                    <div class="alert alert-warning">
                        <!-- wp:list -->
                        <ul><li>Attention l’exposition des enfants très précoces aux écrans => induira des troubles plus au moins important sur la santé et le développent psychomoteur de votre enfants</li><li>Impact sur la santé<ul><li>Myopie</li><li>Surpoids –obésité</li><li>Risque d’addiction</li></ul></li><li>Effet psycho-sociale<ul><li>Passivité</li><li>Repli sur soi, isolement</li><li>Violence</li><li>L’imitation de l’imaginaire</li></ul></li><li>Influence sur notre mode de vie et nos consommations</li><li>Retard des acquisitions<ul><li>Perd de la motricité fine</li><li>Trouble de l’attention et de la concentration</li><li>Trouble du langage et de la communication</li><li>Trouble dans l’apprentissage de l’écriture et les dessins</li></ul></li></ul>
                        <!-- /wp:list -->
                    </div>

                </li>
                
                <li>
                    <h3 class="question_label">5. Estimez, vous parent, votre temps moyen passé chaque jour devant les écrans :</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong>
                    <span class="valeur_reponse">
                        <?= $_POST["parent_devant_ecran"]; ?></span></p>
                    <div class="alert alert-warning">
                        <!-- wp:list -->
                        <ul><li>Attention à votre propre consommation des écrans<ul><li>Des études ont prouvé que la consommation des enfants aux écrans est corréler à la consommation de leurs parents</li></ul><ul><li>Si les parents utilisent régulièrement les écrans l’enfant sera forcément exposer aux écrans</li></ul></li><li>Des études ont prouvé qu’un enfant moins exposer dans son enfance aux écrans => c’est un adulte moins dépendant des écrans</li></ul>
                        <!-- /wp:list -->
                    </div> 
                </li>
			</ul>
		</div>
    </div>
    



     
    <div class="row">
        <div class="offset-md-8">
            <button type="button" class="btn btn-mdb btn-primary waves-effect waves-light telecharger_pdf_questionnaire">Télécharger en PDF</button>
        </div>
    </div>
    
<?php

// Trouver toutes les catégories de fiche conseils
$args = array (
    'taxonomy' => 'age_categorie', //your custom post type
    //'orderby' => 'name',
    //'order' => 'ASC',
    'hide_empty' => 0 //shows empty categories
);
$ages = get_categories( $args );

?>
    <h3 class="text-center"><?=  'Fiches d\'information ' . $ages[$tranche_age]->name; ?></h3>
    <div class="row mt-30">


    

     <!-- Récupérer toutes les charte de famille -->
 <?php
        $fiches = new WP_Query([
            'post_type' => 'affiche',
            'tax_query' => array(
              array(
              'taxonomy' => 'age_categorie',
              'field' => 'name',
              'terms' => $ages[$tranche_age]->name
               )
            )
        ]);

        $modal_number = 0;
        while($fiches->have_posts()) : $fiches->the_post();
        $modal_number++;
    ?>

    
<div class="col-md-4 col-sm-6 pb-2">
            <div class="box17">
                <img height="50%" src="<?= get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?= the_title(); ?>">
                <ul class="icon">
                    <li data-toggle="modal" data-target="#modal<?= $modal_number; ?>"><a href="#"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a></li>
                    <li onclick="PrintImage('<?= get_the_post_thumbnail_url(get_the_ID()); ?>')"><a><i class="fa fa-print" aria-hidden="true"></i></a></li>
                </ul>
                <div class="box-content">
                    <h3 class="title"><?= the_title(); ?></h3>
                </div>
            </div>
        </div>

    <div class="row">
        <div class="col-lg-4 col-md-12 mb-4">
            <div class="modal fade" id="modal<?= $modal_number; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-footer center-content">
                    <h2 style="margin: auto;"> <?= the_title(); ?> </h2>
                </div>

                <div class="modal-body ">
                <img style="margin: auto;" src="<?= get_the_post_thumbnail_url(get_the_ID());  ?>" class="teamy__avatar" alt="<?= the_title(); ?>">
                </div>
                <div class="modal-footer justify-content-center">
                    <button  onclick="PrintImage('<?= get_the_post_thumbnail_url(get_the_ID()); ?>')" type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4">Imprimer</button>
                    <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Fermer</button>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>

<?php endwhile; wp_reset_postdata(); ?>
    </div>






</div>


<?php get_footer(); ?>