<?php
/**
 * Template Name: page des fiches
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>



<?php

/* $args = array (
  'taxonomy' => 'affiche_langues', //your custom post type
  'orderby' => 'name',
  'order' => 'ASC',
  'hide_empty' => 0 //shows empty categories
);
$categories = get_categories( $args );
foreach ($categories as $category) {    
  echo $category->name;

  $post_by_cat = get_posts(array('cat' => $category->term_id));

  echo '<ul>';
  foreach( $post_by_cat as $post ) {
      setup_postdata($post);
      echo '<li><a href="'.the_permalink().'">'.the_title().'</a></li>';
  }
  echo '</ul>';
}*/ 

$args = array (
  'taxonomy' => 'affiche_langues', //your custom post type
  //'orderby' => 'name',
  //'order' => 'ASC',
  'hide_empty' => 0 //shows empty categories
);
$langues = get_categories( $args );

?>


<div class="row">
    <div class="col-md-10 offset-md-1">
    <p class="paragraphe_description"><?php the_title(); ?></p>
<div class="cadre mt-25">

  <ul>

  <?php $blocks = parse_blocks($post->post_content); $iteration = 0; ?>
  <?php foreach ( $blocks as $bloque ) {
        $liens_contenue =apply_filters('the_content', render_block( $bloque ));
    
        if(str_replace(" ", "", $liens_contenue) !== "") {
            if ( $iteration === 3) { ?>
          <form id="form_langue" method="post" action="<?= bloginfo('url'); ?>/charte-de-famille">
            <div class="row">
              <div class="col-md-9 mb-3">
                <select class="form-control form-control-lg" name="langue" id="langues_support">
                  <?php foreach( $langues as $langue ) : ?>
                  <option value="<?= $langue->name; ?>" <?= $langue->name == 'Français' ? 'selected' : ""; ?> ><?= $langue->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-3 mb-3">
                
              </div>
            </div>
          </form>
          <?php  }
            // Cas le bloque a affiché est déjà une liste (ne pas ajouter le tiret)
            if (strpos($liens_contenue, '<ul>') === 0 ||
                strpos($liens_contenue, '<ol>') === 0 ) {

                echo  '<li class="titre-conseil no-list-style">' . $liens_contenue . '</li>';
            } else {
                echo '<li class="fiche-conseil  lien-charte">'. $liens_contenue . '</li>';
            }
            $iteration ++;
        }
       
    }
?>
    
</ul>

  
</div>

</div>
</div>



<?php get_footer(); ?>