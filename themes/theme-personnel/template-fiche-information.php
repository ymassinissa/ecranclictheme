<?php
/**
 * Template Name: page fiche d'information
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>

<div class="mt-40">
    <p class="paragraphe_description"><?php the_title(); ?></p>
    <div class="row mt-30">
 <!-- Récupérer toutes les charte de famille -->
 <?php
        $ecran_nefastes = new WP_Query([
            'post_type' => 'affiche',
            'tax_query' => array(
              array(
              'taxonomy' => 'affiche_details',
              'field' => 'name',
              'terms' => 'écran néfaste'
               )
            )
        ]);

        $modal_number = 0;
        while($ecran_nefastes->have_posts()) : $ecran_nefastes->the_post();
        $modal_number++;
    ?>




        <div class="col-md-4 col-sm-6 pb-2">
            <div class="box17">
                <img height="50%" src="<?= get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?= the_title(); ?>">
                <ul class="icon">
                    <li data-toggle="modal" data-target="#modal<?= $modal_number; ?>"><a href="#"><i class="fa fa-search"></i></a></li>
                    <li><a href="#"><i class="fa fa-link"></i></a></li>
                </ul>
                <div class="box-content">
                    <h3 class="title"><?= the_title(); ?></h3>
                </div>
            </div>
        </div>

    <div class="row">
        <div class="col-lg-4 col-md-12 mb-4">
            <div class="modal fade" id="modal<?= $modal_number; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-footer center-content">
                    <h2 style="margin: auto;"> <?= the_title(); ?> </h2>
                </div>

                <div class="modal-body ">
                <img style="margin: auto;" src="<?= get_the_post_thumbnail_url(get_the_ID());  ?>" class="teamy__avatar" alt="<?= the_title(); ?>">
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4">Télécharger</button>
                    <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Fermer</button>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>

<?php endwhile; wp_reset_postdata(); ?>
</div>
</div>



<?php get_footer(); ?>