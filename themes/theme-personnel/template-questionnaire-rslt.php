<?php
/**
 * Template Name: resultat questionnaire 1
 * Template post type: page, post
 */


$age_apres_3_ans = false;

if($_POST['age_apres_3_ans'] == "true") {
    $age_apres_3_ans = true;
}
$tranche_age = $_POST['tranche_age'];
$age_enfant_ans = $_POST['age_enfant_ans'];
$age_enfant_mois = $_POST['age_enfant_mois'];

?>

<?php  get_header(); ?>
		
<div class="mt-5 mb-5" >
    <div class="row">
        <div class="offset-md-8">
   
        <!--button type="button" class="btn btn-mdb btn-primary waves-effect waves-light " onclick="Printtest()">Télécharger en PDF</button-->
        
            <button type="button" class="btn btn-mdb btn-primary waves-effect waves-light telecharger_pdf_questionnaire" id="telecharger_pdf">Télécharger en PDF</button>
        </div>
    </div>
	
	<div class="row" id="resultat_du_questionnaire">
        
		<div class="col-md-10 offset-md-1" >
            <h2>Résultat du questionnaire </h2>
            <ul class="timeline">
                <li>
                <h3 class="question_label">1. Age de l'enfant : 
                    <?php echo $age_enfant_ans; echo  $age_enfant_ans == "1" ? " an " : " ans ";  ?>
                    et
                    <?php echo ' '.$age_enfant_mois . " mois";  ?>
                </h3>
                </li>
				<li>
					<h3 class="question_label">2. Confiez-vous votre téléphone à votre enfant ? </h3>
                    <h4 class="sous_question_label">2.1. Pour le calmer quand il pleure ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["telephone_pleur"]; ?></span></p>
                    <?php if($_POST["telephone_pleur"] === "oui") : ?>

                        
                        
                        <?php if($tranche_age == "0" || $tranche_age == "1"
                                || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger ">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran pour le calmer car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:group -->
                                <div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:paragraph -->
                                <p>Des astuces pour calmer les pleurs de son enfant sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:list -->
                                <ul><li>Parlez-lui pour le calmer <ul><li>En gardant votre calme. </li><li>Mettez-vous à sa hauteur. </li><li>Parlez-lui normalement. </li><li>Rassurez-le en le caressant. </li><li>L’enfant réagit par mimétisme.</li></ul></li><li>Détournez son attention <ul><li>Utiliser l’humour ou en parlant d’un autre sujet.</li><li>Prenez un petit sac avec des jouets ou des livres pour l’occuper. </li></ul></li><li>Si la situation est tendue sortez à l’extérieur et faite plus au moins une promenade pour faire oublier les écrans.</li></ul>
                                <!-- /wp:list --></div></div>
                                <!-- /wp:group -->
                            </div>
                        <?php endif ?>
                       
                        <?php if ($tranche_age == "4") : ?>
                            <div class="alert alert-danger ">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran pour le calmer car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>

                            <div class="alert alert-info">
                               <!-- wp:paragraph -->
                                <p>Des astuces pour calmer les pleurs de son adolescent sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Parlez-lui pour le calmer <ul><li>En gardant votre calme </li><li>En se mettant à sa hauteur</li><li>Parlez-lui normalement </li><li>Rassurez-le en le caressant </li></ul></li><li>Détournez son attention avec humour ou en parlant d’un autre sujet </li><li>Si la situation est tendue sortez à l’extérieur et faites plus au moins une promenade pour faire oublier les écrans.</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif ?>
                    <?php endif ?>

                    <?php if($_POST["telephone_pleur"]==="non"): ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                    
                    <h4 class="sous_question_label">2.2. Pour l’occuper ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["telephone_occupe"]; ?></span></p>
                    <?php if($_POST["telephone_occupe"] === "oui") : ?>
                        
                        <?php if($tranche_age == "0" || $tranche_age == "1"
                                || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran pour l’occuper car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>À cet âge, votre petit a les sens en éveil. Il ne demande qu’à voir, toucher, sentir et, déjà, il adore votre compagnie ! Profitez-en pour lui proposer des activités qui vont l’aider à découvrir le monde et à progresser dans ses différents apprentissages avec des petits jeux d'éveil pour bébé. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p><strong><span class="has-inline-color has-vivid-red-color">0-6 mois :</span></strong></p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Les hochets, petits objets de couleurs, textures, matières, sonorités différentes </li><li>Les portiques </li><li>Les mobiles </li><li>Les bouliers </li><li>Les tapis sensoriels, sonores</li><li>La musique, les chants, les comptines </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p><strong><span class="has-inline-color has-vivid-red-color">6-12 mois</span></strong></p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Le tableau des découvertes </li><li>Des cubes </li><li>Des balles, ballons </li><li>Des livres en tissu, en carton, en plastique </li><li>Des anneaux </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p><strong><span class="has-inline-color has-vivid-red-color">12-15 mois </span></strong></p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Des porteurs </li><li>Des jouets à tirer </li><li>Des jeux d’empilage, d’emboîtage</li><li>Des voitures, camions en plastique </li><li>Des catalogues </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p><strong><span class="has-inline-color has-vivid-red-color">15 mois – 2 ans </span></strong></p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Poupées, dînettes </li><li>Gros lego </li><li>Livres </li><li>Transvasements: jeu d’eau, de sable, de grosses pâtes </li><li>Pâte à sel </li><li>Peinture </li><li>Dessin </li><li>Pâtisserie </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p><strong><span class="has-inline-color has-vivid-red-color">2 ans - 3 ans</span></strong></p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Tricycle</li><li>Puzzle, loto</li><li>Enfilage, laçage</li><li>Lego</li><li>Histoires, contes</li><li>Jeu d’imitation : établis, garages, parcours, landaus, marchande…</li><li>Collage, gommettes, peinture, dessin…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "1" ): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Des astuces pour occuper votre enfant sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:list -->
                                <ul><li>L’impliquer dans toutes les tâches : les enfants de 3-4 ans adorent effectuer des tâches ménagères : <ul><li>Passer un coup de balayette, faire les poussières, nettoyer les vitres… </li><li>Vous pouvez également lui proposer de mettre la table.</li><li>Vous pouvez également lui proposer de vous aider quand vous faites la cuisine, en mélangeant l’appareil à quiche par exemple, ou en nettoyant les légumes d'une salade… </li><li>Si vous avez une terrasse, un balcon, ou simplement une fenêtre, initiez votre jeune enfant aux joies du jardinage, même s’il ne s’agit que de planter quelques plantes dans une jardinière et de les arroser. Il adorera certainement mettre ses doigts dans la terre et prendre soin de “ses” fleurs. </li><li>… </li></ul></li><li>Les activités physiques : Il aura envie de se défouler au moins une fois par jour, et quand on ne peut pas sortir, cela peut rapidement se révéler une galère. Cela pourra prendre la forme : <ul><li>D’une boum improvisée en famille dans le salon.</li><li>D’une mini séance de yoga adaptée pour les enfants.</li><li>De comptines à mimer.</li><li>D’une construction de forteresse ou de cabane géante.</li><li>Ou encore de gym avec roulades </li><li>… </li></ul></li><li>Faire le plein d’activités artistiques : <ul><li>La pâte à modeler.</li><li>La pâte à sel.</li><li>La peinture.</li><li>Le dessin.</li><li>Les tampons.</li><li>Les gommettes.</li><li>Les pompons et autres fils chenille.</li><li>Le sable magique.</li><li>Des pauses musicales : en chantant, en jouant de l’harmonica, des maracas, du tambourin, ou encore en écoutant des styles de musique différents.</li><li>… </li></ul></li><li>Proposez-lui des jeux :<ul><li>Un jeu de société adapté à son jeune âge.</li><li>Des puzzles.</li><li>Des Lego.</li><li>Une pêche à la ligne.</li><li>Mais aussi des jeux d’imitation comme le docteur ou le vétérinaire par exemple.</li><li>Le cache-cache. </li><li>Vous pouvez aussi vous déguiser ensemble, avec un costume spécifique ou en utilisant de vieux habits et accessoires, ce qui amusera certainement votre enfant.</li><li>Ou encore lui proposer un petit spectacle de marionnettes, fabriquées par vos soins.</li><li>…</li></ul></li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                        
                        <?php if($tranche_age == "2"): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Des astuces pour occuper votre enfant sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>L’impliquer dans toutes les tâches : les enfants de 4-6 ans adorent effectuer des tâches ménagères : <ul><li>Passer un coup de balayette, faire les poussières, nettoyer les vitres… </li><li>Vous pouvez également lui proposer de mettre la table.</li><li>Vous pouvez également lui proposer de vous aider quand vous faites la cuisine, en mélangeant l’appareil à quiche par exemple, ou en nettoyant les légumes d'une salade… </li><li>Si vous avez une terrasse, un balcon, ou simplement une fenêtre, initiez votre jeune enfant aux joies du jardinage, même s’il ne s’agit que de planter quelques plantes dans une jardinière et de les arroser. Il adorera certainement mettre ses doigts dans la terre et prendre soin de “ses” fleurs. </li><li>… </li></ul></li><li>Les activités physiques : Il aura envie de se défouler au moins une fois par jour, et quand on ne peut pas sortir, cela peut rapidement se révéler une galère. Cela pourra prendre la forme : <ul><li>D’une boum improvisée en famille dans le salon.</li><li>D’une mini séance de yoga adaptée pour les enfants.</li><li>De comptines à mimer.</li><li>D’une construction de forteresse ou de cabane géante.</li><li>Ou encore de gym avec roulades </li><li>… </li></ul></li><li>Faire le plein d’activités artistiques : <ul><li>La pâte à modeler.</li><li>La pâte à sel.</li><li>La peinture.</li><li>Le dessin.</li><li>Les tampons.</li><li>Les gommettes.</li><li>Les pompons et autres fils chenille.</li><li>Le sable magique.</li><li>Des pauses musicales : en chantant, en jouant de l’harmonica, des maracas, du tambourin, ou encore en écoutant des styles de musique différents.</li><li>… </li></ul></li><li>Proposez-lui des jeux :<ul><li>Un jeu de société adapté à son jeune âge.</li><li>Des puzzles.</li><li>Des Lego.</li><li>Une pêche à la ligne.</li><li>Mais aussi des jeux d’imitation comme le docteur ou le vétérinaire par exemple.</li><li>Le cache-cache. </li><li>Vous pouvez aussi vous déguiser ensemble, avec un costume spécifique ou en utilisant de vieux habits et accessoires, ce qui amusera certainement votre enfant.</li><li>Ou encore lui proposer un petit spectacle de marionnettes, fabriquées par vos soins.</li><li>…</li></ul></li><li>La lecture est pour l'enfant le moyen de bâtir : <ul><li>Sa personnalité.</li><li>Sa confiance en soi.</li><li>la rapidité de compréhension.</li><li>Le discernement</li><li>La justesse de jugement et donc.</li><li>Le sens de la justice.</li><li>L'écoute.</li><li>Le sens de l'évasion.</li><li>La liberté.</li><li>La créativité.</li><li>L'indépendance.</li><li>La maturité.</li></ul></li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "3"): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Des astuces pour occuper votre enfant sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>L’impliquer dans toutes les tâches ménagères : <ul><li>Passer un coup de balayette, faire les poussières, nettoyer les vitres… </li><li>Vous pouvez également lui proposer de mettre la table </li><li>Vous pouvez également lui proposer de vous aider quand vous faites la cuisine, en mélangeant l’appareil à quiche par exemple, ou en nettoyant les légumes d'une salade… </li><li>… </li></ul></li><li>Les activités physiques : Il aura envie de se défouler au moins une fois par jour. Vous pourriez : <ul><li>Jouer aux devinettes.</li><li>Jouer au jeu du mime.</li><li>Jouer à la phrase sans fin.</li><li>Faire une activité avec du papier et des crayons comme : de drôles de bonshommes, un dessin en miroir…</li><li> … </li></ul></li><li>Faire le plein d’activités artistiques : <ul><li>La pâte à modeler</li><li>La pâte à sel.</li><li>La peinture.</li><li>Le dessin.</li><li>Des pauses musicales : en chantant, en jouant de l’harmonica, des maracas, du tambourin, ou encore en écoutant des styles de musique différents. </li><li>… </li></ul></li><li>Proposez-lui des jeux <ul><li>Un jeu de société adapté à son jeune âge.</li><li>Des puzzles.</li><li>Des Playmobiles.</li><li>De cache-cache. </li><li>… </li></ul></li><li>La lecture est pour l'enfant le moyen de bâtir : <ul><li>Sa personnalité.</li><li>Sa confiance en soi.</li><li>La rapidité de compréhension.</li><li>Le discernement.</li><li>La justesse de jugement et donc le sens de la justice.</li><li>L'écoute.</li><li>Le sens de l'évasion.</li><li>La liberté.</li><li>La créativité.</li><li>L'indépendance.</li><li>La maturité</li></ul></li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommander de mettre votre adolescent devant un écran pour l’occuper car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>

                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Des astuces pour occuper votre adolescent sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>L’impliquer dans toutes les tâches ménagères : <ul><li>passer un coup de balayette, faire les poussières, nettoyer les vitres…</li><li>Vous pouvez également lui proposer de mettre la table. </li><li>Vous pouvez également lui proposer de vous aider quand vous faites la cuisine, en mélangeant l’appareil à quiche par exemple, ou en nettoyant les légumes d'une salade…</li><li> … </li></ul></li><li>Faire des activités physiques. </li><li>Faire des activités artistiques. </li><li>Faire des jeux de sociétés. </li><li>Faire des jeux de constructions. </li><li>Faire de la lecture qui est un moyen de bâtir : <ul><li>Sa personnalité.</li><li>Sa confiance en soi.</li><li>Sa rapidité de compréhension</li><li>Le discernement.</li><li>La justesse de jugement et donc le sens de la justice.</li><li>L'écoute.</li><li>Le sens de l'évasion.</li><li>La liberté.</li><li>La créativité.</li><li>L'indépendance.</li><li>La maturité.</li></ul></li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                    <?php endif ?>

                    <?php if($_POST["telephone_occupe"]==="non"): ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                    <h4 class="sous_question_label">2.3. Pour l’aider à manger ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["telephone_manger"]; ?></span></p>
                    <?php if($_POST["telephone_manger"] === "oui") : ?>
                        <?php if($tranche_age == "0" || $tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran pour le faire manger car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Le moment du repas est un moment de partage et d’apprentissage. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son enfant devant un écran pendant le repas, c’est l’exposé aux : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Risque de surpoids et d’obésité => car l’enfant mange par automatisme donc son attention est captée par l’écran. </li><li>Risque du trouble du langage et de la communication => temps volé aux échanges verbaux. </li><li>Risque de trouble de la motricité fine => temps volé à l’acquisition de la motricité fine (toucher…)</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "4" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran pour le faire manger car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Le moment de repas est un moment de partage et d’apprentissage. Mettre son adolescent devant un écran pendant le repas c’est l’exposé aux : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Risque de surpoids et d’obésité => car l’adolescent mange par automatisme, car son attention est captée par l’écran. </li><li>Risque du trouble du langage et de la communication => temps volé aux échanges verbaux.</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son enfant à manger sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Il faudra être patient et positif </li><li>Votre enfant a besoin de votre attention, de vos paroles et de votre réaction.</li><li>À partir de 4 mois calez-le bien assis, à la bonne hauteur, avec un grand bavoir </li><li>Laissez-lui des choses à toucher </li><li>Donnez-lui une cuillère </li><li>Pendant qu’il explore poursuivez le repas </li><li>Parlez-lui dans votre langue maternelle</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "1" ): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Dès 3 ans, l’enfant commence à manger comme les adultes, mais dans des quantités moindres. C’est le temps de l’éducation au goût qui passe par l’exemple, mais aussi par l’apprentissage. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son enfant à manger sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Il faudra être patient et positif.</li><li>L’éducation au goût est très importante. Il faut y aller progressivement et s’armer de patience ! Utilisez votre imagination : variez les aliments, leur préparation, leur mode de cuisson, leur présentation… </li><li>Rendre la nourriture attractive pour donner envie de manger. </li><li>Cuisiner les légumes avec du goût en mettant des épices ou les cuire dans du bouillon. </li><li>Mélanger les légumes à son aliment préféré. </li><li>Manger en famille quand c’est possible. </li><li>Le laisser cuisiner avec maman ou papa. </li><li>Bien le fatiguer et le stimuler pour réveiller la faim. </li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif ?>

                        <?php if($tranche_age == "2" || $tranche_age == "3" ): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Le temps du repas est un temps d’éducation et d’apprentissage.</p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son enfant à manger sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Il faudra être patient et positif.</li><li>L’éducation au goût est très importante. Il faut y aller progressivement et s’armer de patience ! Utilisez votre imagination : variez les aliments, leur préparation, leur mode de cuisson, leur présentation… </li><li>Rendre la nourriture attractive pour donner envie de manger. </li><li>Cuisiner les légumes avec du goût en mettant des épices ou les cuire dans du bouillon. </li><li>Mélanger les légumes à son aliment préféré. </li><li>Manger en famille quand c’est possible. </li><li>Le laisser cuisiner avec maman ou papa. </li><li>Bien le fatiguer et le stimuler pour réveiller la faim. </li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4" ): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Le temps du repas est un temps d’éducation et d’apprentissage. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Il faudra éduquer votre adolescent à manger à table sans avoir recours aux écrans. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son adolescent à manger sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Il faudra être patient et positif </li><li>Apprenez-lui le plaisir de manger </li><li>Diversifiez les plats et titillez sa curiosité. Utilisez votre imagination : variez les aliments, leur préparation, leur mode de cuisson, leur présentation… </li><li>Rendre la nourriture attractive pour donner envie de manger. </li><li>Cuisiner les légumes avec du goût en mettant des épices ou les cuire dans du bouillon. </li><li>Mélanger les légumes à son aliment préféré. </li><li>Manger en famille quand c’est possible. </li><li>Le laisser cuisiner avec maman ou papa. </li><li>Lui cuisiner ses plats préférés </li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["telephone_manger"]==="non"): ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>

                    <h4 class="sous_question_label">2.4. Pour s’endormir ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["telephone_endormir"]; ?></span></p>
                    <?php if($_POST["telephone_endormir"] === "oui") : ?>
                        
                        <?php if($tranche_age == "0" || $tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger ">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran pour l’aider à dormir car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son enfant devant un écran pour l’aider à dormir c’est l’exposé : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>À la lumière bleue de l’écran qui va induire une modification de la sécrétion de la Mélatonine et ainsi provoquer un trouble du sommeil. </li><li>Il peut être exposé à des images ou des scènes non adaptées à son âge et induire à une exposition à une violence audiovisuelle qui pourra se manifester par des cauchemars, de la peur, l’angoisse…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son enfant à s’endormir sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:list -->
                                <ul><li>Mettre en place une routine pour le coucher va rassurer l’enfant et le mettre dans de bonnes conditions pour dormir. </li><li>Pour le rassurer et l’aider à s’endormir dans son lit, vous pouvez rester quelques minutes à côté de lui. </li><li>Utiliser les livres : racontez-lui une histoire adaptée à son âge avant de le coucher. </li><li>Faites-lui des papouilles ou un massage : Les petites papouilles et les massages permettent à l’enfant de se sentir bien et de s’apaiser. Grâce à ce moment de détente, sa respiration va ralentir, ses muscles se détendre, son rythme cardiaque et sa pression artérielle diminuer. </li><li>Rassurez votre enfant : Parlez avec lui et rassurez-le. Le rassurer avec une voix douce va apaiser votre enfant et le placer dans de meilleures conditions pour dormir. </li><li>Laissez entrouverte la porte de sa chambre : Le son de votre voix et les bruits de la maison contribuent à apaiser et à rassurer votre enfant. Ces bruits de fond lui permettront de se sentir moins seul.</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger ">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran pour l’aider à dormir car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son adolescent devant un écran pour l’aider à dormir c’est l’exposé : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>A la lumière bleue de l’écran bleu qui va induire une modification de la sécrétion de la mélatonine et ainsi induire un trouble du sommeil</li><li>Il peut être exposé à des images ou des scènes non adaptées à son âge et induire à une exposition à une violence audiovisuelle qui pourra se manifester par des cauchemars, la peur, l’angoisse…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>A cet âge, l’apprentissage se fait grâce aux activités créatives et aux jeux, l'adolescent exerce son adresse et élargit sa palette de connaissances. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son adolescent à apprendre sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Se servir d’outils (découvrir, assembler, coller, construire, mouler, sculpter, tresser, manipuler) </li><li>Jeux de construction</li><li> Proposer des techniques, des idées et des matériaux </li><li>Jouer de l’instrument de musique </li><li>Lecture</li><li>jeux de logique </li><li>Jeux de découverte et d’apprentissage</li><li>Thématique autour des sciences, aventure, technologie, évasion</li><li> …</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["telephone_endormir"]==="non"): ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                    <h4 class="sous_question_label">2.5. Pour son apprentissage ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["telephone_apprentissage"]; ?></span></p>
                    <?php if($_POST["telephone_apprentissage"] === "oui") : ?>
                        <?php if($tranche_age == "0" || $tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son enfant devant un écran pour son apprentissage c’est l’exposé aux un retard des acquisitions </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Trouble de l’attention </li><li>Trouble du langage </li><li>Trouble de la motricité fine </li><li>Trouble de l’écriture, dessin, lecture </li></ul>
                                <!-- /wp:list -->
                                <?php if($tranche_age == "0" ): ?>
                                    <!-- wp:paragraph -->
                                    <p>Car jusqu’à l’âge de ses 3 ans, le cerveau de votre enfant n’a pas encore la capacité d’interposer les objets virtuels à la réalité, c’est qu’on appelle scientifiquement « le déficit du transfert »</p>
                                    <!-- /wp:paragraph -->
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son adolescent devant un écran pour son apprentissage c’est l’exposé aux un retard des acquisitions </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Trouble de l’attention </li><li>Trouble du langage </li><li>Trouble de la motricité fine </li><li>Trouble de l’écriture, dessin, lecture </li></ul>
                                <!-- /wp:list -->
                                <?php if($tranche_age == "0" ): ?>
                                    <!-- wp:paragraph -->
                                    <p>Car jusqu’à l’âge de ses 3 ans, le cerveau de votre adolescent n’a pas encore la capacité d’interposer les objets virtuels à la réalité, c’est qu’on appelle scientifiquement « le déficit du transfert »</p>
                                    <!-- /wp:paragraph -->
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son enfant à apprendre sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Parlez souvent à votre bébé, et ce, dès sa naissance. Faites-le pendant que vous le nourrissez, le changez, l’habillez, le calmez, lui donnez le bain … </li><li>Etablissez un contact visuel et rapprochez-vous de lui lorsque vous lui parlez. </li><li>Les relations chaleureuses, constantes et positives favorisent le développement du cerveau de votre bébé et le protègent des effets négatifs du stress. </li><li>Participez à des activités amusantes. Parler, lire et chanter à votre bébé constituent une façon facile et amusante de favoriser son développement. </li><li>Les visages aimants et souriants des adultes qui interagissent avec lui sont ses meilleurs jouets. </li><li>Nommez les choses qui attirent son attention (ex. : un jouet qu’il essaie d’attraper, l’oiseau posé sur le bord de la fenêtre…). </li><li>Appelez souvent votre bébé par son prénom afin qu’il le reconnaisse. Son prénom est souvent un des premiers mots qu’il reconnaît. </li><li>Ne parlez pas en « bébé » à votre enfant (ex. : ne dites pas « kiki » pour « biscuit »). Il doit apprendre les vrais mots.</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2"): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>À cet âge l’apprentissage se fait grâce aux activités créatives et aux jeux, l'enfant exerce son adresse et élargit sa palette de connaissances. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son enfant à apprendre sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Dessiner va l’aider à développer son imaginaire. </li><li>Le coloriage va l'aide à appréhender la notion d'espace. </li><li>Le collage, modelage, va l’aider dans la préhension de la motricité fine et ainsi l’écriture.</li><li>La peinture va l’aider dans la préhension de la motricité fine et ainsi l’écriture.</li><li>Les gommettes vont l’aider dans la préhension de la motricité fine et ainsi l’écriture.</li><li>Parlez-lui, expliquez-lui les choses pour enrichir son vocabulaire. </li><li>Lisez-lui des histoires, commenter avec lui la vie courante ainsi vous enrichissez son vocabulaire </li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif ?>

                        <?php if($tranche_age == "3"): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>À cet âge l’apprentissage se fait grâce aux activités créatives et aux jeux, l'enfant exerce son adresse et élargit sa palette de connaissances. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son enfant à apprendre sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Se servir d’outils (découvrir, assembler, coller, construire, mouler, sculpter, tresser, manipuler) .</li><li>Jeux de construction.</li><li>Proposer des techniques, des idées et des matériaux .</li><li>Jouer de l’instrument de musique.</li><li>Lecture.</li><li>Jeux de logique.</li><li>Jeux de découverte et d’apprentissage.</li><li>Thématique autour des sciences, aventure, technologie, évasion.</li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif ?>


                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>À cet âge l’apprentissage se fait grâce aux activités créatives et aux jeux, l'adolescent exerce son adresse et élargit sa palette de connaissances. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Des astuces pour aider son adolescent à apprendre sans avoir recours aux écrans : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Dessiner va l’aider à développer son imaginaire. </li><li>Le coloriage va l'aide à appréhender la notion d'espace. </li><li>Le collage, modelage, va l’aider dans la préhension de la motricité fine et ainsi l’écriture.</li><li>La peinture va l’aider dans la préhension de la motricité fine et ainsi l’écriture.</li><li>Les gommettes vont l’aider dans la préhension de la motricité fine et ainsi l’écriture.</li><li>Parlez-lui, expliquez-lui les choses pour enrichir son vocabulaire. </li><li>Lisez-lui des histoires, commenter avec lui la vie courante ainsi vous enrichissez son vocabulaire </li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif ?>
                    <?php endif ?>
                    <?php if($_POST["telephone_apprentissage"]==="non"): ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                    <h4 class="sous_question_label">2.6. Pour parler avec la famille ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["telephone_parler"]; ?></span></p>
                    <?php if($_POST["telephone_parler"] == "oui") : ?>
                        
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran pour pouvoir parler en famille car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran pour pouvoir parler en famille car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["telephone_parler"]==="non"): ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>


                    <!-- Affihes les infos -->
                    <?php if($tranche_age == "0") : ?>
                        <div class="alert alert-info">
                            <!-- wp:paragraph -->
                            <p>Les échanges avec votre enfant à table ou en famille sont très formateurs et enrichissants pour votre enfant. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>Il va apprendre du nouveau vocabulaire et améliorer son langage et sa communication. </li><li>La communication aide l’enfant à bâtir une relation de confiance avec un adulte, en plus de lui permettre l’expression de ses besoins et de ses émotions. </li><li>Parler aide également à développer la pensée. Et plus la pensée se développe, plus le langage peut se développer également.</li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif; ?>
                    <?php if($tranche_age == "1" || $tranche_age == "2"): ?>
                        <div class="alert alert-info">
                            <!-- wp:paragraph -->
                            <p>Les échanges avec votre enfant à table ou en famille sont très formateurs et enrichissants pour votre enfant. </p>
                            <!-- /wp:paragraph -->
                            <!-- wp:list -->
                            <ul><li>Il va apprendre du nouveau vocabulaire et améliorer son langage et sa communication. </li><li>La communication aide l’enfant à bâtir une relation de confiance avec un adulte, en plus de lui permettre l’expression de ses besoins et de ses émotions. </li><li>Parler aide également à développer la pensée. Et plus la pensée se développe, plus le langage peut se développer également.</li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif; ?>
                    <?php if($tranche_age == "3"): ?>
                        <div class="alert alert-info">
                            <!-- wp:paragraph -->
                            <p>Les échanges avec votre enfant à table ou en famille sont très formateurs et enrichissants pour votre enfant. </p>
                            <!-- /wp:paragraph -->
                            <!-- wp:list -->
                            <ul><li>La communication aide l’enfant à bâtir une relation de confiance avec un adulte, en plus de lui permettre l’expression de ses besoins et de ses émotions. </li><li>Parler aide également à développer la pensée. Et plus la pensée se développe, plus le langage peut se développer également.</li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif; ?>

                    <?php if($tranche_age == "4"): ?>
                        <div class="alert alert-info">
                            <!-- wp:paragraph -->
                            <p>Les échanges avec votre adolescent à table ou en famille sont très formateurs et enrichissants pour votre adolescent. </p>
                            <!-- /wp:paragraph -->
                            <!-- wp:list -->
                            <ul><li>La communication aide l’adolescent à bâtir une relation de confiance avec un adulte, en plus de lui permettre l’expression de ses besoins et de ses émotions. </li><li>Parler aide également à développer la pensée. Et plus la pensée se développe, plus le langage peut se développer également.</li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif; ?>
                </li>
				<li>
                    <h3 class="question_label">3. Laissez-vous en général votre télé allumée en permanence (ou presque) ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["television_allume"]; ?></span></p>
                    
                    <?php if($_POST["television_allume"]==="oui"): ?>
                    <?php if($tranche_age == "0" ): ?>
                    <div class="alert alert-danger" >
                        <!-- wp:list -->
                        <ul><li>La télévision allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parent-enfant. </li><li>Sans le vouloir vous exposer votre enfant à des programmes non destinés aux enfants (actualités, sport, programme de jeu, film…) qui peuvent être une source de violence visuelle. </li><li>La télévision en bruit de fond interrompt en permanence l’attention de votre enfant => son attention est en permanence morcelée par les courtes interruptions où il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité. </li><li>Donc <ul><li>Pour chaque augmentation du temps d’écran de 30 minutes par jour => augmente de 50% le risque que l’enfant souffre d’un retard du langage, car lorsque la télévision est allumée en arrière-plan toute la journée, un enfant entendra 13 400 mots en moins par semaine que si la télévision était éteinte => Diminution de son temps d’échange verbal => Trouble du langage. </li><li>Diminution de son temps d’attention aux jeux => Trouble de l’apprentissage. </li><li>Exposition à une violence visuelle.</li></ul></li></ul>
                        <!-- /wp:list -->
                    </div>
                    <?php endif; ?>
                    
                    <?php if($tranche_age == "1" ): ?>
                    <div class="alert alert-danger">
                        <!-- wp:list -->
                        <ul><li>La télévision allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parent-enfant. </li><li>Sans le vouloir vous exposer votre enfant à des programmes non destinés aux enfants (actualités, sport, programme de jeu, film…) qui peuvent être une source de violence visuelle. </li><li>La télévision en bruit de fond interrompt en permanence l’attention de votre enfant => son attention est en permanence morcelée par les courtes interruptions où il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité. </li><li>Donc <ul><li>Pour chaque augmentation du temps d’écran de 30 minutes par jour => augmente de 50% le risque que l’enfant souffre d’un retard du langage, car lorsque la télévision est allumée en arrière-plan toute la journée, un enfant entendra 13 400 mots en moins par semaine que si la télévision était éteinte => Diminution de son temps d’échange verbal => Trouble du langage. </li><li>Diminution de son temps d’attention aux jeux => Trouble de l’apprentissage. </li><li>Exposition à une violence visuelle.</li></ul></li></ul>
                        <!-- /wp:list -->
                    </div>
                    <?php endif; ?>

                    <?php if($tranche_age == "2" || $tranche_age == "3" ): ?>
                    <div class="alert alert-danger">
                        <!-- wp:list -->
                        <ul><li>La télévision allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parent-enfant. </li><li>Sans le vouloir vous exposer votre enfant à des programmes non destinés aux enfants (actualités, sport, programme de jeu, film…) qui peuvent être une source de violence visuelle. </li><li>La télévision en bruit de fond interrompt en permanence l’attention de votre enfant => son attention est en permanence morcelée par les courtes interruptions où il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité. </li><li>Donc <ul><li>Diminution de son temps d’attention aux jeux => Trouble de l’apprentissage.</li><li>Exposition à une violence visuelle.</li></ul></li></ul>
                        <!-- /wp:list -->
                    </div>
                    <?php endif; ?>
                    
                    <?php if($tranche_age == "4" ): ?>
                    <div class="alert alert-danger">
                        <!-- wp:list -->
                        <ul><li>La télévision allumée en arrière-plan interfère avec les processus d’apprentissage et avec les interactions parent-enfant. </li><li>Sans le vouloir vous exposer votre adolescent à des programmes non destinés aux adolescents (actualités, sport, programme de jeu, film…) qui peuvent être une source de violence visuelle. </li><li>La télévision en bruit de fond interrompt en permanence l’attention de votre adolescent => son attention est en permanence morcelée par les courtes interruptions où il essaie de comprendre ce qui se passe à l’écran avant de revenir à son activité. </li><li>Donc <ul><li>Diminution de son temps d’attention aux jeux => Trouble de l’apprentissage.</li><li>Exposition à une violence visuelle.</li></ul></li></ul>
                        <!-- /wp:list -->
                    </div>
                    <?php endif; ?>
                    
                    <?php if($tranche_age == "0" || $tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3" ): ?>
                        <div class="alert alert-info">
                            <!-- wp:paragraph -->
                            <p><strong>Astuces pour limiter l’accès aux écrans </strong></p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>Pour éviter que la télévision ne soit allumée par votre enfant : <ul><li>Mettre un code. </li><li>Ranger la télécommande.</li><li>Fermer la télévision après utilisation. </li></ul></li><li>Et n’hésitez pas à vous interroger sur vos habitudes. </li><li>Si vous avez le reflex d’allumer la télévision changez vos habitudes : <ul><li>En allument la radio. </li><li>En écoutant de la musique. </li><li>En jouant ou en faisant une activité avec votre enfant.</li></ul></li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif; ?>
                    <?php if($tranche_age == "4"): ?>
                        <div class="alert alert-info">
                            <!-- wp:paragraph -->
                            <p><strong>Astuces pour limiter l’accès aux écrans </strong></p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>Pour éviter que la télévision ne soit allumée par votre adolescent : <ul><li>Mettre un code. </li><li>Ranger la télécommande.</li><li>Fermer la télévision après utilisation. </li></ul></li><li>Et n’hésitez pas à vous interroger sur vos habitudes. </li><li>Si vous avez le reflex d’allumer la télévision changez vos habitudes : <ul><li>En allument la radio. </li><li>En écoutant de la musique. </li><li>En jouant ou en faisant une activité avec votre adolescent.</li></ul></li></ul>
                            <!-- /wp:list -->
                        </div>
                    <?php endif; ?>

                <?php endif; ?>

                    
                    <?php if($_POST["television_allume"]==="non"): ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                </li>
				<li>
                    <h3 class="question_label">4. Votre enfant passe-t-il du temps devant un écran ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong>
                        <span class="valeur_reponse">
                            <?= $_POST["temps_ecran"]; ?> 
                            <?php if($_POST["temps_ecran"] === "oui") {
                                echo  '('.$_POST["temps_enfant_ecran"].')';
                            }
                            ?>
                        </span>
                    </p>
                    <?php if($_POST["temps_ecran"] === "oui"): ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p><em>Sauf à une condition : pas plus de 1 fois par jour </em></p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Pour regarder des photos sur une tablette</li><li>Faire un coucou à la famille par Skype ou autre </li><li>Regarder une comptine sur les genoux d’un parent pendant quelque minutes</li></ul>
                                <!-- /wp:list -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Car votre enfant : </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:list -->
                                <ul><li>A besoin de construire ses repères dans l’espace et dans le temps. </li><li>N’apprend rien devant les écrans, car il n’a pas encore la faculté de comprendre ce qu’il regarde et ainsi de faire le lien avec la réalité. </li><li>Par exemple : les enfants qui apprennent à compter ou à parler une autre langue via les écrans, ils ne font que répéter ce qu’ils ont entendu en boucle sans savoir la signification exacte du mot</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "1" ): ?>
                            <?php if($_POST["temps_enfant_ecran"] == "Moins de 30 mn" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["temps_enfant_ecran"] == "30 mn à 1 h" ||
                                     $_POST["temps_enfant_ecran"] == "1 h à 1 h 30 mn" ||
                                     $_POST["temps_enfant_ecran"] == "1 h 30 mn à 2 h" ||
                                     $_POST["temps_enfant_ecran"] == "Plus de 2 h" ): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran plus de 30 minutes par jour car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Le recours à l’écran semble au début magique car votre enfant est tellement fasciné qu’il se laisse faire (manger, le calmer…) => Mais cela crée une telle dépendance qu’il devient impossible de lui retirer sans pleurs et ainsi induire une dépendance aux écrans.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Attention aux messages publicitaires qui peuvent influencer le mode de vie et la consommation de votre enfant.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Il faut savoir que l’accompagnement, à cet âge, est une règle d’or il faudra toujours commenter les scènes regardées à l’écran avec votre enfant. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Il est préférable de prévoir des temps courts de 10-15 min avec des moments sans écrans en essayant de passer à une autre activité. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Car votre enfant a besoin </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>De se dépenser, de bouger. </li><li> Manipuler. </li><li> Comprendre la notion de causalité. </li><li>D’utiliser la 3D. </li><li>De dessiner avec un vrai crayon, pour le préparer à l’écriture </li><li>De lire de vrais livres pour le préparer à l’apprentissage de la lecture.</li><li>…</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if($tranche_age == "2" ): ?>
                            <?php if($_POST["temps_enfant_ecran"] == "Moins de 30 mn" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($_POST["temps_enfant_ecran"] == "30 mn à 1 h" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["temps_enfant_ecran"] == "1 h à 1 h 30 mn" ||
                                     $_POST["temps_enfant_ecran"] == "1 h 30 mn à 2 h" ||
                                     $_POST["temps_enfant_ecran"] == "Plus de 2 h" ): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran plus de 1 heure par jour car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Le recours à l’écran semble au début magique car votre enfant est tellement fasciné qu’il se laisse faire (manger, le calmer…) => Mais cela crée une telle dépendance qu’il devient impossible de lui retirer sans pleurs et ainsi induire une dépendance aux écrans.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Attention aux messages publicitaires qui peuvent influencer le mode de vie et la consommation de votre enfant.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Il faut savoir que l’accompagnement, à cet âge, est une règle d’or il faudra toujours commenter les scènes regardées à l’écran avec votre enfant. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Il est préférable de prévoir des temps courts de 10-15 min avec des moments sans écrans en essayant de passer à une autre activité. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Car votre enfant a besoin </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>De se dépenser, de bouger.</li><li>De manipuler.</li><li>De comprendre la notion de causalité. </li><li>D’utiliser la 3D. </li><li>De dessiner avec un vrai crayon, pour le préparer à l’écriture </li><li>De lire de vrais livres pour le préparer à l’apprentissage de la lecture.</li><li>…</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>



                        <?php if($tranche_age == "3" ): ?>
                            <?php if($_POST["temps_enfant_ecran"] == "Moins de 30 mn" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($_POST["temps_enfant_ecran"] == "30 mn à 1 h" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["temps_enfant_ecran"] == "1 h à 1 h 30 mn" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["temps_enfant_ecran"] == "1 h 30 mn à 2 h" ||
                                     $_POST["temps_enfant_ecran"] == "Plus de 2 h" ): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran plus de 1 heure et 30 minutes par jour car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Le recours à l’écran semble au début magique car votre enfant est tellement fasciné qu’il se laisse faire (manger, le calmer…) => Mais cela crée une telle dépendance qu’il devient impossible de lui retirer sans pleurs et ainsi induire une dépendance aux écrans.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Attention aux messages publicitaires qui peuvent influencer le mode de vie et la consommation de votre enfant.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra toujours vérifier le contenu des scènes que regarde votre enfant. Car votre enfant a besoin : </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>De se dépenser, de bouger. </li><li>D’explorer, manipuler, coordonner.</li><li>De rêver, mémoriser.</li><li>De perfectionner ses attitudes.</li><li>C’est l’âge des principales acquisitions.</li><li>La recherche de savoir.</li><li>De devenir indépendant pendant le jeu.</li><li>Il acquiert la logique mathématique / apprentissage de la lecture.</li><li>Découverte du monde (animal) (curiosité de leur vie).</li><li>À cet âge, l’enfant a de l’énergie pour faire des apprentissages. </li><li>…</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>


                        <?php if($tranche_age == "4"): ?>
                            <?php if($_POST["temps_enfant_ecran"] == "Moins de 30 mn" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($_POST["temps_enfant_ecran"] == "30 mn à 1 h" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["temps_enfant_ecran"] == "1 h à 1 h 30 mn" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["temps_enfant_ecran"] == "1 h 30 mn à 2 h" ): ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["temps_enfant_ecran"] == "Plus de 2 h" ): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge la durée maximale d’écrans ne devrait pas dépasser les 2 heures par jour car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Attention au risque de dépense aux écrans à cet âge.</p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Attention aux messages publicitaires qui peuvent influencer le mode de vie et la consommation de votre adolescent.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra toujours contrôler le contenu des scènes que regarde votre adolescent.</p>
                                    <!-- /wp:paragraph -->                               
                                    <!-- wp:paragraph -->
                                    <p>Mettre en place avec votre adolescent un contrat de confiance ou une charte familiale pour la bonne utilisation des écrans et ainsi éviter que votre adolescent ne soit exposé aux risques liés à la sur exposition aux écrans.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>


                    <?php endif; ?>


                    <?php if($_POST["temps_ecran"] === "non"): ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien, vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "1" ): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien, mais à cet âge votre enfant il peut regarder des programmes adaptés à son âge sans dépasser les 30 minutes par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "2" ): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien, mais à cet âge votre enfant il peut regarder des programmes adaptés à son âge sans dépasser 1 heure par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "3" ): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien mais il n’est pas déconseiller de mettre votre enfant à cet âge devant un programme adapté à son âge sans dépasser la limite de 1heure 30 minutes par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "4" ): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien mais il n’est pas déconseiller de mettre votre adolescent à cet âge devant un programme adapté à son âge sans dépasser la limite de 2 heures par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                </li>
                
                <li>
                    <h3 class="question_label">5. Un ou des écrans sont-ils allumés pendant les repas ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong>
                        <span class="valeur_reponse">
                            <?= $_POST["allume_repas"]; ?> 
                        </span>
                    </p>

                    <?php if($_POST["allume_repas"] === "non"): ?>
                    <div class="alert alert-success">
                        <!-- wp:paragraph -->
                        <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                        <!-- /wp:paragraph -->
                    </div>
                    <?php endif; ?>

                    <!-- Cas de réponse oui on affiches les sous questions -->
                    <?php if($_POST["allume_repas"] === "oui") : ?>
                        <h4 class="sous_question_label">5.1. Refus de manger ?</h4>
                        <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["allume_ecran_cause_refus_manger"]; ?></span></p>
                        <?php if($_POST["allume_ecran_cause_refus_manger"] === "oui") : ?>
                            <!-- En fonction de la tranche d'age afficher une réponse -->
                            <?php if($tranche_age == "0" ): ?>
                                <div class="alert alert-danger ">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Il faut faire attention car si votre enfant refuse de manger et n’ouvre la bouche que quand vous lui allumer un écran, vous rentrez là dans un processus de chantage et un risque que votre enfant devienne dépendant aux écrans. </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Le moment du repas est un moment de partage et d’apprentissage. </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Mettre son enfant devant un écran pendant le repas c’est l’exposé aux : </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:list -->
                                    <ul><li>Risque de surpoids et d’obésité => car l’enfant mange par automatisme, car son attention est captée par l’écran. </li><li>Risque du trouble du langage et de la communication => temps volé aux échanges verbaux. </li><li>Risque de trouble de la motricité fine => temps volé à l’acquisition de la motricité fine (toucher…)</li></ul>
                                    <!-- /wp:list -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Des astuces pour aider votre enfant à manger sans écrans : </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:list -->
                                    <ul><li>Calez-le bien assis, à la bonne hauteur. </li><li>Protéger ses habilles avec un grand bavoir. </li><li>Donnez-lui à manger avec des couverts adaptés à son âge => une petite cuillère, un bol ou une assiette en plastique. </li><li>Laissez-lui des choses à toucher. </li><li>Donnez-lui une cuillère.</li><li>Pendant qu’il explore poursuivez le repas.</li><li>Parlez-lui dans votre langue maternelle.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3" ): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Il faut faire attention car si votre enfant refuse de manger et n’ouvre la bouche que quand vous lui allumer un écran, vous rentrez là dans un processus de chantage et un risque que votre enfant devienne dépendant aux écrans. </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Le moment du repas est un moment de partage et d’apprentissage. </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:paragraph -->
                                    <p>Mettre son enfant devant un écran pendant le repas c’est l’exposé aux : </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:list -->
                                    <ul><li>Risque de surpoids et d’obésité => car l’enfant mange par automatisme, car son attention est captée par l’écran. </li><li>Risque du trouble du langage et de la communication => temps volé aux échanges verbaux. </li><li>Risque de trouble de la motricité fine => temps volé à l’acquisition de la motricité fine (toucher…)</li></ul>
                                    <!-- /wp:list -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Des astuces pour aider votre enfant à manger sans écrans : </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Il faudra être patient et positif.</li><li>L’éducation au goût est très importante. </li><li>Il faut y aller progressivement et s’armer de patience ! Utilisez votre imagination : variez les aliments, leur préparation, leur mode de cuisson, leur présentation… </li><li>Rendre la nourriture attractive pour donner envie de manger. </li><li>Cuisiner les légumes avec du goût en mettant des épices ou les cuire dans du bouillon. </li><li>Mélanger les légumes à son aliment préféré. </li><li>Manger en famille quand c’est possible. </li><li>Le laisser cuisiner avec maman ou papa. </li><li>Bien le fatiguer et stimuler pour réveiller la faim. </li><li>…</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>
                        
                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran pour manger car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Attention au risque de dépendance. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Le moment du repas est un moment de partage et d’apprentissage. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Mettre son adolescent devant un écran pendant le repas c’est l’exposé aux :</p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Risque de surpoids et d’obésité => car l’adolescent mange par automatisme, car son attention est captée par l’écran. </li><li>Risque du trouble du langage et de la communication => temps volé aux échanges verbaux.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Des astuces pour aider votre adolescent à manger sans écran </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Il faudra être patient et positif </li><li>Utilisez votre imagination : variez les aliments, leur préparation, leur mode de cuisson, leur présentation… </li><li>Rendre la nourriture attractive pour donner envie de manger. </li><li>Cuisiner les légumes avec du goût en mettant des épices ou les cuire dans du bouillon. </li><li>Mélanger les légumes à son aliment préféré. </li><li>Manger en famille quand c’est possible. </li><li>Le laisser cuisiner avec maman ou papa. </li><li>…</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if($_POST["allume_ecran_cause_refus_manger"] === "non"): ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <h4 class="sous_question_label">5.2. Utilisation de son smartphone lors qu’on donne à manger à son enfant ?</h4>
                        <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["allume_ecran_cause_donne_manger"]; ?></span></p>
                        <?php if($_POST["allume_ecran_cause_donne_manger"] === "oui") : ?>
                            <?php if($tranche_age == "0" ): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. Attention à votre propre consommation des écrans </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Des études ont prouvé que la consommation des enfants aux écrans est corrélée à la consommation de leurs parents. </li><li>Si les parents utilisent régulièrement les écrans l’enfant sera forcément exposé aux écrans. </li><li>Des études ont prouvé qu’un enfant moins exposé dans son enfance aux écrans devient un adulte moins dépendant aux écrans.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                                
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Lors du repas votre enfant aura besoin de votre attention, de vos paroles et de votre réaction. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Il aura également besoin des interactions en 3 dimensions avec une personne pour pouvoir se développer. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Les interactions entre les parents et l’enfant à cet âge sont tellement importantes qu’il faudra que chaque moment d’éveil soit un moment d’échange à 100%.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "1" || $tranche_age == "2" ): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Attention à votre propre consommation des écrans </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Des études ont prouvé que la consommation des enfants aux écrans est corrélée à la consommation de leurs parents. </li><li>Si les parents utilisent régulièrement les écrans l’enfant sera forcément exposer aux écrans. </li><li>Des études ont prouvé qu’un enfant moins exposé dans son enfance aux écrans : est un adulte moins dépendant des écrans.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Lors du repas, votre enfant aura besoin de votre attention, de vos paroles et de votre réaction, pour apprendre et grandir. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Il aura également besoin des interactions en 3 dimensions avec une personne pour pouvoir se développer. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Les interactions entre les parents et enfant à cet âge sont très importantes</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Attention à votre propre consommation des écrans </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Des études ont prouvé que la consommation des enfants aux écrans est corrélée à la consommation de leurs parents. </li><li>Si les parents utilisent régulièrement les écrans l’enfant sera forcément exposer aux écrans. </li><li>Des études ont prouvé qu’un enfant moins exposé dans son enfance aux écrans : est un adulte moins dépendant des écrans.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Lors du repas votre enfant aura besoin de votre attention, de vos paroles et de votre réaction, pour apprendre et grandir.</p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Les interactions entre les parents et enfant, à cet âge, sont très important.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Attention à votre propre consommation des écrans </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Des études ont prouvé que la consommation des adolescents aux écrans est corrélée à la consommation de leurs parents. </li><li>Si les parents utilisent régulièrement les écrans l’adolescent sera forcément exposer aux écrans. </li><li>Des études ont prouvé qu’un adolescent moins exposé dans son enfance aux écrans : est un adulte moins dépendant des écrans.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Lors du repas votre adolescent aura besoin de votre attention, de vos paroles et de votre réaction, pour apprendre et grandir.</p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Les interactions entre les parents et adolescent, à cet âge, sont très important.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if($_POST["allume_ecran_cause_donne_manger"] === "non"): ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <h4 class="sous_question_label">5.3. La télévision allumée en fond d’écran ou pour regarder les informations ?</h4>
                        <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["allume_ecran_cause_information"]; ?></span></p>
                        <?php if($_POST["allume_ecran_cause_information"] === "oui") : ?>
                            <?php if($tranche_age == "0" ): ?>
                                <div class="alert alert-danger ">
                                <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Une télévision allumée c’est : </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Passage des images choquantes pour votre enfant. </li><li>Vous êtes moins attentif à votre enfant et à l’échange. </li><li>Conversation mal entendue à cause du son. </li><li>Manger de façon automatique, sans prendre gare aux goûts, aux odeurs et aux textures. </li><li>Prendre l’habitude de se remplir au lieu de manger.</li></ul>
                                    <!-- /wp:list -->

                                    <!-- wp:paragraph -->
                                    <p>Votre enfant risque : </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>De devenir obèse => IMC qui augmente pour chaque heure passé devant un écran pour un enfant &lt; 2 ans.</li><li>De voir des images choquantes pour l’enfant = violence visuelle avec les risques connu comme comportement violent, stress, cauchemars… </li><li>Echange pauvre avec les parents qui induira à un trouble du langage et de la communication par la suite.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3"  ): ?>
                                <div class="alert alert-danger ">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de laisser la télévision allumée en fond d’écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Une télévision allumée c’est : </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Passage des images choquantes pour votre enfant. </li><li>Vous êtes moins attentif à votre enfant et à l’échange. </li><li>Conversation mal entendue à cause du son. </li><li>Manger de façon automatique, sans prendre garde aux goûts, aux odeurs et aux textures. </li><li>Prendre l’habitude de se remplir au lieu de manger. </li></ul>
                                    <!-- /wp:list -->

                                    <!-- wp:paragraph -->
                                    <p>Votre enfant risque :</p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Le surpoids, l’obésité… </li><li>De voir des images choquantes pour l’enfant = violence visuelle avec les risques connu comme comportement violent, stress, cauchemars… </li><li>Echanges pauvre avec les parents qui induira à un trouble du langage et de la communication</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "4" ): ?>
                                <div class="alert alert-danger ">
                                    <!-- wp:paragraph -->
                                    <p>Il faudra faire attention car à cet âge il n’est pas recommandé de laisser la télévision allumée en fond d’écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:paragraph -->
                                    <p>Une télévision allumée c’est : </p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Passage des images choquantes pour votre adolescent. </li><li>Vous êtes moins attentif à votre adolescent et à l’échange. </li><li>Conversation mal entendue à cause du son. </li><li>Manger de façon automatique, sans prendre garde aux goûts, aux odeurs et aux textures. </li><li>Prendre l’habitude de se remplir au lieu de manger. </li></ul>
                                    <!-- /wp:list -->

                                    <!-- wp:paragraph -->
                                    <p>Votre adolescent risque :</p>
                                    <!-- /wp:paragraph -->

                                    <!-- wp:list -->
                                    <ul><li>Le surpoids, l’obésité… </li><li>De voir des images choquantes pour l’adolescent = violence visuelle avec les risques connu comme comportement violent, stress, cauchemars… </li><li>Echanges pauvre avec les parents qui induira à un trouble du langage et de la communication</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "0" || $tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3"  ): ?>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Les bénéfices d’un repas sans télévision : </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:list -->
                                    <ul><li>Un moment d’échange => les enfants apprennent à s’exprimer => amélioration de leurs expressions orales </li><li>Apprendre des règles de vie (attendre son tour pour parler, bien se tenir…) </li><li>Développer la motricité fine et le goût (apprendre les textures, le gout, le toucher…) </li><li>Développer l’éveil de l’enfant avec le toucher, les goûts, les couleurs et la parole.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-info">
                                    <!-- wp:paragraph -->
                                    <p>Les bénéfices d’un repas sans télévision : </p>
                                    <!-- /wp:paragraph -->
                                    <!-- wp:list -->
                                    <ul><li>Un moment d’échange => les adolescents apprennent à s’exprimer => amélioration de leurs expressions orales </li><li>Apprendre des règles de vie (attendre son tour pour parler, bien se tenir…) </li><li>Développer la motricité fine et le goût (apprendre les textures, le gout, le toucher…) </li><li>Développer l’éveil de l’adolescent avec le toucher, les goûts, les couleurs et la parole.</li></ul>
                                    <!-- /wp:list -->
                                </div>
                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if($_POST["allume_ecran_cause_information"] === "non"): ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>

                

                    
                </li>
                <li>
                    <h3 class="question_label"> 6. Votre enfant passe-t-il du temps devant un écran le matin ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong>
                    <span class="valeur_reponse">
                        <?= $_POST["ecran_matin"]; ?> 
                    </span>
                    </p>
                    <?php if($_POST["ecran_matin"] === "oui"):  ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Pas d’écrans le matin pour un enfant âgé de moins de 3 ans.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info" >
                                <!-- wp:paragraph -->
                                <p>Le petit déjeuner, c’est le repas le plus important de la journée. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Le repas du matin est essentiel pour bien démarrer la journée. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Il est important de réserver un peu de temps au petit déjeuner.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran le matin car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info" >
                                <!-- wp:paragraph -->
                                <p>Pas d’écrans le matin avent d’aller à la maternelle car ce temps est riche : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Pour les apprentissages. </li><li>Pour exercer sa motricité fine lors du petit-déjeuner (boire à la tasse, tartiner, couper…) et lors de l’habillage (enfiler, mettre à l’endroit, boutonner …). </li><li>Il sera plus concentré en arrivant à l’école s’il a joué et a participé à toutes les activités du matin (toilette, repas, habillage…) sans un œil rivé sur l’écran. </li><li>Vous évitez l’excitation sensorielle auditive et visuelle que subit votre enfant en regardant la télévision ou la tablette qui sera source d’ennui ou d’agitation en classe. </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>La visualisation des écrans le matin vide les batteries de votre enfant suite aux flashs visuels, les séquences rapides et les sens aigües, qui vont puiser dans sa réserve d’attention volontaire.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran le matin car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info" >
                                <!-- wp:paragraph -->
                                <p>Pas d’écrans le matin avent d’aller au collège et lycée. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Il sera plus concentré en arrivant à l’école s’il a participé à toutes les activités du matin (toilette, repas, habillage…) sans un œil rivé sur l’écran. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Vous évitez l’excitation sensorielle auditive et visuelle que subit votre adolescent en regardant la télévision ou la tablette qui sera source d’ennui ou d’agitation en classe.</p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>La visualisation des écrans le matin vide les batteries de votre adolescent suite aux flashs visuels, les séquences rapides et les sens aigües, qui vont puiser dans sa réserve d’attention volontaire.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>
                    
                    <?php if($_POST["ecran_matin"] === "non"):  ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>

                </li>
                <li>
                    <h3 class="question_label"> 7.	Votre enfant passe-t-il du temps devant un écran avant de se coucher ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong>
                    <span class="valeur_reponse">
                        <?= $_POST["ecran_avant_coucher"]; ?>
                    </span>
                    </p>
                    <?php if($_POST["ecran_avant_coucher"] === "oui"):  ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Pas d’écrans le soir pour un enfant âgé de moins de 3 ans. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son enfant devant un écran pour l’aider à dormir c’est l’exposé : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>À la lumière bleue qui va induire une modification de la sécrétion de la mélatonine et ainsi provoquer un trouble du sommeil. </li><li>Il peut être exposé à des images ou des scènes non adaptées à son âge et induire à une exposition à une violence audiovisuelle qui pourra se manifester par des cauchemars, de la peur, l’angoisse…</li></ul>
                                <!-- /wp:list -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Le sommeil a un rôle majeur dans le bon développement de l’enfant d’un point de vue physique par la croissance mais aussi d’un point de vue psychique. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>En effet, c’est pendant le sommeil que l’enfant va : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Récupérer de la fatigue physique. </li><li>Grandir en sécrétant l’hormone de croissance. </li><li>Rêver et développer son psychisme. </li><li>Développer sa capacité d’apprentissage et de mémorisation. </li><li>Stimuler son système immunitaire. </li><li>Éliminer les toxines. </li><li>Maintenir sa température corporelle tout au long des 24 heures. </li><li>Réguler la glycémie (mauvais sommeil = risque accru de surpoids). </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>Les besoins de sommeil de votre enfant en fonction de son âge :</p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>0-3 mois : 16 à 20 heures. Répartition sur toute la journée. </li><li>3-6 mois : 16-17 heures. Jusqu’à 9 heures la nuit et 7 à 8 heures durant la journée. </li><li>6 -12 mois : 14 à 15 heures. Environ 11 heures la nuit et 3 à 4 heures durant la journée. </li><li>2-3 ans : 13 heures. Environs 11h la nuit et 2 heures durant la sieste de l’après –midi.</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "1" ): ?>
                        <div class="alert alert-danger">
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran avant le coucher car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:paragraph -->
                            <p>Mettre son enfant devant un écran pour l’aider à dormir c’est l’exposé : </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>À la lumière bleue qui va induire une modification de la sécrétion de la mélatonine et ainsi provoquer un trouble du sommeil </li><li>Il peut être exposé à des images ou des scènes non adaptées à son âge et induire à une exposition, à une violence audiovisuelle qui pourra se manifester par des cauchemars, de la peur, de l’angoisse…</li></ul>
                            <!-- /wp:list -->
                        </div>
                        <div class="alert alert-info">
                             <!-- wp:paragraph -->
                            <p>Le sommeil a un rôle majeur dans le bon développement de l’enfant d’un point de vue physique par la croissance mais aussi d’un point de vue psychique. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:paragraph -->
                            <p>En effet, c’est pendant le sommeil que l’enfant va : </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>Récupérer de la fatigue physique. </li><li>Grandir en sécrétant l’hormone de croissance. </li><li>Rêver et développer son psychisme. </li><li>Développer sa capacité d’apprentissage et de mémorisation. </li><li>Stimuler son système immunitaire. </li><li>Éliminer les toxines. </li><li>Réguler la glycémie (mauvais sommeil = risque accru de surpoids). </li></ul>
                            <!-- /wp:list -->

                            <!-- wp:paragraph -->
                            <p>Il est préférable de favoriser </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul id="block-9c7b4759-88ba-468b-a382-7dbd9a4d4328"><li>Les jeux calmes. </li><li>La lecture. <li>Des jeux de société.</li> </li><li>Des puzzles… </li><li>Introduire un rituel de coucher qu’il faudra lui expliquer lors d’un moment calme par exemple lors d’un bain.</li><li>Expliquer à votre enfant comment fonctionne l’endormissement avec des mots simples. </li></ul>
                            <!-- /wp:list -->

                            <!-- wp:paragraph -->
                            <p>Les besoins de sommeil de votre enfant en fonction de son âge est de 13 heures. Environs 11 heures la nuit et 2 heures durant la sieste de l’après–midi.</p>
                            <!-- /wp:paragraph -->
                        </div>

                        <?php endif; ?>


                        <?php if($tranche_age == "2" ): ?>
                        <div class="alert alert-danger">
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran avant le coucher car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:paragraph -->
                            <p>Mettre son enfant devant un écran pour l’aider à dormir c’est l’exposé : </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>À la lumière bleue qui va induire une modification de la sécrétion de la mélatonine et ainsi provoquer un trouble du sommeil </li><li>Il peut être exposé à des images ou des scènes non adaptées à son âge et induire à une exposition, à une violence audiovisuelle qui pourra se manifester par des cauchemars, de la peur, de l’angoisse…</li></ul>
                            <!-- /wp:list -->
                        </div>
                        <div class="alert alert-info">
                             <!-- wp:paragraph -->
                            <p>Le sommeil a un rôle majeur dans le bon développement de l’enfant d’un point de vue physique par la croissance mais aussi d’un point de vue psychique. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:paragraph -->
                            <p>En effet, c’est pendant le sommeil que l’enfant va : </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul><li>Récupérer de la fatigue physique. </li><li>Grandir en sécrétant l’hormone de croissance. </li><li>Rêver et développer son psychisme. </li><li>Développer sa capacité d’apprentissage et de mémorisation. </li><li>Stimuler son système immunitaire. </li><li>Éliminer les toxines. </li><li>Réguler la glycémie (mauvais sommeil = risque accru de surpoids). </li></ul>
                            <!-- /wp:list -->

                            <!-- wp:paragraph -->
                            <p>Il est préférable de favoriser </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:list -->
                            <ul id="block-9c7b4759-88ba-468b-a382-7dbd9a4d4328"><li>Les jeux calmes. </li><li>La lecture. </li><li> Jeux de société. </li><li>Des puzzles… </li><li>Introduire un rituel de coucher qu’il faudra lui expliquer lors d’un moment calme par exemple lors d’un bain.</li><li>Expliquer à votre enfant comment fonctionne l’endormissement avec des mots simples. </li></ul>
                            <!-- /wp:list -->

                            <!-- wp:paragraph -->
                            <p>Les besoins de sommeil de votre enfant en fonction de son âge est de 10-12h de sommeil /jours.</p>
                            <!-- /wp:paragraph -->
                        </div>

                        <?php endif; ?>


                        <?php if($tranche_age == "3" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran avant le coucher car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son enfant devant un écran pour l’aider à dormir c’est l’exposé : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>À la lumière bleue qui va induire une modification de la sécrétion de la mélatonine et ainsi provoquer un trouble du sommeil </li><li>Il peut être exposé à des images ou des scènes non adaptées à son âge et induire à une exposition, à une violence audiovisuelle qui pourra se manifester par des cauchemars, de la peur, de l’angoisse…</li></ul>
                                <!-- /wp:list -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Le sommeil a un rôle majeur dans le bon développement de l’enfant d’un point de vue physique par la croissance mais aussi d’un point de vue psychique. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>En effet, c’est pendant le sommeil que l’enfant va : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Récupérer de la fatigue physique. </li><li>Grandir en sécrétant l’hormone de croissance. </li><li>Rêver et développer son psychisme. </li><li>Développer sa capacité d’apprentissage et de mémorisation. </li><li>Stimuler son système immunitaire. </li><li>Éliminer les toxines. </li><li>Réguler la glycémie (mauvais sommeil = risque accru de surpoids). </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>Il est préférable de favoriser </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul id="block-9c7b4759-88ba-468b-a382-7dbd9a4d4328"><li>Les jeux calmes. </li><li>La lecture. </li><li> Jeux de société. </li><li>Des puzzles… </li><li>Introduire un rituel de coucher qu’il faudra lui expliquer lors d’un moment calme par exemple lors d’un bain.</li><li>Expliquer à votre enfant comment fonctionne l’endormissement avec des mots simples. </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>Les besoins de sommeil de votre enfant en fonction de son âge est de 12h de sommeil /jours.</p>
                                <!-- /wp:paragraph -->
                            </div>

                        <?php endif; ?>


                        <?php if($tranche_age == "4" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre adolescent devant un écran avant le coucher car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Mettre son adolescent devant un écran pour l’aider à dormir c’est l’exposé : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>À la lumière bleue qui va induire une modification de la sécrétion de la mélatonine et ainsi provoquer un trouble du sommeil </li><li>Il peut être exposé à des images ou des scènes non adaptées à son âge et induire à une exposition, à une violence audiovisuelle qui pourra se manifester par des cauchemars, de la peur, de l’angoisse…</li></ul>
                                <!-- /wp:list -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Le sommeil a un rôle majeur dans le bon développement de l’adolescent d’un point de vue physique par la croissance mais aussi d’un point de vue psychique. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>En effet, c’est pendant le sommeil que l’adolescent va : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Récupérer de la fatigue physique. </li><li>Grandir en sécrétant l’hormone de croissance. </li><li>Rêver et développer son psychisme. </li><li>Développer sa capacité d’apprentissage et de mémorisation. </li><li>Stimuler son système immunitaire. </li><li>Eliminer les toxines. </li><li>Réguler la glycémie (mauvais sommeil = risque accru de surpoids) .</li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>Il est préférable de favoriser </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Les jeux calmes.</li><li>La lecture.</li><li>Des jeux de société.</li><li>Des puzzles… </li><li>Introduire un rituel de coucher.</li><li>Expliquer à votre adolescent comment fonctionne l’endormissement avec des mots simples. </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>Les besoins de sommeil de votre adolescent en fonction de son âge est de 9-10h de sommeil /jours</p>
                                <!-- /wp:paragraph -->
                            </div>

                        <?php endif; ?>


                    <?php endif; ?>

                    <?php if($_POST["ecran_avant_coucher"] === "non"):  ?>
                        <div class="alert alert-success oui_television_famille_alert">
                            
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                </li>
                <li>
                    <h3 class="question_label"> 8. Votre enfant a-il dans sa chambre ? </h3>
                    <h4>Un ordinateur:</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["chambre_ordinateur"]; ?></span></p>
                    <?php if($_POST["chambre_ordinateur"] === "non") : ?>
                    <!-- réponse oui -->
                    <div class="alert alert-success">
                        <!-- wp:paragraph -->
                        <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                        <!-- /wp:paragraph -->
                    </div>
                    <?php endif; ?>
                    <?php if($_POST["chambre_ordinateur"] === "oui") : ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    
                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre adolescent.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>
                    <h4>Une TV:</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["chambre_television"]; ?></span></p>
                    <?php if($_POST["chambre_television"] === "non") : ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                    <?php if($_POST["chambre_television"] === "oui") : ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "1"  || $tranche_age == "2"  || $tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre adolescent.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <h4>Y apporte-t-il un ou des écrans mobiles ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["chambre_ecran_mobile"]; ?></span></p>
                    <?php if($_POST["chambre_ecran_mobile"] === "non") : ?>
                        <div class="alert alert-success">
                            <!-- wp:paragraph -->
                            <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>
                    <?php if($_POST["chambre_ecran_mobile"] === "oui") : ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "1"  || $tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Il faudra éviter que votre adolescent n’emporte son smartphone dans sa chambre, car il y a un risque : </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:list -->
                                <ul><li>De dépendance.</li><li>De violence visuelle, en visualisation des scènes choquantes non adaptés à son âgé (image ou site pornographique, image ou scène de violence…).</li><li>Mauvaise utilisation des réseaux sociaux (harcèlement, dévoiler sa vie privée, envoi de photos oser …</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>

                    <h4>S’endort-il devant ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["chambre_ecran_endormir"]; ?></span></p>
                    <?php if($_POST["chambre_ecran_endormir"] === "non") : ?>
                    <!-- réponse oui -->
                    <div class="alert alert-success">
                        <!-- wp:paragraph -->
                        <p>Très bien, vous adoptez la bonne conduite à tenir</p>
                        <!-- /wp:paragraph -->
                    </div>
                    <?php endif; ?>
                    <?php if($_POST["chambre_ecran_endormir"] === "oui") : ?>
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "1"  || $tranche_age == "2" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        
                        <?php if($tranche_age == "3" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre enfant. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Les risques lorsque votre enfant dort avec son smartphone : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Trouble du sommeil avec des crises d’insomnie, des nuits cauchemardesques, des maux de tête au réveil. </li><li>C’est le rendre plus vigilant, toujours en alerte et sur le qui-vive. Même pendant leur sommeil, il a du mal à s’endormir, car son subconscient reste conscient. En effet, dans l’obscurité, la lumière produite par les téléphones devient plus intense et comporte plus de bleu, à cause des LEDs. Cette lumière bouleverse l’horloge biologique de l’utilisateur et l’empêche de trouver le sommeil facilement. </li><li>De développer une dépendance aux écrans </li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>

                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Aucuns écrans dans la chambre, les écrans nomades doivent être rangé dans un lieu dédié commun à la famille. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Si votre enfant a besoin de faire une recherche sur internet pour l’école, il faudra toujours être à proximité et vérifier le contenu de ce qu’il explore sur internet. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Utiliser les contrôles parentaux, pour éviter que votre enfant se trouve face à des sites ou images non adapter à son âge, comme par exemple des sites pornographiques. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Fermer la wifi le soir, pour déconnecter votre enfant et ainsi il pourra se concentrer sur ses devoir ou son activité.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Jamais d’écrans dans la chambre de votre adolescent. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Les risques lorsque votre adolescent dort avec son smartphone : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Trouble du sommeil avec des crises d’insomnie, des nuits cauchemardesques, des maux de tête au réveil. </li><li>C’est le rendre plus vigilant, toujours en alerte et sur le qui-vive. Même pendant leur sommeil, il a du mal à s’endormir, car son subconscient reste conscient. En effet, dans l’obscurité, la lumière produite par les téléphones devient plus intense et comporte plus de bleu, à cause des LEDs. Cette lumière bouleverse l’horloge biologique de l’utilisateur et l’empêche de trouver le sommeil facilement. </li><li>De développer une dépendance aux écrans </li><li>…</li></ul>
                                <!-- /wp:list -->
                            </div>

                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Aucuns écrans dans la chambre, les écrans nomades doivent être rangé dans un lieu dédié commun à la famille. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Si votre adolescent a besoin de faire une recherche sur internet pour l’école, il faudra toujours être à proximité et vérifier le contenu de ce qu’il explore sur internet. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Utiliser les contrôles parentaux, pour éviter que votre adolescent se trouve face à des sites ou images non adapter à son âge, comme par exemple des sites pornographiques. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Fermer la wifi le soir, pour déconnecter votre adolescent et ainsi il pourra se concentrer sur ses devoir ou son activité.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>




                    <?php endif; ?>





                </li>

                <li>
                    <h3 class="question_label"> 9.	Est-ce que votre enfant regarde et écoute habituellement : </h3>
                    <h4>Les dessins animés :</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["dessin_anime"]; ?></span></p>
                    <?php if(!$age_apres_3_ans) : ?>
                        <?php if($_POST["dessin_anime"] == "aucun") : ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php else : ?>

                        <div class="alert alert-danger" >
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:paragraph -->
                            <p>Il est fortement déconseillé de mettre votre enfant à cet âge devant des dessins animés.</p>
                            <!-- /wp:paragraph -->
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($age_apres_3_ans) : ?>
                        <?php if($_POST["dessin_anime"] == "seul") : ?>
                            <?php if($tranche_age == "1" || $tranche_age == "2"): ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Attention, il est fortement déconseillé de mettre votre enfant à cet âge seul devant des dessins animés.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>À cet âge votre enfant peut regarder des dessins animés seul, mais il faudra vérifier si le contenu est adapté à son âge, et respecter la limite de 1heure 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>À cet âge votre adolescent peut regarder des dessins animés seul, mais il faudra vérifier si le contenu est adapté à son âge, et respecter la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if($_POST["dessin_anime"] == "avec vous") : ?>
                            <?php if($tranche_age == "1"): ?>
                                <div class="alert alert-success" >
                                    <!-- wp:paragraph -->
                                    <p>Votre enfant peut regarder des dessins animés adaptés à son âge en votre présence, sans dépasser la limite de 30 minutes d’écrans par jour. </p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "2" ): ?>
                                <div class="alert alert-success" >
                                    <!-- wp:paragraph -->
                                    <p>Votre enfant peut regarder des dessins animés adaptés à son âge en votre présence, sans dépasser la limite de 1 heure d’écrans par jour. </p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3" ): ?>
                                <div class="alert alert-success" >
                                    <!-- wp:paragraph -->
                                    <p>Votre enfant peut regarder des dessins animés adaptés à son âge en votre présence, sans dépasser la limite de 1 heure 30 minutes d’écrans par jour. </p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "4" ): ?>
                                <div class="alert alert-success" >
                                    <!-- wp:paragraph -->
                                    <p>Votre adolescent peut regarder des dessins animés adapter à son âge en votre présence, sans dépasser la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>


                        <?php if($_POST["dessin_anime"] == "aucun") : ?>
                            <?php if($tranche_age == "1"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge, sans dépasser la limite de 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "2"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptésà son âge, sans dépasser la limite de 1 heure d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge sans dépasser la limite de 1 heure 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien mais votre adolescent peut regarder des programmes adaptés à son âge sans dépasser la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif;?>


                    <?php endif; ?>

                    <h4>Les programmes éducatifs :</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["programme_educatif"]; ?></span></p>
                    <?php if(!$age_apres_3_ans) : ?>

                        <?php if($_POST["programme_educatif"] == "aucun") : ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php else : ?>

                        <div class="alert alert-danger" >
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:paragraph -->
                            <p>Il est fortement déconseillé de mettre votre enfant à cet âge devant des programmes éducatifs.</p>
                            <!-- /wp:paragraph -->
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>

                   
                    <?php if($_POST["programme_educatif"] == "seul") : ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2"): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention, il est fortement déconseillé de mettre votre enfant à cet âge seul devant des programmes éducatifs.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "3"): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>À cet âge votre enfant peut regarder des programmes éducatifs seul, mais il faudra vérifier si le contenu est adapté à son âge, et respecter la limite de 1 heure 30 minutes d’écran par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>A cet âge votre adolescent peut regarder des programmes éducatifs seul, mais il faudra vérifier si le contenu est adapté à son âge, et respecter la limite de 2 heures d’écran par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["programme_educatif"] == "avec vous") : ?>
                        <?php if($tranche_age == "1" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre enfant peut regarder des programmes éducatifs  adaptés à son âge en votre présence, sans dépasser la limite de 30 minutes d’écrans par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "2" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre enfant peut regarder des programmes éducatifs adaptés à son âge en votre présence, sans dépasser la limite de 1 heure d’écrans par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "3" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre enfant peut regarder des programmes éducatifs adaptés à son âge en votre présence, sans dépasser la limite de 1 heure 30 minutes d’écrans par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "4" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre adolescent peut regarder des programmes éducatifs adaptés à son âge en votre présence, sans dépasser la limite de 2 heures d’écrans par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($_POST["programme_educatif"] == "aucun") : ?>
                            <?php if($tranche_age == "1"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge, sans dépasser la limite de 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "2"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptésà son âge, sans dépasser la limite de 1 heure d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge sans dépasser la limite de 1 heure 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien mais votre adolescent peut regarder des programmes adaptés à son âge sans dépasser la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif;?>

                    <h4>Les vidéos :</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["video"]; ?></span></p>
                    <?php if(!$age_apres_3_ans) : ?>
                        <div class="alert alert-danger" >
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->

                            <!-- wp:paragraph -->
                            <p>Il est fortement déconseillé de mettre votre enfant à cet âge devant des vidéos.</p>
                            <!-- /wp:paragraph -->
                        </div>
                    <?php endif; ?>

                    <?php if($_POST["video"] == "seul") : ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2"): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention, il est fortement déconseillé de mettre votre enfant à cet âge seul devant des vidéos.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "3"): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>À cet âge votre enfant peut regarder des vidéos seul, mais il faudra vérifier si le contenu est adapté à son âge, et respecter la limite de 1 heure 30 minutes d’écran par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>À cet âge votre adolescent peut regarder des vidéos seul, mais il faudra vérifier si le contenu et adapté à son âge, et respecte la limite de 2 heures d’écran par jour.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["video"] == "avec vous") : ?>
                        <?php if($tranche_age == "1" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre enfant peut regarder des vidéos adaptés à son âge en votre présence, sans dépasser la limite de 30 minutes d’écrans par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "2" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre enfant peut regarder des vidéos adaptés à son âge en votre présence, sans dépasser la limite de 1 heure d’écrans par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "3" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre enfant peut regarder des vidéos adaptés à son âge en votre présence, sans dépasser la limite de 1 heure et 30 minutes d’écrans par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4" ): ?>
                            <div class="alert alert-success" >
                                <!-- wp:paragraph -->
                                <p>Votre adolescent peut regarder des vidéos adapter à son âge en votre présence, sans dépasser la limite de 2 heures d’écran par jour. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["video"] == "aucun") : ?>
                            <?php if($tranche_age == "1"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge, sans dépasser la limite de 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "2"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptésà son âge, sans dépasser la limite de 1 heure d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge sans dépasser la limite de 1 heure 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien mais votre adolescent peut regarder des programmes adaptés à son âge sans dépasser la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif;?>

                    <h4>Des programmes non destinés aux enfants :</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["programme_non_enfant"]; ?></span></p>
                    <?php if(!$age_apres_3_ans) : ?>
                        <?php if($_POST["programme_non_enfant"] == "aucun") : ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php else : ?>
                        <div class="alert alert-danger" >
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    <?php if($_POST["programme_non_enfant"] == "seul") : ?>
                        <?php if($tranche_age == "1" ): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention ! </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Les programmes destinés aux adultes n’apporte rien à votre enfant => c’est du temps volé à toutes les autres activités. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Risque de violence visuel => les petits enfants n’ont pas les capacités émotionnelles de comprendre des images qui ne sont pourtant ni violentes ni choquantes.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "2" || $tranche_age == "3"): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention ! </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les programmes destinés aux adultes n’apportent rien à votre enfant => c’est du temps volé à toutes les autres activités avec un risque d’exposer votre enfant à des violences visuelles. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention ! </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les programmes destinés aux adultes n’apportent rien à votre adolescent => c’est du temps volé à toutes les autres activités avec un risque d’exposer votre adolescent à des violences visuelles. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["programme_non_enfant"] == "avec vous") : ?>
                        <?php if($tranche_age == "1" ): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention ! </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Les programmes destinés aux adultes n’apporte rien à votre enfant => c’est du temps volé à toutes les autres activités. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Risque de violence visuel => les petits enfants n’ont pas les capacités émotionnelles de comprendre des images qui ne sont pourtant ni violentes ni choquantes.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "2"  || $tranche_age == "3"): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention ! </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les programmes destinés aux adultes n’apportent rien à votre enfant => c’est du temps volé à toutes les autres activités avec un risque d’exposer votre enfant à des violences visuelles. </p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger" >
                                <!-- wp:paragraph -->
                                <p>Attention ! </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les programmes destinés aux adultes n’apportent rien à votre adolescent => c’est du temps volé à toutes les autres activités avec un risque d’exposer votre adolescent à des violences visuelles</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>

                    <?php if($_POST["programme_non_enfant"] == "aucun") : ?>
                            <?php if($tranche_age == "1"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge, sans dépasser la limite de 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "2"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptésà son âge, sans dépasser la limite de 1 heure d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge sans dépasser la limite de 1 heure 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien mais votre adolescent peut regarder des programmes adaptés à son âge sans dépasser la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif;?>
                    
                    <h4>Jeux video :</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["jeux_video"]; ?></span></p>
                    <?php if(!$age_apres_3_ans) : ?>
                        <?php if($_POST["jeux_video"] == "aucun") : ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php else : ?>

                        <div class="alert alert-danger" >
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>

                
                    <?php if($_POST["jeux_video"] == "seul") : ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les jeux vidéo sont déconseillés à cet âge.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les jeux vidéo sont déconseillés avant l’âge de 6 ans.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Préférez les jeux vidéo auxquels on joue à plusieurs à ceux auxquels on joue seul. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>N’offrez pas une console de jeux personnelle à votre enfant => car jouer seul devient rapidement stéréotypé et compulsif. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Il faudra contrôler le contenu des scènes du jeu vidéo et respecter l’âge indiqué pour le jeu. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Vous pouvez utiliser le site PEGI ( Pan European Game Information) pour savoir l’âge pour lequel le jeu est préconisé. <a href="https://pegi.info/fr" target="_blank" rel="noreferrer noopener">https://pegi.info/fr</a></p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention aux risques de dépendance !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Attention aux jeux violents !</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Préférez les jeux vidéo auxquels on joue à plusieurs à ceux auxquels on joue seul. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>N’offrez pas une console de jeux personnelle à votre adolescent => car jouer seul devient rapidement stéréotypé et compulsif. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Il faudra contrôler le contenu des scènes du jeu vidéo et respecter l’âge indiqué pour le jeu. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Vous pouvez utiliser le site PEGI ( Pan European Game Information) pour savoir l’âge pour lequel le jeu est préconisé. <a href="https://pegi.info/fr" target="_blank" rel="noreferrer noopener">https://pegi.info/fr</a></p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["jeux_video"] == "avec vous") : ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2" ): ?>
                        <div class="alert alert-danger">
                            <!-- wp:paragraph -->
                            <p>Attention !</p>
                            <!-- /wp:paragraph -->
                            <!-- wp:paragraph -->
                            <p>Les jeux vidéo sont déconseillés à cet âge.</p>
                            <!-- /wp:paragraph -->
                        </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "3"): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les jeux vidéo sont déconseillés avant l’âge de 6 ans.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Préférez les jeux vidéo auxquels on joue à plusieurs à ceux auxquels on joue seul. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>N’offrez pas une console de jeux personnelle à votre enfant => car jouer seul devient rapidement stéréotypé et compulsif. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Il faudra contrôler le contenu des scènes du jeu vidéo et respecter l’âge indiqué pour le jeu. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Vous pouvez utiliser le site PEGI ( Pan European Game Information) pour savoir l’âge pour lequel le jeu est préconisé. <a href="https://pegi.info/fr" target="_blank" rel="noreferrer noopener">https://pegi.info/fr</a></p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4"): ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Préférez les jeux vidéo auxquels on joue à plusieurs à ceux auxquels on joue seul. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>N’offrez pas une console de jeux personnelle à votre adolescent => car jouer seul devient rapidement stéréotypé et compulsif. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Il faudra contrôler le contenu des scènes du jeu vidéo et respecter l’âge indiqué pour le jeu. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Vous pouvez utiliser le site PEGI ( Pan European Game Information) pour savoir l’âge pour lequel le jeu est préconisé. <a href="https://pegi.info/fr" target="_blank" rel="noreferrer noopener">https://pegi.info/fr</a></p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($_POST["jeux_video"] == "aucun") : ?>
                            <?php if($tranche_age == "1"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge, sans dépasser la limite de 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "2"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptésà son âge, sans dépasser la limite de 1 heure d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge sans dépasser la limite de 1 heure 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien mais votre adolescent peut regarder des programmes adaptés à son âge sans dépasser la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif;?>

                    <h4>Internet :</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["internet"]; ?></span></p>
                    <?php if(!$age_apres_3_ans) : ?>
                        <?php if($_POST["internet"] == "aucun") : ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php else : ?>
                        <div class="alert alert-danger" >
                            <!-- wp:paragraph -->
                            <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                            <!-- /wp:paragraph -->
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    
                    <?php if($_POST["internet"] == "seul") : ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2" ) : ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>L’accès à internet est déconseillé à cet âge.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "3") : ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>L’accès à internet est déconseillé avant l’âge de 9 ans.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les réseaux sociaux sont interdits aux moins de 13 ans.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention en France les sites pornographiques sont accessibles à tous, sans aucune restriction.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention aux scènes violentes non adaptées à l’âge de votre enfant qui peuvent avoir des conséquences graves sur le développement psychologique de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4") : ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les réseaux sociaux sont interdits aux moins de 13 ans.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention en France les sites pornographiques sont accessibles à tous, sans aucune restriction.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention aux scènes violentes non adaptées à l’âge de votre adolescent qui peuvent avoir des conséquences graves sur le développement psychologique de votre adolescent.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:list -->
                                <ul><li>À partir de 13 ans il faudra expliquer à votre adolescent : <ul><li>Le droit à l’image et le droit à l’intimité </li><li>Ainsi lui rappeler régulièrement les 3 caractéristiques d’internet <ul><li>Tout ce que l’on y met peut tomber dans le domaine public. </li><li>Tout ce que l’on y met y restera éternellement. </li><li>Tout ce que l’on y trouve est sujet à doute : certaines données sont vraies et d’autres fausses. </li></ul></li></ul></li><li>Si votre adolescent a besoin de faire une recherche sur internet pour l’école, il faudra toujours être à proximité et vérifier le contenu de ce qu’il explore sur internet. </li><li>Utiliser les contrôles parentaux, pour éviter que votre adolescent se trouve face à des sites ou images non adapté à son âge, comme par exemple des sites pornographiques. </li><li>Fermer la wifi le soir, pour déconnecter votre adolescent et ainsi il pourra se concentrer sur ses devoirs ou ses activités.</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>


                    <?php if($_POST["internet"] == "avec vous") : ?>
                        <?php if($tranche_age == "1" || $tranche_age == "2" ) : ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>L’accès à internet est déconseillé à cet âge.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "3") : ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>L’accès à internet est déconseillé avant l’âge de 9 ans.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les réseaux sociaux sont interdits aux moins de 13 ans.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention en France les sites pornographiques sont accessibles à tous, sans aucune restriction.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention aux scènes violentes non adaptées à l’âge de votre enfant qui peuvent avoir des conséquences graves sur le développement psychologique de votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Si votre enfant a besoin de faire une recherche sur internet pour l’école, il faudra toujours être à proximité et vérifier le contenu de ce qu’il explore sur internet.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4") : ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Attention !</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Les réseaux sociaux sont interdits aux moins de 13 ans.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention en France les sites pornographiques sont accessibles à tous, sans aucune restriction.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Faites attention aux scènes violentes non adaptées à l’âge de votre adolescent qui peuvent avoir des conséquences graves sur le développement psychologique de votre adolescent.</p>
                                <!-- /wp:paragraph -->
                            </div>
                            <div class="alert alert-info">
                                <!-- wp:list -->
                                <ul><li>À partir de 13 ans il faudra expliquer à votre adolescent : <ul><li>Le droit à l’image et le droit à l’intimité </li><li>Ainsi lui rappeler régulièrement les 3 caractéristiques d’internet <ul><li>Tout ce que l’on y met peut tomber dans le domaine public. </li><li>Tout ce que l’on y met y restera éternellement. </li><li>Tout ce que l’on y trouve est sujet à doute : certaines données sont vraies et d’autres fausses. </li></ul></li></ul></li><li>Si votre adolescent a besoin de faire une recherche sur internet pour l’école, il faudra toujours être à proximité et vérifier le contenu de ce qu’il explore sur internet. </li><li>Utiliser les contrôles parentaux, pour éviter que votre adolescent se trouve face à des sites ou images non adapté à son âge, comme par exemple des sites pornographiques. </li><li>Fermer la wifi le soir, pour déconnecter votre adolescent et ainsi il pourra se concentrer sur ses devoirs ou ses activités.</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    

                    <?php if($_POST["internet"] == "aucun") : ?>
                            <?php if($tranche_age == "1"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge, sans dépasser la limite de 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "2"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptésà son âge, sans dépasser la limite de 1 heure d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "3"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien, mais votre enfant peut regarder des programmes adaptés à son âge sans dépasser la limite de 1 heure 30 minutes d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <?php if($tranche_age == "4"): ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Bien mais votre adolescent peut regarder des programmes adaptés à son âge sans dépasser la limite de 2 heures d’écran par jour.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                        <?php endif;?>




                </li>
                <li>
                    <h3 class="question_label">10.	Est-ce que vous regardez régulièrement en famille la télévision ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["television_famille"]; ?> </span></p>
                    <?php if($_POST["television_famille"] === "oui") : ?>
                    <h4>Ces moments sont-ils l’occasion d’échanges avec votre enfant ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["echange_television_famille"]; ?></span></p>
                    <?php endif; ?>

                    <?php if($_POST["television_famille"] === "non") : ?>
                        <?php if($tranche_age == "0") : ?>
                            <div class="alert alert-success">
                               <!-- wp:paragraph -->
                               <p>Très bien, vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "1") : ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien, mais il est possible de regarder la télévision avec son enfant à conditions : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Que le contenu soi adapté à son âge pour limiter la violence visuelle. ·</li><li>Respecter la durée limitée à 30 minutes par jour à cet âge. </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>Il est très fortement recommandé de commenter le contenu visualisé avec votre enfant, vous devez avoir un rôle actif.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if($tranche_age == "2" ) : ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien, mais il est possible de regarder la télévision avec son enfant à conditions : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Que le contenu soi adapté à son âge pour limiter la violence visuelle. ·</li><li>Respecter la durée limitée à 1 heure par jour à cet âge. </li></ul>
                                <!-- /wp:list -->

                                <!-- wp:paragraph -->
                                <p>Il est très fortement recommandé de commenter le contenu visualisé avec votre enfant, vous devez avoir un rôle actif.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "3" ) : ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien, mais il est possible de regarder la télévision avec son enfant à conditions : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Que le contenu soi adapté à son âge pour limiter la violence visuelle.</li><li>Respecter la durée limitée à 1 heure et 30 minutes par jour à cet âge. </li></ul>
                                <!-- /wp:list -->

                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4" ) : ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Bien, mais il est possible de regarder la télévision avec son adolescent à conditions : </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Que le contenu soi adapté à son âge pour limiter la violence visuelle. </li><li>Respecter la durée limitée à 2 heures par jour à cet âge. </li></ul>
                                <!-- /wp:list -->

                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($_POST["television_famille"] === "oui") : ?>
                        
                        <?php if($tranche_age == "0" ): ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "1" || $tranche_age == "2") :  ?>
                            <?php if($_POST["echange_television_famille"] === "oui") : ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["echange_television_famille"] === "non") : ?>
                                <div class="alert alert-danger">
                                    <!-- wp:paragraph -->
                                    <p>Attention, à cet âge il est important d’échanger avec son enfant lors qu’on regarde la télévision.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Regarder la télévision en famille est un moment de partage et un moment pédagogique.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Il est très important d’avoir un rôle actif en commentent ce que l’on voit à la télévision avec son enfant pour rendre le virtuel réel et compressible par votre enfant.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "3") :  ?>
                            <?php if($_POST["echange_television_famille"] === "oui") : ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["echange_television_famille"] === "non") : ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Il est préférable d’échanger avec son enfant sur les scènes visualisées à la télévision.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Regarder la télévision en famille est un moment de partage et un moment pédagogique.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Il est très important d’avoir un rôle actif en commentent ce que l’on voit à la télévision avec son enfant pour éviter de l’exposer au risque de violence visuelle.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                        <?php if($tranche_age == "4") :  ?>
                            <?php if($_POST["echange_television_famille"] === "oui") : ?>
                                <div class="alert alert-success">
                                    <!-- wp:paragraph -->
                                    <p>Très bien, vous adoptez la bonne conduite à tenir.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>

                            <?php if($_POST["echange_television_famille"] === "non") : ?>
                                <div class="alert alert-warning">
                                    <!-- wp:paragraph -->
                                    <p>Il est préférable d’échanger avec son adolescent sur les scènes visualisées à la télévision.</p>
                                    <!-- /wp:paragraph -->
                                </div>
                            <?php endif; ?>
                            <div class="alert alert-info">
                                <!-- wp:paragraph -->
                                <p>Regarder la télévision en famille est un moment de partage et un moment pédagogique.</p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Il est très important d’avoir un rôle actif en commentent ce que l’on voit à la télévision avec son adolescent pour éviter de l’exposer au risque de violence visuelle.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>
                </li>

                <li>
                    <h3 class="question_label"> 11. Votre famille s’est-elle dotée de règles ou de directives relatives à l’utilisation des écrans ?</h3>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["regle_ecran"]; ?></span></p>
                    <?php /* if($_POST["regle_ecran"] === "oui") : ?>
                    <h4>Sont-elles faciles à faire respecter ?</h4>
                    <p class="question_reponse"><strong>Votre réponse : </strong><span class="valeur_reponse"><?= $_POST["regle_facile"]; ?></span></p>
                    <?php endif; */ ?>

                    <?php if($tranche_age == "0"): ?>
                        <?php if($_POST["regle_ecran"] == "non" || 
                                $_POST["regle_ecran"] == "oui") : ?>
                            <div class="alert alert-warning">
                                <!-- wp:paragraph -->
                                <p>Il faudra faire attention car à cet âge il n’est pas recommandé de mettre votre enfant devant un écran car ceci peut avoir des effets néfastes sur sa santé et son développement psychomoteur. </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:paragraph -->
                                <p>Risques : </p>
                                <!-- /wp:paragraph -->
                                <!-- wp:list -->
                                <ul><li>Impact sur la santé <ul><li>Myopie </li><li>Surpoids </li><li>Obésité </li><li>Risque d’addiction </li><li>Trouble du sommeil </li></ul></li><li>Effet psycho-sociale <ul><li>Passivité </li><li>Repli sur soi, isolement</li><li>Violence</li><li>L’imitation de l’imaginaire </li></ul></li><li>Influence sur notre mode de vie et nos consommations </li><li>Violence visuelle</li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    <?php if($tranche_age == "1" || $tranche_age == "2" || $tranche_age == "3" || $tranche_age == "4"): ?>
                        <?php if ($_POST["regle_ecran"] == "oui") : ?>
                            <div class="alert alert-success">
                                <!-- wp:paragraph -->
                                <p>Très bien, vous adoptez la bonne conduite à tenir.</p>
                                <!-- /wp:paragraph -->
                            </div>
                        <?php endif; ?>
                        <?php if ($_POST["regle_ecran"] == "non") : ?>
                            <div class="alert alert-danger">
                                <!-- wp:paragraph -->
                                <p>Il est important de mettre en place des règles pour la bonne pratique des écrans (la charte familiale). </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:paragraph -->
                                <p>Ces règles ont pour but d’aider les familles à se positionner sur les différents sujets des écrans dans la famille et d’impliquer toute la famille sur les bonnes règles à suivre. </p>
                                <!-- /wp:paragraph -->

                                <!-- wp:list -->
                                <ul><li>Il faudra évoquer tous les sujets en famille et de s’accorder sur les points essentiels. </li><li>C’est un document personnalisable selon les habitudes familiales et l’âge des enfants. </li><li>Une fois rempli par tous les membres de la famille, il est possible de l’imprimer pour l’afficher dans la cuisine sur le réfrigérateur. <ul><li>Pour les anglophones et hispanophones il est possible de le remplir en ligne en <a href="https://www.healthychildren.org/English/media/Pages/default.aspx" data-type="URL" data-id="https://www.healthychildren.org/English/media/Pages/default.aspx" target="_blank" rel="noreferrer noopener">cliquant ici</a>.</li><li>Pour les francophones : <ul><li>En <a href="http://www.surexpositionecrans.org/wp-content/uploads/2018/05/Charte-familiale-moins-de-cinq-ans-cose.pdf" data-type="URL" data-id="http://www.surexpositionecrans.org/wp-content/uploads/2018/05/Charte-familiale-moins-de-cinq-ans-cose.pdf" target="_blank" rel="noreferrer noopener">cliquant ici</a> pour la charte familiale du moins de cinq ans </li><li>En <a href="http://www.surexpositionecrans.org/wp-content/uploads/2018/05/Charte-familiale-plus-de-cinq-ans-cose.pdf" data-type="URL" data-id="http://www.surexpositionecrans.org/wp-content/uploads/2018/05/Charte-familiale-plus-de-cinq-ans-cose.pdf" target="_blank" rel="noreferrer noopener">cliquant ici</a> pour la charte familiale du plus de cinq ans</li></ul></li></ul></li></ul>
                                <!-- /wp:list -->
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>






                </li>
			</ul>
		</div>
    </div>
    
    <div class="row">
        <div class="offset-md-8">
            <button type="button" class="btn btn-mdb btn-primary waves-effect waves-light telecharger_pdf_questionnaire">Télécharger en PDF</button>
        </div>
    </div>

<?php

// Trouver toutes les catégories de fiche conseils
$args = array (
    'taxonomy' => 'age_categorie', //your custom post type
    //'orderby' => 'name',
    //'order' => 'ASC',
    'hide_empty' => 0 //shows empty categories
);
$ages = get_categories( $args );

?>
    <h3 class="text-center"><?=  'Fiches d\'information ' . $ages[$tranche_age]->name; ?></h3>
    <div class="row mt-30">


    

     <!-- Récupérer toutes les charte de famille -->
 <?php
        $fiches = new WP_Query([
            'post_type' => 'affiche',
            'tax_query' => array(
              array(
              'taxonomy' => 'age_categorie',
              'field' => 'name',
              'terms' => $ages[$tranche_age]->name
               )
            )
        ]);

        $modal_number = 0;
        while($fiches->have_posts()) : $fiches->the_post();
        $modal_number++;
    ?>

    
<div class="col-md-4 col-sm-6 pb-2">
            <div class="box17">
                <img height="50%" src="<?= get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?= the_title(); ?>">
                <ul class="icon">
                    <li data-toggle="modal" data-target="#modal<?= $modal_number; ?>"><a href="#"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a></li>
                    <li onclick="PrintImage('<?= get_the_post_thumbnail_url(get_the_ID()); ?>')"><a><i class="fa fa-print" aria-hidden="true"></i></a></li>
                </ul>
                <div class="box-content">
                    <h3 class="title"><?= the_title(); ?></h3>
                </div>
            </div>
        </div>

    <div class="row">
        <div class="col-lg-4 col-md-12 mb-4">
            <div class="modal fade" id="modal<?= $modal_number; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-footer center-content">
                    <h2 style="margin: auto;"> <?= the_title(); ?> </h2>
                </div>

                <div class="modal-body ">
                <img style="margin: auto;" src="<?= get_the_post_thumbnail_url(get_the_ID());  ?>" class="teamy__avatar" alt="<?= the_title(); ?>">
                </div>
                <div class="modal-footer justify-content-center">
                    <button  onclick="PrintImage('<?= get_the_post_thumbnail_url(get_the_ID()); ?>')" type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4">Imprimer</button>
                    <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Fermer</button>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>

<?php endwhile; wp_reset_postdata(); ?>
    </div>
</div>


<?php get_footer(); ?>