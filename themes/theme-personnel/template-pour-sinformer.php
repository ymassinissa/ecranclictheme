<?php
/**
 * Template Name: page pour s'informer
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>

<!-- Les SVG -->
<div class="svg"> 
    <svg width="26" height="28" style="display: none;">
        <symbol id="telecharger" viewBox="0 0 26 28">
        <path d="M11 21.883l-6.235-7.527-.765.644 7.521 9 7.479-9-.764-.645-6.236 7.529v-21.884h-1v21.883z"/>
        </symbol>
        <symbol id="voir" viewBox="0 0 26 28">
            <path d="M11 21h8v-2l1-1v4h-9v2l-10-3v-18l10-3v2h9v5l-1-1v-3h-8v18zm10.053-9l-3.293-3.293.707-.707 4.5 4.5-4.5 4.5-.707-.707 3.293-3.293h-9.053v-1h9.053z"/>  
        </symbol>
        <symbol id="codepen" viewBox="0 0 28 28">
        <path d="M3.375 18.266l9.422 6.281v-5.609l-5.219-3.484zM2.406 16.016l3.016-2.016-3.016-2.016v4.031zM15.203 24.547l9.422-6.281-4.203-2.812-5.219 3.484v5.609zM14 16.844l4.25-2.844-4.25-2.844-4.25 2.844zM7.578 12.547l5.219-3.484v-5.609l-9.422 6.281zM22.578 14l3.016 2.016v-4.031zM20.422 12.547l4.203-2.812-9.422-6.281v5.609zM28 9.734v8.531c0 0.391-0.203 0.781-0.531 1l-12.797 8.531c-0.203 0.125-0.438 0.203-0.672 0.203s-0.469-0.078-0.672-0.203l-12.797-8.531c-0.328-0.219-0.531-0.609-0.531-1v-8.531c0-0.391 0.203-0.781 0.531-1l12.797-8.531c0.203-0.125 0.438-0.203 0.672-0.203s0.469 0.078 0.672 0.203l12.797 8.531c0.328 0.219 0.531 0.609 0.531 1z"></path>
        </symbol>
        
    </svg>
</div>


<div class="cadre" style="margin: 25px auto!important;">

    <div class="titre-conseil">
    <!-- Afficher le contenu de la page "s'informer" --> 
    <?php the_content(); ?>
    </div>

    <div>
        <div class="row">
    <!-- Récupérer tous les livres -->
    <?php
        $livre = new WP_Query([
            'post_type' => 'livre'
        ]);
        $modal_number = 0;
        while($livre->have_posts()) : $livre->the_post();
        $modal_number++;
    ?>

            <article class="teamy teamy_style3 teamy_mask-circle col-md-4 col-sm-6" data-toggle="modal" data-target="#modal<?= $modal_number; ?>">
                <div class="teamy__layout">
                    <div class="teamy__preview">
                        <img src="<?= get_the_post_thumbnail_url(get_the_ID());  ?>" class="teamy__avatar" alt="<?= the_title(); ?>">
                    </div>
                    <div class="teamy__back">
                        <div class="teamy__back-inner">
                            <div class="teamy__content">
                            <h3 class="teamy__name"><?php the_title(); ?></h3>
                            </div>
                            <div class="actions">
                            <span  class="action">
                                <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                                <span class="action__name">Voir</span>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <div class="row">
                <div class="col-lg-4 col-md-12 mb-4">
                    <div class="modal fade" id="modal<?= $modal_number; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-footer center-content">
                            <h2 style="margin: auto;"> <?= the_title(); ?> </h2>
                        </div>
                        <div class="modal-body center-content" >
                        <img style="margin: auto;" src="<?= get_the_post_thumbnail_url(get_the_ID());  ?>" class="teamy__avatar" alt="<?= the_title(); ?>">
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
  
        <?php endwhile; ?>

        </div>
    </div>

</div>



<?php get_footer(); ?>