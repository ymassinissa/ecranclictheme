<?php
/**
 * Template Name: page chart famille
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>

<div class="svg"> 
  <svg width="26" height="28" style="display: none;">
    <symbol id="telecharger" viewBox="0 0 26 28">
      <path d="M11 21.883l-6.235-7.527-.765.644 7.521 9 7.479-9-.764-.645-6.236 7.529v-21.884h-1v21.883z"/>
    </symbol>
    <symbol id="voir" viewBox="0 0 26 28">
        <path d="M11 21h8v-2l1-1v4h-9v2l-10-3v-18l10-3v2h9v5l-1-1v-3h-8v18zm10.053-9l-3.293-3.293.707-.707 4.5 4.5-4.5 4.5-.707-.707 3.293-3.293h-9.053v-1h9.053z"/>  
    </symbol>
    <symbol id="codepen" viewBox="0 0 28 28">
      <path d="M3.375 18.266l9.422 6.281v-5.609l-5.219-3.484zM2.406 16.016l3.016-2.016-3.016-2.016v4.031zM15.203 24.547l9.422-6.281-4.203-2.812-5.219 3.484v5.609zM14 16.844l4.25-2.844-4.25-2.844-4.25 2.844zM7.578 12.547l5.219-3.484v-5.609l-9.422 6.281zM22.578 14l3.016 2.016v-4.031zM20.422 12.547l4.203-2.812-9.422-6.281v5.609zM28 9.734v8.531c0 0.391-0.203 0.781-0.531 1l-12.797 8.531c-0.203 0.125-0.438 0.203-0.672 0.203s-0.469-0.078-0.672-0.203l-12.797-8.531c-0.328-0.219-0.531-0.609-0.531-1v-8.531c0-0.391 0.203-0.781 0.531-1l12.797-8.531c0.203-0.125 0.438-0.203 0.672-0.203s0.469 0.078 0.672 0.203l12.797 8.531c0.328 0.219 0.531 0.609 0.531 1z"></path>
    </symbol>
    <symbol id="linkedin" viewBox="0 0 24 28">
      <path d="M5.453 9.766v15.484h-5.156v-15.484h5.156zM5.781 4.984c0.016 1.484-1.109 2.672-2.906 2.672v0h-0.031c-1.734 0-2.844-1.188-2.844-2.672 0-1.516 1.156-2.672 2.906-2.672 1.766 0 2.859 1.156 2.875 2.672zM24 16.375v8.875h-5.141v-8.281c0-2.078-0.75-3.5-2.609-3.5-1.422 0-2.266 0.953-2.641 1.875-0.125 0.344-0.172 0.797-0.172 1.266v8.641h-5.141c0.063-14.031 0-15.484 0-15.484h5.141v2.25h-0.031c0.672-1.062 1.891-2.609 4.672-2.609 3.391 0 5.922 2.219 5.922 6.969z"></path>
    </symbol>
  </svg>
</div>



<div class="page">
  <div class="row">
    
    <!-- Récupérer toutes les charte de famille -->
    <?php
        $livre = new WP_Query([
            'post_type' => 'affiche',
            'tax_query' => array(
              array(
              'taxonomy' => 'affiche_langues',
              'field' => 'name',
              'terms' => $_POST['langue']
               )
            )
        ]);

        $modal_number = 0;
        while($livre->have_posts()) : $livre->the_post();
        $modal_number++;
    ?>




    <div class="col-md-4 col-sm-6 pb-2">
        <div class="box17">
            
            <img height="50%" src="<?= get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?= the_title(); ?>">
            <ul class="icon">
                <li data-toggle="modal" data-target="#modal<?= $modal_number; ?>"><a href="#"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a></li>
                <!--li><a target="_blank" rel="noreferrer noopener" href="<?= get_the_post_thumbnail_url(get_the_ID()); ?>"><i class="fa fa-external-link" aria-hidden="true"></i></a></li-->
                <li onclick="PrintImage('<?= get_the_post_thumbnail_url(get_the_ID()); ?>')"><a><i class="fa fa-print" aria-hidden="true"></i></a></li>
            </ul>
            <div class="box-content">
                <h3 class="title"><?= the_title(); ?></h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-12 mb-4">
            <div class="modal fade" id="modal<?= $modal_number; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-footer center-content">
                    <h2 style="margin: auto;"> <?= the_title(); ?> </h2>
                </div>

                <div class="modal-body ">
                <img id="print-image-element" style="margin: auto;" src="<?= get_the_post_thumbnail_url(get_the_ID());  ?>" class="teamy__avatar" alt="<?= the_title(); ?>">
                </div>
                <div class="modal-footer justify-content-center">
                    
                    <button  onclick="PrintImage('<?= get_the_post_thumbnail_url(get_the_ID()); ?>')" type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4">Imprimer</button>
                    <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Fermer</button>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>



    
  
        <?php endwhile; wp_reset_postdata(); ?>

   

  </div>
</div>





<?php get_footer(); ?>