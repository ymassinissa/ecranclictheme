<?php
/**
 * Template Name: page du questionnaire plus
 * Template post type: page, post
 */

?>

<?php  get_header(); ?>


<form method="post" id="questionnaire_ecran_form" action="<?php bloginfo('url'); ?>/index.php/questionnaire-resultat-plus">

<div class="row">
    <div class="col-md-12 mt-5">
        <h2>Questionnaire sur les écrans</h2>
        <div id="stepper1" class="bs-stepper">
        <div class="bs-stepper-header">
            <div class="step" data-target="#question-l-1">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">1</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-2">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">2</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-3">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">3</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-4">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">4</span>
            </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#question-l-5">
            <button type="button" class="btn step-trigger">
                <span class="bs-stepper-circle">5</span>
            </button>
            </div>
            
        </div>
        <div class="bs-stepper-content">
        <div id="question-l-1" class="content">
            <p class="enonce_question">  1. Age de l'enfant ?</p>
            <div class="row">
                <div class="col-md-3">
                    <label for="age_enfant_ans">Ans :</label>
                    <input type="text" id="age_enfant_ans" name="age_enfant_ans" class="form-control">
                </div>
                <div class="col-md-3">
                    <label for="">Mois :</label>
                    <input type="text" id="age_enfant_mois" name="age_enfant_mois" class="form-control">
                </div>
            </div>
            
            
            <br/>
            <button type="button" class="btn btn-primary next">Suivant</button>
        </div>
        <div id="question-l-2" class="content">
            <p class="enonce_question" >2. Pensez-vous que l’utilisation des écrans : </p>

            <div class="row">
                <div class="amr-radiomenu col-md-4"> 
                    <label>Développe l’éveil et les apprentissages ? </label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_eveil_apprentissage" name="eveil_apprentissage" />
                        <label class="amr-label" for="oui_eveil_apprentissage" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_eveil_apprentissage" name="eveil_apprentissage">
                        <label class="amr-label" for="non_eveil_apprentissage" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

                <div class="amr-radiomenu col-md-4"> 
                    <label>Aide votre enfant à se calmer ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_calme_enfant" name="calme_enfant">
                        <label class="amr-label" for="oui_calme_enfant" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_calme_enfant" name="calme_enfant">
                        <label class="amr-label" for="non_calme_enfant" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>


                <div class="amr-radiomenu col-md-4"> 
                    <label>Améliore son attention et sa concentration  ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_ameliore_attention" name="ameliore_attention">
                        <label class="amr-label" for="oui_ameliore_attention" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_ameliore_attention" name="ameliore_attention">
                        <label class="amr-label" for="non_ameliore_attention" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

                <div class="amr-radiomenu col-md-4"> 
                    <label>Le prépare au monde de demain ?</label><br>
                    <div class="amr-option-holder">
                        <input type="radio" value="oui" id="oui_monde_demain" name="monde_demain">
                        <label class="amr-label" for="oui_monde_demain" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Oui
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="non" id="non_monde_demain" name="monde_demain">
                        <label class="amr-label" for="non_monde_demain" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Non
                        </label>
                    </div>
                </div>

            </div>

        <button type="button" class="btn btn-primary next">Suivant</button>
    </div>
        <div id="question-l-3" class="content">
            <p class="enonce_question">3. D’après vous, le temps passé par votre enfant devant les écrans :</p>
            <div class="row">
                <div class="amr-radiomenu col-md-4"> 
                    <div class="amr-option-holder">
                        <input type="radio" value="Est plutôt adapté" id="plutot_adapte" name="temps_devant_ecran">
                        <label class="amr-label" for="plutot_adapte" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Est plutôt adapté :
                        </label>
                    </div>

                    <div class="amr-option-holder">
                        <input type="radio" value="Est plutôt trop important" id="plutot_important" name="temps_devant_ecran">
                        <label class="amr-label" for="plutot_important" >
                            <span class="amr-checkmark">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                            Est plutôt trop important :
                        </label>
                    </div>
                </div>
            </div>

            <button type="button" class="btn btn-primary next" >Suivant</button>
        </div>
    <div id="question-l-4" class="content">
        <p class="enonce_question">4. Age de début d’exposition aux écrans :</p>
        <div class="row">
            <div class="amr-radiomenu col-md-4"> 
                <div class="amr-option-holder">
                    <input type="radio" value="Séjour à la maternité" id="sejour_maternite" name="debut_exposition_ecran">
                    <label class="amr-label" for="sejour_maternite" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Séjour à la maternité
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="6 mois" id="six_mois" name="debut_exposition_ecran">
                    <label class="amr-label" for="six_mois" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        6 mois
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="1 an" id="un_an" name="debut_exposition_ecran">
                    <label class="amr-label" for="un_an" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        1 an
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="2 ans" id="deux_ans" name="debut_exposition_ecran">
                    <label class="amr-label" for="deux_ans" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        2 ans
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="3 ans" id="trois_ans" name="debut_exposition_ecran">
                    <label class="amr-label" for="trois_ans" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        3 ans
                    </label>
                </div>
            </div>
        </div>

      
        <button type="button" class="btn btn-primary next" >Suivant</button>
    </div>
    <div id="question-l-5" class="content">
        <p class="enonce_question">5. Estimez, vous parent, votre temps moyen passé chaque jour devant les écrans :</p>

        <div class="row">
            <div class="amr-radiomenu col-md-4"> 
                <div class="amr-option-holder">
                    <input type="radio" value="Moins de 30 mn" id="moin_30_min" name="parent_devant_ecran">
                    <label class="amr-label" for="moin_30_min" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        Moins de 30 mn
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="30 mn" id="trente_min" name="parent_devant_ecran">
                    <label class="amr-label" for="trente_min" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        30 mn
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="1 h" id="une_heur" name="parent_devant_ecran">
                    <label class="amr-label" for="une_heur" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        1 h
                    </label>
                </div>


                <div class="amr-option-holder">
                    <input type="radio" value="2 h" id="deux_heures" name="parent_devant_ecran">
                    <label class="amr-label" for="deux_heures" >
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        2 h
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="3 h" id="trois_heures" name="parent_devant_ecran">
                    <label class="amr-label" for="trois_heures">
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        3 h
                    </label>
                </div>

                <div class="amr-option-holder">
                    <input type="radio" value="5 h et plus" id="cinq_heures" name="parent_devant_ecran">
                    <label class="amr-label" for="cinq_heures">
                        <span class="amr-checkmark">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </span>
                        5 h et plus
                    </label>
                </div>
            </div>
        </div>
       
        <button type="submit" class="btn btn-mdb btn-default waves-effect waves-light" style="color:black;">Valider</button>
    </div>
</div>

</form>




<?php get_footer(); ?>